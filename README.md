# Requirements (Required)

[Node.js](https://nodejs.org/en/download/)

[MySQL](http://www.wampserver.com/en/)

[Python](https://www.python.org/downloads/)

<br/><br/>

# Install dependencies (Required)

**MySQL**

1. Open wamp manually clicking on the icon.

2. Open phpMyAdmin ```localhost/phpmyadmin```
3. Create a new database named ```projectdatabase```
4. Go to ```import``` and import the file ```App/API/projectdatabase.sql```
5. You had imported a clean database with a single super admin: ```admin@email.com``` ```rootROOT1```. Make sure you change the password!



**/App**

```
npm install
```

<br/>

**/Sync**

```
npm install
```

<br/><br/>

# Run (Required)

**MySQL**

```
To run MySQL open wamp manually clicking on the icon.
```

```
Before running MySQL it's necessary to increase the max number of connections allowed to at least 500. Otherwise the app will crash. The most users the app has, the most number of connections are needed.

```

[Explained Article](https://www.electrictoolbox.com/update-max-connections-mysql/)

[Change max connections on Windows](http://forum.ragezone.com/f691/fix-max_connections-error-wamp-936152/)

<br/>

<br/>

**API and FrontEnd -> /App**

```
npm run start
```

<br/>

**Sync service -> /Sync**

```
npm run start
```

<br/>

If any of the last two steps fails, make sure you execute the next section in this document: ```Secret files``` and then try to run again.

<br/><br/>

# Secret files (Required)

**JWT Secret Key**

Add a file at "/App/API/datafiles/" named **jwtsecretkey.json** with the following format:
```
{
    "secret_key": "Your secret key. Don't change it in runtime"
}
```

<br/>

**MySignals Credentials**

Add a file at "/Sync/datafiles/" named **mysignalscredentials.json** with the following format:
```
{
    "email": "MySignals email access",
    "password": "MySignals password access"
}
```

<br/><br/>

# Patient call to doctor (Optional)

1. Intall browser extension: ```Tampermonkey```
2. Create a new script and place the code from the file ```/App/Tampermonkey Scripts/Patient Call doctor consultation.txt```

<br/><br/>

# Changing the domain (Required if you enable HTTPS, change the port or change the domain of your app)

When you change the domain of the app it's necessary to change it in the frontend. Change the domain name in the following files:

- App/API/models/mysqlModule.js
- App/FrontEnd/Hospital/Backoffice/js/pedidos.js
- App/FrontEnd/Hospital/Frontffice/js/pedidos.js
- App/FrontEnd/Patient/js/pedidos.js
- App/FrontEnd/Patient/views/home.htm
- App/FrontEnd/Portal/index.html
- Sync/mysqlModule.js

<br/><br/>

# Enable HTTPS (Recommended)

To enable HTTPS follow the next steps:

1. Go to the file ```App/bin/app.js``` and scroll down to the bottom
2. Comment this code:

```
app.listen(8080, () => {
    console.log('Server is Running on Port 8080!');
});
```

3. Uncomment this code:

```
https.createServer({
    key: fs.readFileSync('../encryption/server.key'),
    cert: fs.readFileSync('../encryption/server.cert'),
}, app)
.listen(8080, () => {
    console.log('Server is Running on Port 8080!');
});
```



Make sure that the files ```key``` and ```cert``` are updated.

<br/>

<br/>

# Run on Port 80 (Recommended)

To run the app on Port 80 follow the next steps:

1. Go to the file ```App/bin/app.js``` and scroll down to the bottom
2. Find all instances of ```.listen(8080, () => {``` and change the number to ```80```
3. Change the domain name (read the section ```Changing the Domain``` above in this file) in order to put the new port number
4. Make sure that Port 80 is free. Check if ```Apache``` is not running
5. Restart the App server

<br/>

<br/>

# Change the login lifespan (Optional)

To change the amount of time that a user needs to do login again follow the next steps:

1. Go to the file ```App/API/controllers/adminController.js``` and find this line in the function ```login```: ```jwt.sign({ user: adminObj }, secretKey.secret_key, { expiresIn: '1h' }, (err, token) => {``` and change ```'1h'``` to whatever you want to change. [Guide to the module](https://www.npmjs.com/package/jsonwebtoken) 
2. Repeat the previous step to the files ```App/API/controllers/doctorController.js``` and ```App/API/controllers/patientController.js``` 
3. Go to the file ```App/FrontEnd/Hospital/Backoffice/controllers/loginController.js``` and find this piece of code and change to whatever you need:

```
let expiration = new Date();
const hoursTokenIsValid = 1;
expiration.setTime(expiration.getTime() + (hoursTokenIsValid * 60 * 60 * 1000));
```

4. Repeat the previous step to the files  ```App/FrontEnd/Hospital/Frontoffice/controllers/loginController.js``` and ```App/FrontEnd/Hospital/Patient/controllers/loginController.js```  

<br/>

<br/>

# Backup database automatically (Recommended)

1. Add the MySQL ```bin``` folder to the [windows environment variables](https://www.computerhope.com/issues/ch000549.htm)
2. If the MySQL has a password, change the ```App/API/backupdb.bat``` file at the last line to put the password. The line must look like ```mysqldump -u [user name] -p[password] [database name] > [dump file]```. [Read more about it](https://stackoverflow.com/questions/9293042/how-to-perform-a-mysqldump-without-a-password-prompt)

By the moment if you double click in the file ```App/API/backupdb.bat```a backup will be created and appear at ```App/API/DB_Backups```

If you want to schedule a task to do the backup automatically you need to create a schedule task to execute the file ```App/API/backupdb.bat``` whenever you want. To do it follow the steps in this [Tutorial](https://www.technipages.com/scheduled-task-windows). After you create the schedule task, if you want to edit it, this [link](https://www.digitalcitizen.life/first-steps-working-task-scheduler) might be useful.

