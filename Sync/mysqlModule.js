"use strict";
const mysql = require('mysql');
const database = 'projectdatabase';

// Efetuar uma consulta(query) na base de dados(database)
function queryDB(query, callback) {

    const con = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: database,
        timezone: 'utc'
    });

    con.connect(function (err) {
        if (err) throw err;

        con.query(query, function (err, result) {
            if (err) {
                console.log("ERRO DB: ");
                console.log(err);
                callback(false);
            } else {
                // Imprimir a query realizada na consola
                //console.log('>>>>>Query in \'' + database + '\' database: ' + query);
                callback(result);
            }

        });

        con.end();

    });

}

function queryDBPromise(query) {

    // Promise
    return new Promise(

        function (resolve, reject) {

            const con = mysql.createConnection({
                host: 'localhost',
                user: 'root',
                password: '',
                database: database,
                timezone: 'utc'
            });

            con.connect(function (err) {
                if (err) throw err;

                con.query(query, function (err, result) {
                    if (err) {
                        console.log("ERRO DB: ");
                        console.log(err);
                        con.end();
                        reject(false);
                    } else {
                        // Imprimir a query realizada na consola
                        //console.log('>>>>>Query in \'' + database + '\' database: ' + query);
                        con.end();
                        resolve(result);
                    }

                });

            });

        });

}

exports.queryDB = queryDB;
exports.queryDBPromise = queryDBPromise;