const express = require('express');
const app = express();

const fs = require('fs');

const cron = require('node-cron');


// MY SIGNALS FUNCTIONS

let accessToken;
let reqNumber = 0;

const cronFunctions = require('./cronFunctions.js');

// Cron to connect with the MySignals API and sync the databases
cron.schedule('* * * * *', () => {
    console.log('Updating the sensors measured values!');

    // Increase the request
    reqNumber++;

    // If the access token still does not exist
    if (accessToken === undefined || reqNumber > 5) {

        reqNumber = 0;

        // Get a new access token
        cronFunctions.loginMySignals((token) => {

            console.log("Refreshing access token!");

            if (token !== undefined) {
                accessToken = token;
                updateValues(); // Call function to update values
            }

        });

    } else { // If the access token exists

        // Call function to update values
        updateValues();

    }

});

// Cron to connect with the MySignals API and sync the databases
cron.schedule('*/5 * * * * *', () => {
    console.log('Updating the existing emergencies!');

    // If the access token still does not exist
    if (accessToken === undefined) {

        // Get a new access token
        cronFunctions.loginMySignals((token) => {

            if (token !== undefined) {
                accessToken = token;
            }

        });

    } else { // If the access token exists

        // Get all devices
        cronFunctions.getDevices((devices) => {

            // Get the date of te last emergency
            cronFunctions.getLastDateEmergency((last) => {

                let lastDate;

                // If there is a last emergency
                if (last) {

                    lastDate = last.lastDate;

                } else { // If there isn't any emergency yet

                    lastDate = null;

                }

                let deviceIndex = 0;

                /* For loop with delay between iterations in order to avoid being blocked 
                 * by the mysignals api due to excessive requests at once */
                var interval = setInterval(function () {
                    if (deviceIndex < devices.length) {

                        // Get all data from my signals api for each member and sensor
                        cronFunctions.getDataFromMySignals(accessToken, devices[deviceIndex].id, 'button_ble', lastDate, (returnedData) => {

                            // If there is data                                        
                            if (returnedData.length > 0) {

                                // Iterate over the returned emergencies
                                for (let i = 0; i < returnedData.length; i++) {

                                    // Insert the emergencies in the DB
                                    cronFunctions.insertEmergencyIntoDB(returnedData[i].id, returnedData[i].value, returnedData[i].ts, returnedData[i].sensor_id, returnedData[i].member_id, (inserted) => {

                                        if (inserted) {
                                            //console.log("INSERTED");
                                        } else {
                                            //console.log("DIDNT INSERT");
                                        }

                                    });

                                }

                            }


                        });

                        deviceIndex++;
                    }
                    else {
                        clearInterval(interval);
                    }
                }, 10);


            });

        });


    }

});

// Function to sync the project DB with the MySignals DB
function updateValues() {

    cronFunctions.getDevices((devices) => {
        //console.log(devices[0].id)

        // Get all the existing sensors
        cronFunctions.getSensors((sensors) => {

            // Contains all the sensors and their date of the last measurement
            let sensorsLastDate = {};

            // Iterate all over the sensors array
            for (let i = 0; i < sensors.length; i++) {

                // Get the current sensor iterating
                let sensor = sensors[i].sensor_id;

                // Get the date of the last measurement
                cronFunctions.getLastDateBySensor(sensor, (last) => {

                    // Update the object that contain information about sensors and the date of last measurement
                    sensorsLastDate[sensor] = last[0].lastDate;

                    // When finish to iterate the array of sensors
                    if (i === sensors.length - 1) {

                        let i, j;

                        for (i = 0; i < devices.length; i += 1) {
                            for (j = 0; j < sensors.length; j += 1) {

                                // add function to the queue, shadowing i/j with an IIFE:
                                /* This allows to do a nested for loop with a delay between iterations 
                                 * in order to avoid being blocked by the mysignals api due to excessive requests at once*/
                                delayed(10, function (i, j) {
                                    return function () {

                                        // Get all data from my signals api for each member and sensor
                                        cronFunctions.getDataFromMySignals(accessToken, devices[i].id, sensors[j].sensor_id, sensorsLastDate[sensors[j].sensor_id], (returnedData) => {

                                            // If there is data                                        
                                            if (returnedData && returnedData.length > 0) {

                                                for (let k = 0; k < returnedData.length; k++) {

                                                    cronFunctions.insertMeasurementIntoDB(returnedData[k].id, returnedData[k].value, returnedData[k].ts, returnedData[k].sensor_id, returnedData[k].member_id, (inserted) => {
                                                        if (inserted) {
                                                            //console.log("INSERTED");
                                                        } else {
                                                            //console.log("DIDNT INSERT");
                                                        }
                                                    });

                                                }

                                            }

                                        });

                                    };
                                }(i, j));
                            }
                        }

                    }


                });

            }

        });
    });

}

// helper function
var delayed = (function () {
    var queue = [];

    function processQueue() {
        if (queue.length > 0) {
            setTimeout(function () {
                queue.shift().cb();
                processQueue();
            }, queue[0].delay);
        }
    }

    return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
            processQueue();
        }
    };
}());


// Start server on port 8079
app.listen(8079, () => {
    console.log('Server is Running on Port 8079!');
});
