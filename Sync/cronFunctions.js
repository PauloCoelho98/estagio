"use strict";

const fs = require('fs');
const unirest = require("unirest");
const mysqlModule = require("./mysqlModule.js");

// Login to get an API access token
function loginMySignals(callback) {

    // Read the access credentials from a file
    fs.readFile('./datafiles/mysignalscredentials.json', 'utf8', function (err, credentials) {
        if (err) {
            //throw err;
            console.log(err)
            callback(undefined);
        } else {

            credentials = JSON.parse(credentials);

            const req = unirest("POST", "https://api.libelium.com/mysignals/auth/login");

            req.headers({
                "Accept": "application/x.webapi.v1+json",
                "Content-Type": "application/x-www-form-urlencoded"
            });

            req.form({
                "email": credentials.email,
                "password": credentials.password
            });

            // Make the request
            req.end(function (res) {

                if (res.error) { // No success

                    console.log(res.status)

                    callback(undefined);
                    //throw new Error(res.error);

                } else { // Success

                    callback(res.body.token);

                }

                //console.log(res.body);
                //console.log(res.status)
            });

        }
    });
}

// Get all devices
function getDevices(callback) {

    const query = "SELECT * FROM device;";

    mysqlModule.queryDB(query, (result) => {
        if (result) {
            callback(result);
        } else {
            callback(false);
        }
    });

}

// Get all sensors
function getSensors(callback) {
    const query = "SELECT * FROM sensors;";

    mysqlModule.queryDB(query, (result) => {
        if (result) {
            callback(result);
        } else {
            callback(false);
        }
    });

}

// Get the last measure date for a sensor registed in the internal database
function getLastDateBySensor(sensorId, callback) {

    const query = "SELECT MAX(ts) as lastDate FROM sensors_values WHERE sensor_id='" + sensorId + "' ";

    mysqlModule.queryDB(query, (result) => {
        if (result) {
            callback(result);
        } else {
            callback(false);
        }
    });

}

// Get the last existing date of the emergencies
function getLastDateEmergency(callback) {

    const query = "SELECT MAX(ts_start) as lastDate FROM alert_closed";

    // Get the last date of the closed emergencies
    mysqlModule.queryDB(query, (result) => {
        if (result) {

            const query = "SELECT MAX(ts_start) as lastDate FROM alert_active";

            // Get the last date of the active emergencies
            mysqlModule.queryDB(query, (result2) => {
                if (result2) {

                    // If there aren't results in the closed emergencies and there are results 
                    // in the active emegencies, return the last date of the active emergencies
                    if (result[0].lastDate == null && result2[0].lastDate != null) {

                        callback(result2[0]);

                        // If there are results in the closed emergencies and there aren't results 
                        // in the active emegencies, return the last date of the closed emergencies
                    } else if (result[0].lastDate != null && result2[0].lastDate == null) {

                        callback(result[0]);

                        // If there are results in both active and closed emergencies, compare the last of each one and return the highest
                    } else if (result[0].lastDate != null && result2[0].lastDate != null) {

                        let d1 = result[0].lastDate;
                        let d2 = result2[0].lastDate;

                        if (d1 > d2) {
                            callback(result[0])
                        } else {
                            callback(result2[0])
                        }

                        // If the aren't results in both closed and active emergencies, return false
                    } else {
                        callback(false)
                    }

                }
            });

        }
    });

}

//Get data from my signals from a specific device, a specific sensor and a specific date start
function getDataFromMySignals(token, memberId, sensorId, dateStart, callback) {

    // Get current time and add extra 4 hours to make sure it receives all the data from the start to the present
    let now = new Date();
    now.setHours(now.getHours() + 4);

    // If there isn't a start date, set the start date at 1970
    if (dateStart == null) {
        dateStart = new Date();
        dateStart.setYear(1970);
    }

    // Add one second to don't get repeated measurements from the API
    dateStart.setSeconds(dateStart.getSeconds() + 1)
    dateStart = dateStart.toISOString();
    dateStart = dateStart.split('T')[0] + " " + dateStart.split('T')[1].substring(0, 8);


    now = now.toISOString();
    now = now.split('T')[0] + " " + now.split('T')[1].substring(0, 8);


    //console.log(memberId + ", " + sensorId + ", " + dateStart + ", " + now);

    var req = unirest("GET", "https://api.libelium.com/mysignals/values");

    req.query({
        "sensor_id": sensorId,
        "member_id": memberId,
        "ts_start": dateStart,
        "ts_end": now,
        "order": "desc"
    });

    req.headers({
        "Authorization": "Bearer " + token,
        "Accept": "application/x.webapi.v1+json"
    });


    req.end(function (res) {
        if (res.error) {
            callback(undefined);
            //throw new Error(res.error);

        } else {
            callback(res.body.data);
        }

    });

}

// Check the patient that a device belongs based on a date time
function checkWhoTheMeasurementBelongs(dateTime, memberId, callback) {

    // Query que obtem o id do paciente a partir do id da maquina e a hora
    const query = "SELECT patient_id FROM device_history WHERE device_id='" + memberId + "' AND '" + dateTime + "' > `start` AND ('" + dateTime + "' < `end` OR `end` is NULL) ";

    mysqlModule.queryDB(query, (result) => {
        if (result) {
            if (result[0]) {
                callback(result[0].patient_id);
            } else {
                callback(undefined);
            }
        } else {
            callback(false);
        }
    });

}

// Insert a measurement into the DB
function insertMeasurementIntoDB(id, value, ts, sensorId, memberId, callback) {

    // Check the patient who the measurement belongs to
    checkWhoTheMeasurementBelongs(ts, memberId, (patientId) => {

        // If there is a patient assigned to the device in the time of the measurement
        if (patientId) {

            // INSERT THE MESUREMENT
            let query = "INSERT INTO sensors_values (id, value, ts, sensor_id, member_id) VALUES('" + id + "', '" + value + "', '" + ts.slice(0, 19) + "', '" + sensorId + "', '" + patientId + "');";

            mysqlModule.queryDB(query, (result) => {
                if (result) {
                    callback(true);
                } else {
                    callback(false);
                }
            });

            // CHECK IF THE MEASUREMENT IS UNDER OR OVER THE REGULAR VALUES AND IF SO INSERT IN THE DATABASE

            // Get the limits of the patient for this sensor
            let queryPatientLimits = "SELECT * FROM patient_sensors_limits WHERE patient_id='" + patientId + "' AND sensor_id='" + sensorId + "';";
            mysqlModule.queryDB(queryPatientLimits, (patientLimits) => {

                let min;
                let max;

                // If the patient has limits
                if(patientLimits.length > 0) {
                    min = patientLimits[0].min;
                    max = patientLimits[0].max;
                }

                // If the limits of this patient are undefined, check for the default limits
                if (min == undefined || max == undefined) {

                    let queryLimits = "SELECT * FROM sensors WHERE sensor_id='" + sensorId + "';";

                    mysqlModule.queryDB(queryLimits, (limits) => {

                        min = limits[0].min;
                        max = limits[0].max;

                        if (min == undefined || max == undefined) {
                        } else { // When the  measurement is beyond the limits

                            min = Number(min);
                            max = Number(max);
                            value = Number(value);

                            // Insert into the DB when the measurement is under or above the recommended values 
                            if (value < min || value > max) {
                                //console.log("ALERT: " + "min: " + min + " - max: " + max + " - actual: " + value);

                                let queryInsertAlertLimits = "INSERT INTO alert_signal (id, sensor_id, patient_id, date) VALUES ('" + id + "', '" + sensorId + "', '" + patientId + "', '" + ts.slice(0, 19) + "');";

                                mysqlModule.queryDB(queryInsertAlertLimits, (alertLimInserted) => { });

                            }

                        }
                    });


                } else { // When the  measurement is beyond the limits

                    min = Number(min);
                    max = Number(max);
                    value = Number(value);

                    // Insert into the DB when the measurement is under or above the recommended values 
                    if (value < min || value > max) {
                        //console.log("ALERT: " + "min: " + min + " - max: " + max + " - actual: " + value);

                        let queryInsertAlertLimits = "INSERT INTO alert_signal (id, sensor_id, patient_id, date) VALUES ('" + id + "', '" + sensorId + "', '" + patientId + "', '" + ts.slice(0, 19) + "');";

                        mysqlModule.queryDB(queryInsertAlertLimits, (alertLimInserted) => { });

                    }

                }

            });

        } else {
            callback(false);
        }
    });

}

// Insert an emergency into the DB
function insertEmergencyIntoDB(id, value, ts, sensorId, memberId, callback) {

    // Only adds to the dayabase when the value is 1
    if (value == 1) {

        checkWhoTheMeasurementBelongs(ts, memberId, (patientId) => {

            // If there is a patient assigned to the device in the time of the measurement
            if (patientId) {

                let query = "INSERT INTO alert_active (id, ts_start, patient_id, state) VALUES('" + id + "', '" + ts.slice(0, 19) + "', '" + patientId + "', 'waiting');";

                mysqlModule.queryDB(query, (result) => {
                    if (result) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                });

            } else {
                callback(false);
            }
        });

    }

}

exports.loginMySignals = loginMySignals;
exports.getDevices = getDevices;
exports.getSensors = getSensors;
exports.getLastDateBySensor = getLastDateBySensor;
exports.getDataFromMySignals = getDataFromMySignals;
exports.checkWhoTheMeasurementBelongs = checkWhoTheMeasurementBelongs;
exports.insertMeasurementIntoDB = insertMeasurementIntoDB;

exports.getLastDateEmergency = getLastDateEmergency;
exports.insertEmergencyIntoDB = insertEmergencyIntoDB;