"use strict";
const express = require('express');
const router = express.Router();

const adminController = require('../controllers/adminController');

// Function that checks is a request was made by a super admin
function isSuperAdmin(req, res, next) {
    let role = res.locals.authData.user._role;

    if(role === "S_Admin") {
        next();
    }else{
        res.status(403).json();
    }

}

// Function that checks is a request was made by an admin
function isAdmin(req, res, next) {
    let role = res.locals.authData.user._role;

    if(role === 'S_Admin' || role === "Admin") {
        next();
    }else{
        res.status(403).json();
    }
}

// Function that checks is a request was made by a super admin or an admin that has doctors permissions
function hasDoctorsPermissions(req, res, next) {

    let superAdmin = res.locals.authData.user._role === 'S_Admin';
    let admin = res.locals.authData.user._role === 'Admin';
    let doctors = res.locals.authData.user._permissions.includes("doctors");

    if(superAdmin || (admin && doctors)) {
        next();
    }else{
        res.status(403).json({ "Message": "You don't have permissions" });
    }

}

// Function that checks is a request was made by a super admin or an admin that has patients permissions
function hasPatientsPermissions(req, res, next) {

    let superAdmin = res.locals.authData.user._role === 'S_Admin';
    let admin = res.locals.authData.user._role === 'Admin';
    let patients = res.locals.authData.user._permissions.includes("patients");

    if(superAdmin || (admin && patients)) {
        next();
    }else{
        res.status(403).json({ "Message": "You don't have permissions" });
    }

}

// Function that checks is a request was made by a super admin or an admin that has assigndoctors permissions
function hasAssignDoctorsPermissions(req, res, next) {

    let superAdmin = res.locals.authData.user._role === 'S_Admin';
    let admin = res.locals.authData.user._role === 'Admin';
    let assignDoctors = res.locals.authData.user._permissions.includes("assigndoctors");

    if(superAdmin || (admin && assignDoctors)) {
        next();
    }else{
        res.status(403).json({ "Message": "You don't have permissions" });
    }

}

// Function that checks is a request was made by a super admin or an admin that has contacts permissions
function hasContactsPermissions(req, res, next) {

    let superAdmin = res.locals.authData.user._role === 'S_Admin';
    let admin = res.locals.authData.user._role === 'Admin';
    let contacts = res.locals.authData.user._permissions.includes("contacts");

    if(superAdmin || (admin && contacts)) {
        next();
    }else{
        res.status(403).json({ "Message": "You don't have permissions" });
    }

}

// Function that checks is a request was made by a super admin or an admin that has devices permissions
function hasDevicesPermissions(req, res, next) {

    let superAdmin = res.locals.authData.user._role === 'S_Admin';
    let admin = res.locals.authData.user._role === 'Admin';
    let devices = res.locals.authData.user._permissions.includes("devices");

    if(superAdmin || (admin && devices)) {
        next();
    }else{
        res.status(403).json({ "Message": "You don't have permissions" });
    }

}

// Function that checks is a request was made by a super admin or an admin that has patients or contacts permissions
function hasPatientsOrContactsPermissions(req, res, next) {
    
    let superAdmin = res.locals.authData.user._role === 'S_Admin';
    let admin = res.locals.authData.user._role === 'Admin';
    let patients = res.locals.authData.user._permissions.includes("patients");
    let contacts = res.locals.authData.user._permissions.includes("contacts");

    if(superAdmin || (admin && (patients || contacts))) {
        next();
    }else{
        res.status(403).json({ "Message": "You don't have permissions" });
    }

}

// Function that checks is a request was made by a super admin or an admin that has doctors or assign doctors permissions
function hasDoctorOrAssignDoctorPermissions(req, res, next) {

    let superAdmin = res.locals.authData.user._role === 'S_Admin';
    let admin = res.locals.authData.user._role === 'Admin';
    let doctors = res.locals.authData.user._permissions.includes("doctors");
    let assigndoctors = res.locals.authData.user._permissions.includes("assigndoctors");

    
    if(superAdmin || (admin && (doctors || assigndoctors))) {
        next();
    }else{
        res.status(403).json({ "Message": "You don't have permissions" });
    }

}

// Function that checks is a request was made by a super admin or an admin that has assigndoctors or doctor permissions
function hasPatientOrAssignDoctorPermissions(req, res, next) {

    let superAdmin = res.locals.authData.user._role === 'S_Admin';
    let admin = res.locals.authData.user._role === 'Admin';
    let patients = res.locals.authData.user._permissions.includes("patients");
    let assigndoctors = res.locals.authData.user._permissions.includes("assigndoctors");

    if(superAdmin || (admin && (patients || assigndoctors))) {
        next();
    }else{
        res.status(403).json({ "Message": "You don't have permissions" });
    }

}

// Route that allows the admins to authenticate
router.route('/api/admin/login')
    .post(function (req, res) {
        adminController.login(req, res);
    });

// DOCTORS ROUTES

router.route('/api/admin/doctor')
    .get(hasDoctorOrAssignDoctorPermissions, function (req, res) { // Get all the doctors
        adminController.listDoctors(req, res)
    });

router.route('/api/admin/doctor')
    .post(hasDoctorsPermissions, function (req, res) { // Create a doctor
        adminController.addDoctor(req, res)
    });

router.route('/api/admin/doctor/:id')
    .get(hasDoctorOrAssignDoctorPermissions, function (req, res) { // Get a doctor by id
        adminController.getDoctor(req, res);
    });

router.route('/api/admin/doctor/:id')
    .put(hasDoctorsPermissions, function (req, res) { // Update a doctor by id
        adminController.updateDoctor(req, res);
    });

router.route('/api/admin/doctor/:id')
    .delete(hasDoctorsPermissions, function (req, res) { // Delete a doctor by id
        adminController.deleteDoctor(req, res)
    });

router.route('/api/admin/totaldoctors')
    .get(hasDoctorsPermissions, function (req, res) { // Get the total number of doctors
        adminController.totalDoctors(req, res);
    });


// PATIENTS ROUTES

router.route('/api/admin/patient')
    .get(hasPatientOrAssignDoctorPermissions, function (req, res) { // Get all the patients
        adminController.listPatients(req, res)
    })
    .post(hasPatientsPermissions, function (req, res) { // Create a patient
        adminController.addPatient(req, res)
    });

router.route('/api/admin/patient/:id')
    .get(hasPatientOrAssignDoctorPermissions, function (req, res) { // Get a patient by id
        adminController.getPatient(req, res);
    });

router.route('/api/admin/patient/:id')
    .put(hasPatientsPermissions, function (req, res) { // Update a patient by id
        adminController.updatePatient(req, res);
    });

router.route('/api/admin/patient/:id')
    .delete(hasPatientsPermissions, function (req, res) { // Delete a patient by id
        adminController.deletePatient(req, res)
    });

router.route('/api/admin/totalpatients')
    .get(hasPatientsPermissions, function (req, res) { // Get the total number of patients
        adminController.totalPatients(req, res);
    });

// Get a specific patient picture of the doctor
router.route('/api/admin/patient/:patientId(\\d+)/picture')
    .get(isAdmin, function (req, res) { // Get the picture of a patient by id
        adminController.getPatientPicture(req, res);
    });

// DOCTORPATIENT ROUTES

router.route('/api/admin/doctorpatient')
    .get(hasAssignDoctorsPermissions, function (req, res) { // Get the list of associations between doctor and patients
        adminController.getListOfDoctorPatient(req, res);
    })
    .post(hasAssignDoctorsPermissions, function (req, res) { // Create an association between a doctor and a patient
        adminController.addDoctorPatient(req, res);
    });

router.route('/api/admin/doctorpatient/:doctorId/:patientId')
    .delete(hasAssignDoctorsPermissions, function (req, res) { // Delete an association between a doctor and a patient by their id
        adminController.deleteDoctorPatient(req, res);
    });

// SUPER ADMIN ROUTES

router.route('/api/admin/admin')
    .get(isSuperAdmin, function (req, res) { // Get all the admins
        adminController.listAdmins(req, res)
    })
    .post(isSuperAdmin, function (req, res) { // Create an admin
        adminController.addAdmin(req, res)
    });

router.route('/api/admin/admin/:id')
    .get(isSuperAdmin, function (req, res) { // Get an admin by id
        adminController.getAdmin(req, res);
    });

router.route('/api/admin/admin/:id')
    .put(isSuperAdmin, function (req, res) { // Update an admin by id
        adminController.updateAdmin(req, res);
    });

router.route('/api/admin/admin/:id')
    .delete(isSuperAdmin, function (req, res) { // Delete an admin by id
        adminController.deleteAdmin(req, res)
    });

router.route('/api/admin/totalAdmins')
    .get(isSuperAdmin, function (req, res) { // Get the total number of admins
        adminController.totalAdmins(req, res);
    });


// CONTACTS
router.route('/api/admin/contacts')
    .get(hasPatientsOrContactsPermissions, function (req, res) { // Get all the contacts
        adminController.listContacts(req, res)
    })
    .post(hasContactsPermissions, function (req, res) { // Create a contact
        adminController.addContact(req, res)
    });

router.route('/api/admin/contacts/:id')
    .get(hasPatientsOrContactsPermissions, function (req, res) { // Get a contact by id
        adminController.getContact(req, res);
    });

router.route('/api/admin/contacts/:id')
    .put(hasContactsPermissions,  function (req, res) { // Update a contact by id
        adminController.updateContact(req, res);
    });

router.route('/api/admin/contacts/:id')
    .delete(hasContactsPermissions, function (req, res) { // Delete a contact by id
        adminController.deleteContact(req, res)
    });

router.route('/api/admin/totalcontacts')
    .get(hasContactsPermissions, function (req, res) { // Get the total number of contacts
        adminController.totalContacts(req, res);
    });

// PATIENT CONTACTS

router.route('/api/admin/patientcontacts/:patientId')
    .get(hasPatientsPermissions, function (req, res) { // Get all the contacts of a patient
        adminController.listPatientContacts(req, res)
    })

router.route('/api/admin/patientcontacts/')
    .post(hasPatientsPermissions, function (req, res) { // Add a new contact to a patient
        adminController.addPatientContact(req, res)
    });

router.route('/api/admin/patientcontacts/:patientId/:patientContactId')
    .get(hasPatientsPermissions, function (req, res) { // Get a contact of a patient by their id
        adminController.getPatientContact(req, res);
    });

router.route('/api/admin/patientcontacts/:patientId')
    .delete(hasPatientsPermissions, function (req, res) { // Delete all the contacts of a patient by he's id
        adminController.deletePatientContacts(req, res)
    });

// DEVICES

router.route('/api/admin/device')
    .get(hasDevicesPermissions, function (req, res) {
        adminController.listDevices(req, res); // Get all devices
    })
    .post(hasDevicesPermissions, function(req, res) {
        adminController.addDevice(req, res); // Add a new device
    });

router.route('/api/admin/device/:deviceId')
    .delete(hasDevicesPermissions, function (req, res) {
        adminController.deleteDevice(req, res); // Delete a device
    });

router.route('/api/admin/totaldevices')
    .get(hasDevicesPermissions, function (req, res) {
        adminController.totalDevices(req, res); // Get the total number of devices
    });

// PASSWORD
router.route('/api/admin/password')
    .patch(function(req, res) {
        adminController.changePassword(req, res);
    });

module.exports = router;