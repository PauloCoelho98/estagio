"use strict";
const express = require('express');
const router = express.Router();

const patientController = require('../controllers/patientController');

function isPatient(req, res, next) {
    let role = res.locals.authData.user._role;

    if (role === 'Patient') {
        next();
    } else {
        res.status(403).json();
    }
}

// Route that allows the patients to authenticate
router.route('/api/patient/login')
    .post(function (req, res) {
        patientController.login(req, res);
    });


// PATIENT ROUTES

router.route('/api/patient/patient')
    .get(isPatient, function (req, res) { // Get the patient object
        patientController.getPatient(req, res)
    });

router.route('/api/patient/plan')
    .get(isPatient, function (req, res) { // Get Patient plan
        patientController.getPatientPlan(req, res);
    });

router.route('/api/patient/allsensors')
    .get(isPatient, function (req, res) { // Get all sensors
        patientController.getAllSensors(req, res);
    });

router.route('/api/patient/medicalconsultation')
    .get(isPatient, function (req, res) { // Get patient medical cunsultations
        patientController.getPatientMedicalConsultation(req, res);
    });

router.route('/upload')
    .post(isPatient, function (req, res) { // Uploud a file
        patientController.uploadFile(req, res);
    });

router.route('/api/patient/getfiles')
    .get(isPatient, function (req, res) { // Get all files
        patientController.getFiles(req, res);
    });

router.route('/patient/getfile/:fileId')
    .get(isPatient, function (req, res) { // Get a file by id
        patientController.getFile(req, res);
    });

router.route('/api/patient/doctorlink/:doctorId')
    .get(isPatient, function (req, res) {
        patientController.getDoctorLink(req, res);
    })

// PASSWORD
router.route('/api/patient/password')
    .patch(function (req, res) {
        patientController.changePassword(req, res);
    });

module.exports = router;