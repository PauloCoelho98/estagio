"use strict";
const express = require('express');
const router = express.Router();

const doctorController = require('../controllers/doctorController');

const doctor = require("../models/doctorModule");

// Function that checks is a request was made by a doctor
function isDoctor(req, res, next) {
    let role = res.locals.authData.user._role;

    if (role === 'Doctor') {
        next();
    } else {
        res.status(403).json();
    }
}

function hasDoctorPermissions(req, res, next) {
    if (res.locals.authData.user._role === 'Doctor' && res.locals.authData.user._permissions.includes("doctor")) {
        next();
    } else {
        res.status(403).json();
    }

}

function hasEmergencyPermissions(req, res, next) {

    if (res.locals.authData.user._role === 'Doctor' && res.locals.authData.user._permissions.includes("emergency")) {
        next();
    } else {
        res.status(403).json();
    }

}

// Function that checks in a GET request if the doctor has the patient
function hasPatient(req, res, next) {

    let doctorId = res.locals.authData.user._id;

    if (res.locals.authData.user._permissions.includes('emergency')) {
        next();
    } else {

        doctor.hasPatient(doctorId, req.params.patientId, (hasPatient) => {
            if (hasPatient) {
                next();
            } else {
                res.status(403).json({ "Message": "You don't have permitions for this patient" });
            }
        });
    }

}

// Function that checks in a POST request if the doctor has the patient
function hasPatientPost(req, res, next) {

    let doctorId = res.locals.authData.user._id;

    doctor.hasPatient(doctorId, req.body.patientId, (hasPatient) => {
        if (hasPatient) {
            next();
        } else {
            res.status(403).json({ "Message": "You don't have permitions for this patient" });
        }
    });

}

// Function that checks in a request if the doctor has the view
function hasView(req, res, next) {

    let doctorId = res.locals.authData.user._id;

    doctor.hasView(doctorId, req.params.viewId, (hasView) => {
        if (hasView) {
            next();
        } else {
            res.status(403).json({ "Message": "You don't have permitions for this view" });
        }
    });

}

// Function that checks in a request if the doctor has the medical consultation
function hasMedicalConsultation(req, res, next) {

    let doctorId = res.locals.authData.user._id;

    doctor.hasMedicalConsultation(doctorId, req.params.idconsultation, (hasView) => {
        if (hasView) {
            next();
        } else {
            res.status(403).json({ "Message": "You don't have permitions for this medical consultation" });
        }
    });

}

// Route that allows the doctors to authenticate
router.route('/api/doctor/login')
    .post(function (req, res) {
        doctorController.login(req, res);
    });


// DOCTORS ROUTES

router.route('/api/doctor/')
    .get(isDoctor, function (req, res) { // Get an object of himself
        doctorController.getDoctor(req, res);
    });


// DOCTOR PATIENTS

router.route('/api/doctor/patients')
    .get(hasDoctorPermissions, function (req, res) { // Get all the patients of the doctor
        doctorController.getDoctorPatients(req, res);
    });

router.route('/api/doctor/totalPatients')
    .get(hasDoctorPermissions, function (req, res) { // Get the total patients of the doctor
        doctorController.getTotalPatients(req, res);
    });

router.route('/api/doctor/patient/:patientId(\\d+)/')
    .get(isDoctor, hasPatient, function (req, res) { // Get a specific patient of the doctor
        doctorController.getPatient(req, res);
    });

router.route('/api/doctor/patient/:patientId(\\d+)/picture')
    .get(hasDoctorPermissions, hasPatient, function (req, res) { // Get a specific patient picture of the doctor
        doctorController.getPatientPicture(req, res);
    });

router.route('/api/doctor/patient/:patientId(\\d+)/allvalues')
    .get(hasDoctorPermissions, hasPatient, function (req, res) { // Get all values measured by sensors of a patient of this doctor
        doctorController.getPatientAllValues(req, res);
    });

router.route('/api/doctor/patient/:patientId(\\d+)/values/:sensor')
    .get(hasDoctorPermissions, hasPatient, function (req, res) { // Get all values measured by one sensor of a patient of this doctor
        doctorController.getPatientValuesBySensor(req, res);
    });


// SENSORS

router.route('/api/doctor/getSensors')
    .get(hasDoctorPermissions, function (req, res) { // Get all the existing sensors
        doctorController.getSensors(req, res);
    });


// PATIENT DEVICES

router.route('/api/doctor/assigndevice')
    .post(hasDoctorPermissions, function (req, res) { // Assign a device to a patient
        doctorController.assignDevice(req, res);
    });

router.route('/api/doctor/getdevices')
    .get(hasDoctorPermissions, function (req, res) { // Get the existing devices
        doctorController.getDevices(req, res);
    });

router.route('/api/doctor/removeassigneddevice/:patientId(\\d+)/')
    .delete(hasDoctorPermissions, hasPatient, function (req, res) { // Remove an assigned device
        doctorController.removeAssignedDevice(req, res);
    });


// DOCTOR VIEWS

router.route('/api/doctor/views')
    .get(hasDoctorPermissions, function (req, res) { // Get all the doctor views
        doctorController.getViews(req, res);
    });

router.route('/api/doctor/views')
    .post(hasDoctorPermissions, function (req, res) { // Add a doctor view
        doctorController.newView(req, res);
    });

router.route('/api/doctor/views/:viewId(\\d+)/')
    .delete(hasDoctorPermissions, hasView, function (req, res) { // Delete a doctor view
        doctorController.deleteView(req, res);
    })
    .put(hasDoctorPermissions, hasView, function (req, res) { // Update a doctor view
        doctorController.updateView(req, res);
    });


// PATIENT MEDICAL CONSULTATION

router.route('/api/doctor/medicalconsultation')
    .get(hasDoctorPermissions, function (req, res) { // Get all medical consultation
        doctorController.getAllMedicalConsultations(req, res);
    })
    .post(hasDoctorPermissions, function (req, res) { // Create a medical consultation
        doctorController.createMedicalConsultation(req, res);
    });

router.route('/api/doctor/medicalconsultation/:idconsultation(\\d+)')
    .get(hasDoctorPermissions, hasMedicalConsultation, function (req, res) { // Get a medical consultation
        doctorController.getMedicalConsultation(req, res);
    })
    .put(hasDoctorPermissions, hasMedicalConsultation, function (req, res) { // Update a medical consultation
        doctorController.updateMedicalConsultation(req, res);
    })
    .delete(hasDoctorPermissions, hasMedicalConsultation, function (req, res) { // Delete a medical consultation
        doctorController.deleteMedicalConsultation(req, res);
    });

router.route('/api/doctor/medicalconsultation/patient/:patientId(\\d+)')
    .get(hasDoctorPermissions, hasPatient, function (req, res) { // Get all medical consultation of one patient
        doctorController.getAllMedicalConsultationsByPatient(req, res);
    })

router.route('/api/doctor/totalmedicalconsultation')
    .get(hasDoctorPermissions, function (req, res) { // Get the total medical consultations of one doctor
        doctorController.getMedicalConsultationTotal(req, res);
    });

// PATIENT MONITORING SENSORS

router.route('/api/doctor/patientmonitoringsensors/:patientId(\\d+)/')
    .get(hasDoctorPermissions, hasPatient, function (req, res) { // List all monitoring sensors of one patient
        doctorController.listPatientMonitoringSensors(req, res)
    })

router.route('/api/doctor/patientmonitoringsensors/')
    .post(hasDoctorPermissions, function (req, res) { // Add one sensor for monitorization to a patient
        doctorController.addPatientMonitoringSensors(req, res)
    });

router.route('/api/doctor/patientmonitoringsensors/:patientId(\\d+)/')
    .delete(hasDoctorPermissions, hasPatient, function (req, res) { // Delete all monitoring sensors of one patient
        doctorController.deletePatientMonitoringSensors(req, res)
    });


// PATIENT EMERGENCIES

router.route('/api/doctor/emergencies')
    .get(hasEmergencyPermissions, function (req, res) { // Get all active emergencies
        doctorController.getListOfActiveEmergencies(req, res);
    });

router.route('/api/doctor/activeemergency/:id(\\d+)/')
    .get(hasEmergencyPermissions, function (req, res) { // Get a specific active emergency
        doctorController.getActiveEmergergencyById(req, res);
    });

router.route('/api/doctor/respondingemergencies')
    .get(hasEmergencyPermissions, function (req, res) { // Get all responding emergencies of one doctor
        doctorController.getListOfRespondingEmergencies(req, res);
    });

router.route('/api/doctor/respondtoemergency')
    .post(hasEmergencyPermissions, function (req, res) { // Respond to an emergency
        doctorController.respondToEmergency(req, res);
    });

router.route('/api/doctor/closeemergency')
    .post(hasEmergencyPermissions, function (req, res) { // Close an emergency
        doctorController.closeEmergency(req, res);
    });

router.route('/api/doctor/totalemergencies')
    .get(hasEmergencyPermissions, function (req, res) {
        doctorController.getTotalEmergencies(req, res);
    });


// PATIENT CONTACTS

router.route('/api/doctor/patient/:id(\\d+)/contacts')
    .get(isDoctor, function (req, res) { // Get all patient contacts
        doctorController.getPatientContacts(req, res);
    });


// PATIENT PLANS

router.route('/api/doctor/patient/:patientId(\\d+)/plan')
    .get(hasDoctorPermissions, hasPatient, function (req, res) { // Get a patient plan
        doctorController.getPatientPlan(req, res);
    })
    .delete(hasDoctorPermissions, hasPatient, function (req, res) { // Delete a patient plan
        doctorController.deletePatientPlan(req, res);
    })

router.route('/api/doctor/patient/plan')
    .post(hasDoctorPermissions, hasPatientPost, function (req, res) { // Create a plan to the patient
        doctorController.createPatientPlan(req, res);
    });


// PATIENT FILES

router.route('/api/doctor/patient/:patientId(\\d+)/files')
    .get(hasDoctorPermissions, hasPatient, function (req, res) { // Get all files of a patient by id
        doctorController.getPatientFiles(req, res);
    });

router.route('/doctor/patient/:patientId(\\d+)/getfile/:fileId')
    .get(hasDoctorPermissions, function (req, res) { // Get a file of apatient by their id
        doctorController.getPatientFile(req, res);
    });

// SIGNALS ALERTS

router.route('/api/doctor/patient/:patientId(\\d+)/alertsignals')
    .get(hasDoctorPermissions, hasPatient, function(req, res){ // Get the alerts of signals
        doctorController.listSignalAlerts(req, res);
    });

router.route('/api/doctor/alertsignals/:alertsignalId(\\d+)')
    .delete(hasDoctorPermissions, function(req, res) { // Delete an alert of a signal
        doctorController.deleteSignalAlert(req, res);
    });


// PASSWORD
router.route('/api/doctor/password')
    .patch(function(req, res) { // Update the password of the doctor
        doctorController.changePassword(req, res);
    });

// CHANGE PATIENT SENSORS LIMITS

router.route('/api/doctor/patient/:patientId(\\d+)/sensorslimits')
    .get(hasDoctorPermissions, hasPatient, function(req, res) { // Get the limits of the sensors of a patient
        doctorController.getPatientLimits(req, res);
    })
    .put(hasDoctorPermissions, hasPatient, function(req, res){ // Change the limits of the sensors of a patient
        doctorController.updatePatientLimits(req, res);
    });


module.exports = router;