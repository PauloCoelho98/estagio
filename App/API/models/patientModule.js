"use strict";

const mysqlModule = require("./mysqlModule");
const bcrypt = require('bcrypt');
const Result = require('./resultModule');

/**
 * Class which represents a Patient
 */
class Patient {

    constructor(id, email, name, contact, picture, description, height, weight, birthday, address, currentDeviceId) {
        this._id = id;
        this._email = email;
        this._name = name;
        this._contact = contact;
        this._picture = picture;
        this._description = description;
        this._height = height;
        this._weight = weight;
        this._birthday = birthday;
        this._address = address;
        this._currentDeviceId = currentDeviceId;
    }

    get id() {
        return this._id;
    }
    get email() {
        return this._email;
    }
    get name() {
        return this._name;
    }
    get contact() {
        return this._contact;
    }
    get picture() {
        return this._picture;
    }
    get description() {
        return this._description;
    }
    get height() {
        return this._height;
    }
    get weight() {
        return this._weight;
    }
    get birthday() {
        return this._birthday;
    }
    get address() {
        return this._address;
    }
    get currentDeviceId() {
        return this._currentDeviceId;
    }

    set id(id) {
        this._id = id;
    }
    set email(email) {
        this._email = email;
    }
    set name(name) {
        this._name = name;
    }
    set contact(contact) {
        this._contact = contact;
    }
    set picture(picture) {
        this._picture = picture;
    }
    set description(description) {
        this._description = description;
    }
    set height(height) {
        this._height = height;
    }
    set weight(weight) {
        this._weight = weight;
    }
    set birthday(birthday) {
        this._birthday = birthday;
    }
    set address(address) {
        this._address = address;
    }
    set currentDeviceId(currentDeviceId) {
        this._currentDeviceId = currentDeviceId;
    }

    // Verify user credentials to log in
    static loginPatient(email, password, callback) {

        const querySearch = "SELECT * FROM patients WHERE email='" + email + "' ORDER BY id;";

        // Get user
        mysqlModule.queryDB(querySearch, (result) => {

            // if there is no such user, can't login
            if (result.length < 1) {

                callback(false);

                // if the user exists
            } else {

                // Instance of the patient
                const patient = new Patient(result[0].id, result[0].email, result[0].name, result[0].contact, result[0].picture, result[0].description, result[0].height, result[0].weight, result[0].birthday, result[0].address, result[0].current_device_id);

                // Compare the real password with a password entered
                bcrypt.compare(password, result[0].password, function (err, response) {

                    // if password match
                    if (response === true) {

                        // callback a valid login and the admin object
                        callback(true, patient);

                    } else {
                        callback(false);
                    }

                });
            }
        });
    }

    // Get list of all patients in the database
    static getListOfPatients(callback) {

        const querySearch = "SELECT * FROM patients ORDER BY id;";

        mysqlModule.queryDB(querySearch, (result) => {

            // If there are doctors in the database
            if (result.length >= 1) {

                let patients = [];

                // Iterate all over the results array
                for (let i = 0; i < result.length; i++) {

                    let patient = new Patient(result[i].id, result[i].email, result[i].name, result[i].contact,
                        result[i].picture, result[i].description, result[i].height,
                        result[i].weight, result[i].birthday, result[i].address, result[i].current_device_id);

                    patients.push(patient);

                }

                callback(patients);

            } else {
                callback([]);
            }
        });

    }

    // Get a patient by id
    static getPatient(id, callback) {

        const querySearch = "SELECT * FROM patients WHERE id='" + id + "' ORDER BY id;";

        mysqlModule.queryDB(querySearch, (result) => {

            // If there are doctors in the database
            if (result.length === 1) {

                // Instance of the patient
                let patient = new Patient(result[0].id, result[0].email, result[0].name, result[0].contact,
                    result[0].picture, result[0].description, result[0].height,
                    result[0].weight, result[0].birthday, result[0].address, result[0].current_device_id);

                callback(patient);

            } else {

                callback(false);

            }
        });

    }

    // Insert a new patient into database
    static signin(patient, password, callback) {

        let email = patient._email;
        let name = patient._name;
        let contact = patient._contact;
        let picture = patient._picture;
        let description = patient._description;
        let height = patient._height;
        let weight = patient._weight;
        let birthday = patient._birthday;
        let address = patient._address;

        const saltRounds = 10;

        // hash and salt the password
        bcrypt.genSalt(saltRounds, function (err, salt) {
            bcrypt.hash(password, salt, function (err, hash) {
                password = hash;

                const queryInsert = "INSERT INTO patients (email, password, name, contact, picture, description, height, weight, birthday, address) " +
                    " VALUES ('" + email + "', '" + password + "', '" + name + "', '" + contact + "', '" + picture +
                    "', '" + description + "', '" + height + "', '" + weight +
                    "', '" + birthday + "', '" + address + "')";

                // Insert doctor data
                mysqlModule.queryDB(queryInsert, (result) => {

                    if (result) {
                        let patient = new Patient(result.insertId, email, name, contact, picture, description, height, weight, birthday, address, undefined);
                        callback(patient);
                    } else {
                        callback(false);
                    }
                });

            });
        });

    }

    // Delete the patient
    static deletePatient(id, callback) {
        const querySearch = "DELETE FROM patients WHERE id='" + id + "';";

        mysqlModule.queryDB(querySearch, (result) => {
            if (result.affectedRows === 0) {
                callback(false);
            } else {
                callback(result);
            }

        });
    }

    // Update a patient
    static updatePatient(patient, callback) {

        let id = patient._id;
        let email = patient._email;
        let name = patient._name;
        let contact = patient._contact;
        let picture = patient._picture;
        let description = patient._description;
        let height = patient._height;
        let weight = patient._weight;
        let birthday = patient._birthday;
        let address = patient._address;

        let query;

        // If the picture is undefined, leave the field empty in the query
        if (picture === undefined) {

            query = "UPDATE patients SET email='" + email + "', name='" + name + "', contact='" + contact
                + "', description='" + description + "', height='" + height +
                "', weight='" + weight + "', birthday='" + birthday + "', address='" + address + "' WHERE id='" + id + "';";

        } else {

            query = "UPDATE patients SET email='" + email + "', name='" + name + "', contact='" + contact +
                "', picture='" + picture + "', description='" + description + "', height='" + height +
                "', weight='" + weight + "', birthday='" + birthday + "', address='" + address + "' WHERE id='" + id + "';";


        }
        mysqlModule.queryDB(query, (result) => {
            if (result.affectedRows === 0) {
                callback(false);
            } else {
                callback(result);
            }

        });

    }

    // Get the total number of patients
    static totalPatients(callback) {
        const query = "SELECT COUNT(*) as total FROM patients";

        mysqlModule.queryDB(query, (result) => {

            if (result) {
                callback(result[0].total);
            } else {
                callback(false);
            }

        });

    }

    // Get all values of all sensors of a patient
    static getAllValues(patientId, callback) {
        const query = "SELECT * FROM sensors_values WHERE member_id='" + patientId + "' ORDER BY ts;"

        mysqlModule.queryDB(query, (result) => {

            if (result) {
                callback(result);
            } else {
                callback(false);
            }

        });

    }

    // Get all values of a sensor of a patient
    static getValuesBySensor(patientId, sensor, callback) {
        const query = "SELECT * FROM sensors_values WHERE member_id='" + patientId + "' AND sensor_id='" + sensor + "' ORDER BY ts;"

        mysqlModule.queryDB(query, (result) => {

            if (result) {
                callback(result);
            } else {
                callback(false);
            }

        });
    }

    // Get a list of the patient contacts
    static async listPatientContacts(patientId, callback) {

        const query = "SELECT * FROM patient_contacts WHERE patient_id='" + patientId + "' ORDER BY id;";

        // Get all the patient contacts
        let result = await mysqlModule.queryDBPromise(query);

        if (result.length > 0) {
            // Iterate over the contacts of the patient
            for (let i = 0; i < result.length; i++) {

                let inside = false;

                // If the contact_id is not null, then it is a standard contact
                if (result[i].contact_id != null) {

                    inside = true;

                    const query2 = "SELECT * FROM contacts WHERE id='" + result[i].contact_id + "' ORDER BY id;";

                    // Get the standard contact
                    let result2 = await mysqlModule.queryDBPromise(query2);
                    result[i]['contact'] = result2[0];

                    // Callback when reaches to the end
                    if (i === result.length - 1) {
                        callback(result);
                    }

                }

                // callback when reaches to the end and if ti's not inside the if statment
                if (i === result.length - 1 && inside === false) {
                    callback(result);
                }

            }
        } else {
            callback([]);
        }


    }

    // Get a patient contact
    static getPatientContact(patientId, patientContactId, callback) {

        const query = "SELECT * FROM patient_contacts WHERE patient_id='" + patientId + "' AND id='" + patientContactId + "' ORDER BY id;";

        mysqlModule.queryDB(query, (result) => {

            if (result) {

                let inside = false;

                // If the contact_id is not null, then it is a standard contact
                if (result[0].contact_id != null) {

                    inside = true;

                    const query2 = "SELECT * FROM contacts WHERE id='" + result[0].contact_id + "' ORDER BY id;";

                    // Get the standard contact
                    mysqlModule.queryDB(query2, (result2) => {

                        result[0]['contact'] = result2[0];

                        callback(result);

                    });

                }

                if (inside === false) {
                    callback(result);
                }



            } else {
                callback(false);
            }

        });

    }

    // Add a contact to the patient
    static addPatientContact(patientId, contactId, name, number, callback) {

        // If this condition matches, then it is an 'own contact'
        if (contactId === undefined && name !== undefined && number !== undefined) {

            const query = "INSERT INTO patient_contacts (patient_id, name, number) VALUES ('" + patientId + "', '" + name + "', '" + number + "');";

            mysqlModule.queryDB(query, (result) => {

                callback({ patientId, name, number });

            });



            // If this condition matches, then it is a 'standard contact'
        } else if (contactId !== undefined && name === undefined && number === undefined) {

            const query = "INSERT INTO patient_contacts (patient_id, contact_id) VALUES ('" + patientId + "', '" + contactId + "');";

            mysqlModule.queryDB(query, (result) => {

                callback({ patientId, contactId });

            });

        }

    }

    // Delete all patient contacts
    static deletePatientContacts(patientId, callback) {

        const query = "DELETE FROM patient_contacts WHERE patient_id='" + patientId + "';";

        mysqlModule.queryDB(query, (result) => {

            if (result.affectedRows > 0) {
                callback(true);
            } else {
                callback(false);
            }

        });

    }

    // List the monitoring sensors of a patient
    static listPatientMonitoringSensors(patientId, callback) {

        const query = "SELECT * FROM patient_monitoring_sensors WHERE patient_id='" + patientId + "' ORDER BY id;";

        mysqlModule.queryDB(query, (result) => {
            if (result) {
                callback(result);
            } else {
                callback(false);
            }
        });

    }

    // Add a sensor to the monitoring sensors of a patient
    static addPatientMonitoringSensors(patientId, sensorId, callback) {

        const query = "INSERT INTO patient_monitoring_sensors (patient_id, sensor_id) VALUES ('" + patientId + "', '" + sensorId + "');";

        mysqlModule.queryDB(query, (result) => {
            if (result) {
                let id = result.insertId;
                callback({ id: id, patientId: patientId, sensorId: sensorId });
            } else {
                callback(false);
            }
        });


    }

    // Delete all monitoring sensors of the patient
    static deletePatientMonitoringSensors(patientId, callback) {

        const query = "DELETE FROM patient_monitoring_sensors WHERE patient_id='" + patientId + "';";

        mysqlModule.queryDB(query, (result) => {

            if (result.affectedRows > 0) {
                callback(true);
            } else {
                callback(false);
            }

        });

    }

    // Get a patient plan
    static getPatientPlan(patientId, callback) {
        const query = "SELECT * FROM patient_plan WHERE patient_id='" + patientId + "' ORDER BY day_week, sensor_id, time;";

        mysqlModule.queryDB(query, (result) => {
            if (result) {
                callback(result);
            } else {
                callback(false);
            }
        });

    }

    // Create a new plan to the patient
    static async createPatientPlan(patientId, plan, callback) {

        let resultFinal = true;

        for (let i = 0; i < plan.length; i++) {

            const query = "INSERT INTO patient_plan (patient_id, sensor_id, day_week, time) VALUES ('" + patientId + "', '" + plan[i].sensorId + "', '" + plan[i].dayWeek + "', '" + plan[i].time + "');";

            let result = await mysqlModule.queryDBPromise(query);

            if (!result) {
                resultFinal = false;
            }

        }

        if (resultFinal) {
            callback(true);
        } else {
            callback(false);
        }

    }

    // Delete the plan of a patient
    static deletePatientPlan(patientId, callback) {

        const query = "DELETE FROM patient_plan WHERE patient_id='" + patientId + "';";

        mysqlModule.queryDB(query, (result) => {

            if (result.affectedRows > 0) {
                callback(true);
            } else {
                callback(false);
            }

        });
    }

    // Insert a file info in the db
    static insertFile(patientId, fileName, description, date, callback) {

        const query = "INSERT INTO patient_files (patient_id, file_name, description, date) values('" + patientId + "', '" + fileName + "', '" + description + "', '" + date + "');";

        mysqlModule.queryDB(query, (result) => {
            if (result) {
                callback(result.insertId);
            } else {
                callback(false);
            }
        });

    }

    // Get all files info of a patient
    static getFiles(patientId, callback) {

        const query = "SELECT * FROM patient_files WHERE patient_id='" + patientId + "' ORDER BY id;";

        mysqlModule.queryDB(query, (result) => {
            callback(result);
        });

    }

    // Get a file info of the db
    static getFile(patientId, fileId, callback) {
        const query = "SELECT * FROM patient_files WHERE patient_id='" + patientId + "' AND id='" + fileId + "';";

        mysqlModule.queryDB(query, result => {
            if (result.length > 0) {
                callback(result[0]);
            } else {
                callback(false);
            }
        });

    }

    // Get all existing sensors
    static getSensors(callback) {
        const query = "SELECT * FROM sensors ORDER BY sensor_id;";

        mysqlModule.queryDB(query, (result) => {
            if (result) {
                callback(result);
            } else {
                callback(false);
            }
        });

    }

    static getDoctorLink(doctorId, callback) {

        const query = "SELECT link_call FROM doctors WHERE id ='" + doctorId + "';";

        mysqlModule.queryDB(query, (result) => {
            if (result.length > 0) {
                callback(result[0]);
            } else {
                callback(false);
            }
        })

    }

    // Change the password of the admin
    static changePassword(id, oldPassword, newPassword, callback) {

        mysqlModule.selectOneDB("patients", "*", "id='" + id + "'", (result) => {

            if (result._result) {
                const storedPassword = result._object.password;

                // Compare the real password with the old password inserted
                bcrypt.compare(oldPassword, storedPassword, function (err, response) {

                    // if password match
                    if (response === true) {

                        const saltRounds = 10;

                        // hash and salt the new password
                        bcrypt.genSalt(saltRounds, function (err, salt) {
                            bcrypt.hash(newPassword, salt, function (err, hash) {
                                newPassword = hash;

                                mysqlModule.updateDB("patients", { password: newPassword }, "id='" + id + "'", (updated) => {
                                    if (updated._result) {
                                        callback(true);
                                    } else {
                                        callback(false);
                                    }
                                });

                            });

                        });

                    } else {
                        callback(false);
                    }

                });

            } else {
                callback(false);
            }


        })

    }

    static getPatientLimits(patientId, callback) {

        mysqlModule.selectListDB("patient_sensors_limits", "*", "patient_id='" + patientId + "'", undefined, (resultDB)=>{

            callback(resultDB)

        });

    }

    static updatePatientLimits(patientId, limits, callback) {

        mysqlModule.deleteDB("patient_sensors_limits", "patient_id='" + patientId + "'", (resultDB)=>{

            for(let i=0; i<limits.length; i++){

                let dataToInsert = limits[i];
                //console.log(dataToInsert)
                dataToInsert["patient_id"] = patientId;

                mysqlModule.insertDB("patient_sensors_limits", dataToInsert, (resultDBInsert)=>{

                    if(i==limits.length-1) {
                        callback(resultDBInsert);
                    }

                });
            }


        });

    }


}
module.exports = Patient;