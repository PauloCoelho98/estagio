"use strict";

const mysqlModule = require("./mysqlModule");
const Result = require('./resultModule');

/**
 * Class which represents a doctorPatient
 */
class doctorPatient {

    constructor(doctorId, patientId) {
        this._doctorId = doctorId;
        this._patientId = patientId;
    }

    get doctorId() {
        return this._doctorId;
    }
    
    get patientId() {
        return this._patientId;
    }

    set doctorId(doctorId) {
        this._doctorId = doctorId;
    }

    set patientId(patientId) {
        this._patientId = patientId;
    }

    // Get list of all doctorPatient in the database
    static getListOfDoctorPatient(callback) {

        mysqlModule.selectListDB("doctors_patients", "*", undefined, "doctor_id", (resultDB)=>{
            callback(resultDB);
        });

    }

    // Insert a new doctorPatient into database
    static addDoctorPatient(doctorId, patientId, callback) {

        const dataToInsert = {
            "doctor_id": doctorId,
            "patient_id": patientId
        }

        mysqlModule.insertDB("doctors_patients", dataToInsert, (resultDB)=>{
            callback(resultDB)
        });

    }

    // Delete the doctorPatient
    static deleteDoctorPatient(doctorId, patientId, callback) {

        mysqlModule.deleteDB("doctors_patients", "doctor_id='" + doctorId + "' and patient_id='" + patientId + "'", (resultDB)=>{
            callback(resultDB);
        });
        
    }

}
module.exports = doctorPatient;