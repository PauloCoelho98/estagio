"use strict";

const mysqlModule = require("./mysqlModule");
const bcrypt = require('bcrypt');
const Result = require('./resultModule');

/**
 * Class which represents an Emergency
 */
class Emergency {

    constructor(id, tsStart, tsResponse, patientId, doctorId, state) {
        this._id = id;
        this._tsStart = tsStart;
        this._tsResponse = tsResponse;
        this._patientId = patientId;
        this._doctorId = doctorId;
        this._state = state;
    }

    get id() {
        return this._id;
    }

    get tsStart() {
        return this._tsStart;
    }

    get tsResponse() {
        return this._tsResponse;
    }

    get patientId() {
        return this._patientId;
    }

    get doctorId() {
        return this._doctorId;
    }

    get state() {
        return this._state;
    }

    set id(id) {
        this._id = id;
    }

    set tsStart(tsStart) {
        this._tsStart = tsStart;
    }

    set tsResponse(tsResponse) {
        this._tsResponse = tsResponse;
    }

    set patientId(patientId) {
        this._patientId = patientId;
    }

    set doctorId(doctorId) {
        this._doctorId = doctorId;
    }

    set state(state) {
        this._state = state;
    }

    // Get list of all active emergencies in the database
    static getListOfActiveEmergencies(callback) {

        mysqlModule.selectListDB("alert_active", "*", "state='waiting'", "id", (resultDB)=>{
            callback(resultDB);
        });

    }

    // Get a list of responding emergencies
    static getListOfRespondingEmergencies(doctorId, callback) {

        mysqlModule.selectListDB("alert_active", "*", "doctor_id='" + doctorId + "' AND state='responding'", "id", (resultDB)=>{
            callback(resultDB)
        });

    }

    // Respond to an emergency
    static respondToEmergency(emergencyId, date, doctorId, callback) {

        const query = "UPDATE alert_active SET ts_response='" + date + "', doctor_id='" + doctorId + "', state='responding' WHERE id='" + emergencyId + "' AND doctor_id IS NULL;";

        mysqlModule.queryDB(query, (result) => {
            if (result && result.affectedRows > 0) {
                callback(new Result(true, 200, undefined, undefined));
            } else {
                callback(new Result(false, 500, undefined, "something went wrong"));
            }
        });

    }

    // Close an emergency
    static closeEmergency(emergencyId, date, doctorId, callback) {

        const querySelect = "SELECT * FROM alert_active WHERE id='" + emergencyId + "' AND doctor_id='" + doctorId + "' ORDER BY id;";

        // Check if the emergency exists and belongs to the doctor who wants to close it
        mysqlModule.queryDB(querySelect, (resultSelect) => {

            if (resultSelect.length>0) {

                // Instance of the emergency
                let emergency = new Emergency(resultSelect[0].id, resultSelect[0].ts_start, resultSelect[0].ts_response, resultSelect[0].patient_id, resultSelect[0].doctor_id, resultSelect[0].state);

                emergency._tsStart = emergency._tsStart.toISOString().slice(0, 19).replace('T', ' ');
                emergency._tsResponse = emergency._tsResponse.toISOString().slice(0, 19).replace('T', ' ');

                const queryDelete = "DELETE FROM alert_active WHERE id='" + emergency._id + "' AND doctor_id='" + emergency._doctorId + "';";

                // Delete the emegency from the active emergencies table
                mysqlModule.queryDB(queryDelete, (resultDeleted) => {
                    if (resultDeleted && resultDeleted.affectedRows > 0) {

                        const queryInsert = "INSERT INTO alert_closed (id, ts_start, ts_response, ts_end, patient_id, doctor_id) VALUES ('" + emergency._id + "', '" + emergency._tsStart + "', '" + emergency.tsResponse + "', '" + date + "', '" + emergency._patientId + "', '" + emergency._doctorId + "');";

                        // Insert the emergency in the table of closed emergencies
                        mysqlModule.queryDB(queryInsert, (resultInsert) => {

                            if (resultInsert) {
                                callback(new Result(true, 200, undefined, undefined));
                            } else {
                                callback(new Result(false, 500, undefined, "Something went wrong"));
                            }

                        });

                    } else {
                        callback(new Result(false, 500, undefined, "Something went wrong"));
                    }
                });

            }else{
                callback(new Result(false, 404, undefined, "Emergency not found"));
            }

        });

    }

    // Get an active emergency by id
    static getActiveEmergergencyById(emergencyId, callback) {

        mysqlModule.selectOneDB("alert_active", "*", "id='" + emergencyId + "'", (resultDB)=>{
            callback(resultDB)
        });

    }

    // Get the total number of emergencies
    static getTotalEmergencies(doctorId, callback) {

        mysqlModule.getCountDB("alert_active", "total", "doctor_id IS NULL OR doctor_id='" + doctorId + "'", (resultDB)=>{
            callback(resultDB)
        });

    }

}
module.exports = Emergency;