"use strict";
const mysql = require('mysql');
const database = 'projectdatabase';

const Result = require('./resultModule');

// Efetuar uma consulta(query) na base de dados(database)
function queryDB(query, callback) {

    const con = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: database,
        timezone: 'utc'
    });

    con.connect(function (err) {
        if (err) throw err;

        con.query(query, function (err, result) {
            if (err) {
                console.log("ERRO DB: ");
                console.log(err);
                callback(false);
            } else {
                // Imprimir a query realizada na consola
                //console.log('>>>>>Query in \'' + database + '\' database: ' + query);
                callback(result);
            }

        });

        con.end();

    });

}

function queryDBPromise(query) {

    // Promise
    return new Promise(

        function (resolve, reject) {

            const con = mysql.createConnection({
                host: 'localhost',
                user: 'root',
                password: '',
                database: database,
                timezone: 'utc'
            });

            con.connect(function (err) {
                if (err) throw err;

                con.query(query, function (err, result) {
                    if (err) {
                        console.log("ERRO DB: ");
                        console.log(err);
                        con.end();
                        reject(false);
                    } else {
                        // Imprimir a query realizada na consola
                        //console.log('>>>>>Query in \'' + database + '\' database: ' + query);
                        con.end();
                        resolve(result);
                    }

                });

            });

        });

}

/**
 * Function to insert a row in a table of the DB
 * @param {*} tableName the name of the table to insert data
 * @param {*} keyValues an Object containing the key and the value to store in the db
 * @param {*} callback response after success or failure
 */
function insertDB(tableName, keyValues, callback) {
console.log(keyValues)
    let keys = [];
    let values = [];

    // Iterate the object properties and put the property and the value in separated arrays
    for (var key in keyValues) {
        if (keyValues.hasOwnProperty(key)) {
            keys.push(key);
            values.push(keyValues[key]);
        }
    }

    // Add the 'INSERT INTO' string to the query
    let query = "INSERT INTO " + tableName + " (";

    // Insert the properties in the query
    for (let i = 0; i < keys.length; i++) {
        query = query + keys[i] + ", ";
    }

    // Remove the last space and comma
    query = query.slice(0, -2);

    // Add the 'VALUES' string to the query
    query = query + ") VALUES ('";

    for (let i = 0; i < values.length; i++) {
        query = query + values[i] + "', '";
    }

    // Remove the last space and comma
    query = query.slice(0, -3);

    // Add the final string to complete the query
    query = query + ");";


    const con = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: database,
        timezone: 'utc'
    });

    // Connectio to the database
    con.connect(function (err) {

        // If failed to connect
        if (err) {
            callback(new Result(false, 500, undefined, "Something went wrong"));
            con.end();
        } else {

            // Make a query to the DB
            con.query(query, function (err, result) {
                if (err) {
                    callback(new Result(false, 500, undefined, "Something went wrong"));
                } else {

                    // If affected at least 1 row, return true and code 200
                    if (result.affectedRows > 0) {
                        callback(new Result(true, 200, result, undefined));
                    } else {
                        callback(new Result(false, 500, result, "Something went wrong"));
                    }

                }

            });

            con.end();
        }

    });

}

/**
 * Function to delete a row from a table of the DB
 * @param {*} tableName the name of the table to delete data
 * @param {*} condition the condition that determines the row that will be deleted
 * @param {*} callback response after success or failure
 */
function deleteDB(tableName, condition, callback) {

    // Add the 'DELETE FROM' string to the query
    let query = "DELETE FROM " + tableName + " WHERE " + condition + ";";


    const con = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: database,
        timezone: 'utc'
    });

    // Connectio to the database
    con.connect(function (err) {

        // If failed to connect
        if (err) {
            callback(new Result(false, 500, undefined, "Something went wrong"));
            con.end();
        } else {

            // Make a query to the DB
            con.query(query, function (err, result) {
                if (err) {
                    callback(new Result(false, 500, undefined, "Something went wrong"));
                } else {
                    // If affected at least 1 row, return true and code 200
                    if (result.affectedRows > 0) {
                        callback(new Result(true, 200, result, undefined));
                    } else {
                        callback(new Result(false, 404, result, "Not found"));
                    }
                }

            });

            con.end();
        }

    });

}

/**
 * Function to updates a row from a table of the DB
 * @param {*} tableName the name of the table to update data
 * @param {*} keyValues an Object containing the key and the value to update in the db
 * @param {*} condition the condition that determines the row that will be updated
 * @param {*} callback response after success or failure
 */
function updateDB(tableName, keyValues, condition, callback) {

    // Add the 'UPDATE' string to the query
    let query = "UPDATE " + tableName + " SET ";

    // Iterate the object properties and put the property and the value in separated arrays
    for (var key in keyValues) {
        if (keyValues.hasOwnProperty(key)) {
            query = query + key + " = '" + keyValues[key] + "', "
        }
    }

    // Remove the last space and comma
    query = query.slice(0, -2);

    query = query + " WHERE " + condition + ";";

    const con = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: database,
        timezone: 'utc'
    });

    // Connectio to the database
    con.connect(function (err) {

        // If failed to connect
        if (err) {
            callback(new Result(false, 500, undefined, "Something went wrong"));
            con.end();
        } else {

            // Make a query to the DB
            con.query(query, function (err, result) {
                if (err) {
                    callback(new Result(false, 500, undefined, "Something went wrong"));
                } else {
                    // If affected at least 1 row, return true and code 200
                    if (result.affectedRows > 0) {
                        callback(new Result(true, 200, result, undefined));
                    } else {
                        callback(new Result(false, 404, result, "Not found"));
                    }
                }

            });

            con.end();
        }

    });

}

/**
 * Make a simple selection in the db
 * @param {*} tableName name of the table to make the selection
 * @param {*} select the columns to show
 * @param {*} condition the condition to filter the results
 * @param {*} orderBy the order attributes
 * @param {*} callback response after success or insuccess
 */
function selectListDB(tableName, select, condition, orderBy, callback) {

    // Add the 'SELECT' string to the query
    let query = "SELECT ";

    // Add the values to show to the query
    for (let i = 0; i < select.length; i++) {
        query = query + select[i] + ", ";
    }

    // Remove the last space and comma
    query = query.slice(0, -2);

    // Add table to the query
    query = query + " FROM " + tableName;

    // Add condition to the query if it exists
    if(condition) {
        query = query + " WHERE " + condition;
    }

    // Add order by to the query if it exists
    if(orderBy) {
        query = query + " ORDER BY " + orderBy;
    }

    // Close the query
    query = query + ";";

    const con = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: database,
        timezone: 'utc'
    });

    // Connectio to the database
    con.connect(function (err) {

        // If failed to connect
        if (err) {
            callback(new Result(false, 500, undefined, "Something went wrong"));
            con.end();
        } else {

            // Make a query to the DB
            con.query(query, function (err, result) {
                if (err) {console.log(err)
                    callback(new Result(false, 500, undefined, "Something went wrong"));
                } else {
                    
                    callback(new Result(true, 200, result, undefined));
                }

            });

            con.end();
        }

    });

}

/**
 * Make a simple selection for one row in the db
 * @param {*} tableName name of the table to make the selection
 * @param {*} select the columns to show
 * @param {*} condition the condition to filter the results
 * @param {*} orderBy the order attributes
 * @param {*} callback response after success or insuccess
 */
function selectOneDB(tableName, select, condition, callback) {

    selectListDB(tableName, select, condition, undefined, (resultDB)=>{

        if(resultDB._result) {

            if(resultDB._object.length > 0) {
                resultDB._object = resultDB._object[0];
                callback(resultDB);
            }else{

                callback(new Result(false, 404, undefined, "Not found"));
            }

        }else{
            callback(resultDB);
        }

    })

}

/**
 * Get a simple count of all rows of a table
 * @param {*} tableName table to count rows
 * @param {*} renameCount rename the column of count
 * @param {*} condition the condition to filter the results
 * @param {*} callback response after success or insuccess
 */
function getCountDB(tableName, renameCount, condition, callback) {

    let query = "SELECT COUNT(*) as " + renameCount + " FROM " + tableName + "";

    if(condition) {
        query = query + " WHERE " + condition;
    }

    query = query+";";

    const con = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: database,
        timezone: 'utc'
    });

    // Connectio to the database
    con.connect(function (err) {

        // If failed to connect
        if (err) {
            callback(new Result(false, 500, undefined, "Something went wrong"));
            con.end();
        } else {

            // Make a query to the DB
            con.query(query, function (err, result) {
                if (err) {
                    callback(new Result(false, 500, undefined, "Something went wrong"));
                } else {
                    
                    callback(new Result(true, 200, result[0], undefined));
                }

            });

            con.end();
        }

    });

}

exports.queryDB = queryDB;
exports.queryDBPromise = queryDBPromise;
exports.insertDB = insertDB;
exports.deleteDB = deleteDB;
exports.updateDB = updateDB;
exports.selectListDB = selectListDB;
exports.selectOneDB = selectOneDB;
exports.getCountDB = getCountDB;