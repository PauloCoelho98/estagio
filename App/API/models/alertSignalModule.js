"use strict";

const mysqlModule = require("./mysqlModule");
const Result = require('./resultModule');

/**
 * Class which represents an AlertSignal
 */
class AlertSignal {

    constructor(id, sensorId, patientId, date) {
        this._id = id;
        this._sensorId = sensorId;
        this._patientId = patientId;
        this._date = date;
    }

    get id() {
        return this._id
    }

    get sensorId() {
        return this._sensorId;
    }

    get patientId() {
        return this._patientId;
    }

    get date() {
        return this._date
    }

    set id(id) {
        this._id = id;
    }

    set sensorId(sensorId) {
        this._sensorId = sensorId;
    }

    set patientId(patientId) {
        this._patientId = patientId;
    }

    set date(date) {
        this._date = date;
    }

    // Get a list of alertsignals from a patient
    static listSignalAlerts(patientId, callback) {


        mysqlModule.selectListDB("alert_signal", "*", "patient_id='" + patientId + "'", "id", (resultDB)=>{
            callback(resultDB)
        });

    }

    // Create an alert
    /*static addContact(name, number, callback) {

        const dataInsertDB = {
            "name": name,
            "number": number
        }

        mysqlModule.insertDB("contacts", dataInsertDB, (resultDB)=>{

            if(resultDB._result) {
            
                let contact = new Contact(resultDB._object.insertId, name, number);

                resultDB._object = contact;

                callback(resultDB);

            }else{
                callback(resultDB);
            }

        });

    }*/

    // Delete an alertsignal
    static deleteSignalAlert(id, callback) {

        mysqlModule.deleteDB("alert_signal", "id='" + id + "'", (resultDB)=>{

            resultDB._object = undefined;

            callback(resultDB);

        });

    }

}
module.exports = AlertSignal;