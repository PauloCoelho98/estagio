"use strict";

const mysqlModule = require("./mysqlModule");
const bcrypt = require('bcrypt');
const Result = require('./resultModule');

/**
 * Class which represents an user
 */
class Admin {

    constructor(id, username, email, role, permissions) {
        this._id = id;
        this._username = username;
        this._email = email;
        this._role = role;
        this._permissions = permissions;
    }

    get id() {
        return this._id;
    }

    get username() {
        return this._username;
    }

    get email() {
        return this._email;
    }

    get role() {
        return this._role;
    }

    get permissions() {
        return this._permissions;
    }

    set id(id) {
        this._id = id;
    }

    set username(username) {
        this._username = username;
    }

    set email(email) {
        this._email = email;
    }

    set role(role) {
        this._role = role;
    }

    set permissions(permissions) {
        this._permissions = permissions;
    }

    // Verify user credentials to log in
    static loginAdmin(email, password, callback) {

        const querySearch = "SELECT * FROM admins WHERE email='" + email + "' ORDER BY id;";

        // Get user
        mysqlModule.queryDB(querySearch, (result) => {

            // if there is no such user, can't login
            if (result.length < 1) {

                callback(false);

                // if the user exists
            } else {

                const queryPermissions = "SELECT * FROM admins_permissions WHERE adminId='" + result[0].id + "' ORDER BY id;";

                // Get the admin permissions
                mysqlModule.queryDB(queryPermissions, (resultPermissions) => {

                    let permissions = [];
                    for (let i = 0; i < resultPermissions.length; i++) {
                        permissions.push(resultPermissions[i].permission);
                    }

                    // Instance of the admin
                    const admin = new Admin(result[0].id, result[0].username, result[0].email, result[0].role, permissions);

                    // Compare the real password with a password entered
                    bcrypt.compare(password, result[0].password, function (err, response) {

                        // if password match
                        if (response === true) {

                            // callback a valid login and the admin object
                            callback(true, admin);

                        } else {
                            callback(false);
                        }

                    });

                });

            }
        });
    }

    // Get list of all admins in the database
    static getListOfAdmins(callback) {

        mysqlModule.selectListDB("admins", "*", undefined, "id", (resultDB) => {

            if (resultDB._result) {

                // If there are doctors in the database
                if (resultDB._object.length >= 1) {

                    let admins = [];

                    // Iterate all over the result array
                    for (let i = 0; i < resultDB._object.length; i++) {

                        mysqlModule.selectListDB("admins_permissions", "*", "adminId='" + resultDB._object[i].id + "'", "id", (resultDBPerm) => {

                            // Push the permissions to an array
                            let userPermissions = [];
                            for (let j = 0; j < resultDBPerm._object.length; j++) {
                                userPermissions.push(resultDBPerm._object[j].permission);
                            }

                            // Instance of the admin with permissions
                            let admin = new Admin(resultDB._object[i].id, resultDB._object[i].username, resultDB._object[i].email, resultDB._object[i].role, userPermissions);
                            admins.push(admin);

                            // When it reaches to the last iteration, callback the admins array
                            if (i === resultDB._object.length - 1) {
                                callback(new Result(resultDB._result, resultDB._status, admins, undefined));
                            }

                        });

                    }

                } else {
                    callback([]);
                }

            } else {
                // Return the error
                callback(new Result(resultDB._result, resultDB._status, [], undefined));
            }

        });

    }

    // Get an admin
    static getAdmin(id, callback) {

        mysqlModule.selectOneDB("admins", "*", "id='" + id + "'", (resultDB) => {

            if (resultDB._result) {

                mysqlModule.selectListDB("admins_permissions", "*", "adminId='" + resultDB._object.id + "'", "id", (resultDBPerm) => {

                    if (resultDBPerm._result) {

                        // Push the permissions to an array
                        let userPermissions = [];
                        for (let j = 0; j < resultDBPerm._object.length; j++) {
                            userPermissions.push(resultDBPerm._object[j].permission);
                        }

                        // Instance of the admin with the permissions
                        let admin = new Admin(resultDB._object.id, resultDB._object.username, resultDB._object.email, resultDB._object.role, userPermissions);

                        callback(new Result(resultDBPerm._result, resultDBPerm._status, admin, undefined));

                    } else {
                        // Return the error
                        callback(new Result(resultDBPerm._result, resultDBPerm._status, undefined, resultDBPerm._errorMessage));
                    }

                });

            } else {
                // Return the error
                callback(new Result(resultDB._result, resultDB._status, [], resultDB._errorMessage));
            }
        });

    }

    // Insert a new admins into database
    static signin(admin, password, callback) {

        const saltRounds = 10;

        // hash and salt the password
        bcrypt.genSalt(saltRounds, function (err, salt) {
            bcrypt.hash(password, salt, function (err, hash) {
                password = hash;

                // Object to send to insert into the DB
                let dataAdminToInsert = {
                    "username": admin._username,
                    "email": admin._email,
                    "password": password,
                    "role": admin._role
                }

                // Request to the module to insert the admin
                mysqlModule.insertDB("admins", dataAdminToInsert, (resultDB) => {

                    if (resultDB._result) {

                        // Update the id of the admin using the id created in the DB
                        admin._id = resultDB._object.insertId;

                        // Return the success
                        callback(new Result(resultDB._result, resultDB._status, admin, resultDB._errorMessage));


                        // Create permissions on database
                        for (let i = 0; i < admin._permissions.length; i++) {

                            // Data of the permission to insert
                            let dataPermissionsToInsert = {
                                "adminId": resultDB._object.insertId,
                                "permission": admin._permissions[i]
                            }

                            // Insert the permission in the db
                            mysqlModule.insertDB("admins_permissions", dataPermissionsToInsert, (resultDBP) => { });

                        }


                    } else {

                        // Return the error
                        callback(new Result(resultDB._result, resultDB._status, undefined, resultDB._errorMessage));

                    }

                });

            });
        });

    }

    // Delete the admins
    static deleteAdmin(id, callback) {

        const deleteCondition = "id='" + id + "'";

        mysqlModule.deleteDB("admins", deleteCondition, (resultDB) => {
            callback(new Result(resultDB._result, resultDB._status, undefined, resultDB._errorMessage));
        });

    }

    // Update the admin
    static updateAdmin(admin, newPassword, callback) {
        let id = admin._id;

        const saltRounds = 10;

        // If the password is not undefined, it updates the password
        if (newPassword != undefined) {
            // hash and salt the password
            bcrypt.genSalt(saltRounds, function (err, salt) {
                bcrypt.hash(newPassword, salt, function (err, hash) {
                    newPassword = hash;

                    const dataAdminToUpdate = {
                        "username": admin._username,
                        "email": admin._email,
                        "password": newPassword
                    }

                    const conditionToUpdate = "id = '" + id + "'";
                    mysqlModule.updateDB("admins", dataAdminToUpdate, conditionToUpdate, (resultDB) => {
                        callback(new Result(resultDB._result, resultDB._status, admin, resultDB._errorMessage));
                    });

                });
            });
        } else { // If the password is undefined, it doesn't update the password


            const dataAdminToUpdate = {
                "username": admin._username,
                "email": admin._email
            }

            const conditionToUpdate = "id = '" + id + "'";
            mysqlModule.updateDB("admins", dataAdminToUpdate, conditionToUpdate, (resultDB) => {
                callback(new Result(resultDB._result, resultDB._status, admin, resultDB._errorMessage));
            });
        }

        // Update permissions

        const conditionToDeleteP = "adminId='" + id + "'";
        mysqlModule.deleteDB("admins_permissions", conditionToDeleteP, (resultDBDelete) => {

            for (let i = 0; i < admin._permissions.length; i++) {

                const dataPermissionsToInsert = {
                    "adminId": id,
                    "permission": admin._permissions[i]
                }

                mysqlModule.insertDB("admins_permissions", dataPermissionsToInsert, (resultDBInsert) => { })

            }

        });

    }

    // Get the total number of admins
    static totalAdmins(callback) {

        mysqlModule.getCountDB("admins", "total", undefined, (resultDB) => {
            callback(resultDB)
        });

    }

    // Get a list of devices
    static listDevices(callback) {

        mysqlModule.selectListDB("device", "*", undefined, undefined, (resultDB) => {
            callback(resultDB);
        });

    }

    // Add a new device
    static addDevice(id, callback) {

        const dataDeviceToInsert = {
            "id": id
        }

        mysqlModule.insertDB("device", dataDeviceToInsert, (resultDB) => {
            callback(new Result(resultDB._result, resultDB._status, { id }, resultDB._errorMessage));
        });

    }

    // Delete a device
    static deleteDevice(id, callback) {

        const deleteCondition = "id='" + id + "'";
        mysqlModule.deleteDB("device", deleteCondition, (resultDB) => {
            callback(new Result(resultDB._result, resultDB._status, undefined, resultDB._errorMessage));
        });

    }

    // Get the total number of devices
    static totalDevices(callback) {

        mysqlModule.getCountDB("device", "total", undefined, (resultDB) => {
            callback(resultDB)
        });

    }

    // Change the password of the admin
    static changePassword(id, oldPassword, newPassword, callback) {

        mysqlModule.selectOneDB("admins", "*", "id='" + id + "'", (result) => {

            if (result._result) {
                const storedPassword = result._object.password;

                // Compare the real password with the old password inserted
                bcrypt.compare(oldPassword, storedPassword, function (err, response) {

                    // if password match
                    if (response === true) {

                        const saltRounds = 10;

                        // hash and salt the new password
                        bcrypt.genSalt(saltRounds, function (err, salt) {
                            bcrypt.hash(newPassword, salt, function (err, hash) {
                                newPassword = hash;

                                mysqlModule.updateDB("admins", { password: newPassword }, "id='" + id + "'", (updated) => {
                                    if (updated._result) {
                                        callback(true);
                                    } else {
                                        callback(false);
                                    }
                                });

                            });

                        });

                    } else {
                        callback(false);
                    }

                });

            } else {
                callback(false);
            }


        })

    }

}
module.exports = Admin;