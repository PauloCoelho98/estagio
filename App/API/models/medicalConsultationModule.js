"use strict";

const mysqlModule = require("./mysqlModule");
const Result = require('./resultModule');
/**
 * Class which represents a MedicalConsultation
 */
class MedicalConsultation {

    constructor(id, doctorId, patientId, date) {
        this._id = id;
        this._doctorId = doctorId;
        this._patientId = patientId;
        this._date = date;
    }

    get id() {
        return this._id
    }

    get doctorId() {
        return this._doctorId;
    }

    get patientId() {
        return this._patientId;
    }

    get date() {
        return this._date;
    }

    set id(id) {
        this._id = id;
    }

    set doctorId(doctorId) {
        this._doctorId = doctorId;
    }

    set patientId(patientId) {
        this._patientId = patientId;
    }

    set date(date) {
        this._date = date;
    }


    // Get list of all medical consultations in the database
    static getListOfMedicalConsultations(doctorId, callback) {

        mysqlModule.selectListDB("medical_consultation", "*", "doctor_id='" + doctorId + "'", "date", (resultDB)=>{
            callback(resultDB);
        });

    }

    // Get list of all medical consultations in the database
    static getListOfMedicalConsultationsByPatient(patientId, callback) {

        mysqlModule.selectListDB("medical_consultation", "*", "patient_id='" + patientId + "'", "date", (resultDB)=>{
            callback(resultDB);
        });

    }

    // Get a specific medical consultation
    static getMedicalConsultation(consultationId, callback) {

        mysqlModule.selectOneDB("medical_consultation", "*", "id='" + consultationId + "'", (resultDB)=>{
            callback(resultDB);
        });

    }

    // Delete a medical consultation
    static deleteMedicalConsultation(consultationId, callback) {

        mysqlModule.deleteDB("medical_consultation", "id = '" + consultationId + "'", (resultDB)=>{
            callback(resultDB);
        });

    }

    // Create a new medical consultation
    static createMedicalConsultation(doctorId, patientId, date, callback) {

        const dataToInsert = {
            "doctor_id": doctorId,
            "patient_id": patientId,
            "date": date   
        }

        mysqlModule.insertDB("medical_consultation", dataToInsert, (resultDB)=>{
            if(resultDB) {

                let medicalConsultation = new MedicalConsultation(resultDB._object.insertId, doctorId, patientId, date);
                resultDB._object = medicalConsultation;
                callback(resultDB);

            }else{
                callback(resultDB);
            }
        });

    }

    // Update a medical consultation
    static updateMedicalConsultation(id, doctorId, patientId, date, callback) {

        const dataToUpdate = {
            "id": id,
            "doctor_id": doctorId,
            "patient_id": patientId,
            "date": date
        }

        mysqlModule.updateDB("medical_consultation", dataToUpdate, "id='" + id + "'", (resultDB)=>{

            if(resultDB._result) {

                let medicalConsultation = new MedicalConsultation(id, doctorId, patientId, date);
                resultDB._object = medicalConsultation;

                callback(resultDB);

            }else{
                callback(resultDB)
            }

        });

    }

    // Get the total number of medical consultations
    static getMedicalConsultationTotal(doctorId, callback) {

        mysqlModule.getCountDB("medical_consultation", "total", "doctor_id='" + doctorId + "'", (resultDB)=>{
            callback(resultDB);
        });

    }

    // Get all medical consultations of a patient
    static getAllMedicalConsultationsByPatient(doctorId, patientId, callback) {

        mysqlModule.selectListDB("medical_consultation", "*", "doctor_id='" + doctorId + "' AND patient_id='" + patientId + "'", "date", (resultDB)=>{

            callback(resultDB);

        });
        
    }

}
module.exports = MedicalConsultation;