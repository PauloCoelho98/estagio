"use strict";

/**
 * Class which represents a result
 */
class result {

    constructor(result, status, object, errorMessage) {
        this._result = result;
        this._status = status;
        this._object = object;
        this._errorMessage = errorMessage
    }

    get result() {
        return this._result;
    }
    
    get status() {
        return this._status;
    }

    get object() {
        return this._object;
    }

    get errorMessage() {
        return this._errorMessage;
    }

    set result(result) {
        this._result = result;
    }

    set status(status) {
        this._status = status;
    }

    set object(object) {
        this._object = object;
    }

    set errorMessage(errorMessage) {
        this._errorMessage = errorMessage;
    }

}
module.exports = result;