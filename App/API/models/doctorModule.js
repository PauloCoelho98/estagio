"use strict";

const mysqlModule = require("./mysqlModule");
const bcrypt = require('bcrypt');
const Result = require('./resultModule');


/**
 * Class which represents a Doctor
 */
class Doctor {

    constructor(id, name, email, role, permissions, linkCall) {
        this._id = id;
        this._name = name;
        this._email = email;
        this._role = role;
        this._permissions = permissions;
        this._linkCall = linkCall;
    }

    get id() {
        return this._id
    }

    get name() {
        return this._name;
    }

    get email() {
        return this._email;
    }

    get role() {
        return this._role;
    }

    get permissions() {
        return this._permissions;
    }

    get linkCall() {
        return this._linkCall;
    }

    set id(id) {
        this._id = id;
    }

    set name(name) {
        this._name = name;
    }

    set email(email) {
        this._email = email;
    }

    set role(role) {
        this._role = role;
    }

    set permissions(permissions) {
        this._permissions = permissions;
    }

    set linkCall(linkCall) {
        this._linkCall = linkCall;
    }

    // Verify user credentials to log in
    static loginDoctor(email, password, callback) {

        const querySearch = "SELECT * FROM doctors WHERE email='" + email + "' ORDER BY id;";

        // Get user
        mysqlModule.queryDB(querySearch, (result) => {

            // if there is no such user, can't login
            if (result.length < 1) {

                callback(false);

                // if the user exists
            } else {

                const queryPermissions = "SELECT * FROM doctors_permissions WHERE doctor_id='" + result[0].id + "' ORDER BY id;";

                // Get the doctor permissions
                mysqlModule.queryDB(queryPermissions, (resultPermissions) => {

                    // Push the permissions to an array
                    let permissions = [];
                    for (let i = 0; i < resultPermissions.length; i++) {
                        permissions.push(resultPermissions[i].permission);
                    }

                    // Instance of the doctor with the permissions
                    const doctor = new Doctor(result[0].id, result[0].name, result[0].email, result[0].role, permissions, result[0].link_call);

                    // Compare the real password with a password entered
                    bcrypt.compare(password, result[0].password, function (err, response) {

                        // if password match
                        if (response === true) {

                            // callback a valid login and the admin object
                            callback(true, doctor);

                        } else {
                            callback(false);
                        }

                    });

                });

            }
        });
    }

    // Get list of all doctors in the database
    static getListOfDoctors(callback) {


        mysqlModule.selectListDB("doctors", "*", undefined, "id", (resultDB) => {

            if (resultDB._result) {

                // If there are doctors in the database
                if (resultDB._object.length >= 1) {

                    let doctors = [];

                    //Iterate all over the doctors array
                    for (let i = 0; i < resultDB._object.length; i++) {

                        // Get the permissions of this doctor
                        mysqlModule.selectListDB("doctors_permissions", "*", "doctor_id='" + resultDB._object[i].id + "'", "id", (resultDBPerm) => {

                            if (resultDBPerm._result) {

                                // Push the permissions to an array
                                let userPermissions = [];
                                for (let j = 0; j < resultDBPerm._object.length; j++) {
                                    userPermissions.push(resultDBPerm._object[j].permission);
                                }


                                // Instance of the doctor with the permissions
                                let doctor = new Doctor(resultDB._object[i].id, resultDB._object[i].name, resultDB._object[i].email, resultDB._object[i].role, userPermissions, resultDB._object[i].link_call);
                                doctors.push(doctor);

                                // When it reaches to the last iteration callback with the array of doctors
                                if (i === resultDB._object.length - 1) {

                                    resultDB._object = doctors;

                                    callback(resultDB);
                                }

                            } else {
                                callback(resultDBPerm);
                            }

                        });

                    }

                } else {
                    callback(new Result(resultDB._result, resultDB._status, [], resultDB._errorMessage));
                }

            } else {
                callback(resultDB);
            }

        });

    }

    // Get a doctor
    static getDoctor(id, callback) {

        mysqlModule.selectOneDB("doctors", "*", "id='" + id + "'", (resultDB) => {

            if (resultDB._result) {

                mysqlModule.selectListDB("doctors_permissions", "*", "doctor_id='" + resultDB._object.id + "'", "id", (resultDBPerm) => {


                    if (resultDBPerm._result) {

                        // Push the permissions to an array 
                        let userPermissions = [];
                        for (let j = 0; j < resultDBPerm._object.length; j++) {
                            userPermissions.push(resultDBPerm._object[j].permission);
                        }

                        // Instance of the doctor
                        let doctor = new Doctor(resultDB._object.id, resultDB._object.name, resultDB._object.email, resultDB._object.role, userPermissions, resultDB._object.link_call);

                        resultDB._object = doctor;

                        callback(resultDB);

                    } else {
                        callback(resultDBPerm);
                    }


                });

            } else {
                callback(resultDB);
            }

        });

    }

    // Insert a new doctor into database
    static signin(name, email, password, permissions, linkCall, callback) {

        const saltRounds = 10;

        // hash and salt the password
        bcrypt.genSalt(saltRounds, function (err, salt) {
            bcrypt.hash(password, salt, function (err, hash) {
                password = hash;

                const dataToInsert = {
                    "name": name,
                    "email": email,
                    "password": password,
                    "role": "Doctor",
                    "link_call": linkCall
                }

                mysqlModule.insertDB("doctors", dataToInsert, (resultDB) => {

                    if (resultDB._result) {

                        let doctor = new Doctor(resultDB._object.insertId, name, email, 'Doctor', permissions, linkCall);
                        callback(new Result(resultDB._result, resultDB._status, doctor, undefined));

                        // Create permissions on database
                        for (let i = 0; i < permissions.length; i++) {

                            const dataToInsertPerm = {
                                "doctor_id": resultDB._object.insertId,
                                "permission": permissions[i]
                            }

                            mysqlModule.insertDB("doctors_permissions", dataToInsertPerm, (resultDBPerm) => { });

                        }

                    } else {
                        callback(resultDB);
                    }

                });

            });
        });

    }

    // Delete the doctor
    static deleteDoctor(id, callback) {

        mysqlModule.deleteDB("doctors", "id='" + id + "'", (resultDB) => {

            callback(resultDB);

        });

    }

    // Update the doctor
    static updateDoctor(doctor, newPassword, callback) {
        let id = doctor._id;
        let name = doctor._name;
        let email = doctor._email;
        let permissions = doctor._permissions;
        let linkCall = doctor._linkCall

        const saltRounds = 10;

        // If the password is not undefined, it updates the password
        if (newPassword != undefined) {
            // hash and salt the password
            bcrypt.genSalt(saltRounds, function (err, salt) {
                bcrypt.hash(newPassword, salt, function (err, hash) {
                    newPassword = hash;

                    const dataToUpdate = {
                        "name": name,
                        "email": email,
                        "password": newPassword,
                        "link_call": linkCall
                    }

                    mysqlModule.updateDB("doctors", dataToUpdate, "id='" + id + "'", (resultDB) => {

                        if (resultDB) {
                            resultDB._object = doctor;
                            callback(resultDB);
                        } else {
                            callback(resultDB);
                        }

                    });

                });
            });
        } else { // If the password is undefined, it doesn't update the password

            const dataToUpdate = {
                "name": name,
                "email": email,
                "link_call": linkCall
            }

            mysqlModule.updateDB("doctors", dataToUpdate, "id='" + id + "'", (resultDB) => {

                if (resultDB) {
                    resultDB._object = doctor;
                    callback(resultDB);
                } else {
                    callback(resultDB);
                }

            });

        }

        // Update permissions
        mysqlModule.deleteDB("doctors_permissions", "doctor_id='" + id + "'", (resultDBDel) => {

            for (let i = 0; i < permissions.length; i++) {
                const dataToInsertPerm = {
                    "doctor_id": id,
                    "permission": permissions[i]
                }

                mysqlModule.insertDB("doctors_permissions", dataToInsertPerm, (resultDBInsPerm) => { });

            }

        });

    }

    // Get the total doctors that exists
    static totalDoctors(callback) {

        mysqlModule.getCountDB("doctors", "total", undefined, (resultDB) => {
            callback(resultDB);
        });

    }

    // Get all the doctors assgined to the patients
    static getDoctorPatients(doctorId, callback) {

        mysqlModule.selectListDB("doctors_patients", "*", "doctor_id='" + doctorId + "'", "doctor_id", (resultDB) => {

            if (resultDB._result) {


                // Get only the patients id to an array
                let patients = [];
                for (let i = 0; i < resultDB._object.length; i++) {
                    patients.push(resultDB._object[i].patient_id);
                }

                // Get complete object of the patients
                let requests = patients.length; // Number of requests it has to do to the DB
                let patientsObj = [];
                for (let i = 0; i < patients.length; i++) {

                    mysqlModule.selectOneDB("patients", "*", "id='" + patients[i] + "'", (resultDBPat) => {

                        requests--; // Decrease requests
                        patientsObj.push(resultDBPat._object); // push the object to the array

                        // If this is the last request, calls callback
                        if (requests === 0) {
                            resultDB._object = patientsObj;
                            callback(resultDB);
                        }

                    });

                }


            } else {
                callback(resultDB);
            }

        });

    }

    // Get the total of doctors assignments
    static getDoctorTotalPatients(doctorId, callback) {

        mysqlModule.getCountDB("doctors_patients", "total", "doctor_id='" + doctorId + "'", (resultDB) => {
            callback(resultDB);
        });

    }

    // Get all existing sensors
    static getSensors(callback) {

        mysqlModule.selectListDB("sensors", "*", undefined, "sensor_id", (resultDB) => {

            callback(resultDB);

        });

    }

    // Check if a doctor has a certain patient
    static hasPatient(doctorId, patientId, callback) {

        mysqlModule.selectListDB("doctors_patients", "*", "doctor_id='" + doctorId + "' AND patient_id='" + patientId + "'", "doctor_id", (resultDB) => {

            callback(resultDB._object.length > 0);

        });

    }

    // Check if a doctor has a view
    static hasView(doctorId, viewId, callback) {

        mysqlModule.selectListDB("dashboard_view", "*", "doctor_id='" + doctorId + "' AND view_id='" + viewId + "'", "view_id", (resultDB) => {

            callback(resultDB._object.length > 0);

        });

    }

    // Check if a doctor has a medical consultation
    static hasMedicalConsultation(doctorId, medicalConsultationId, callback) {

        mysqlModule.selectListDB("medical_consultation", "*", "doctor_id='" + doctorId + "' AND id='" + medicalConsultationId + "'", "id", (resultDB) => {

            callback(resultDB._object.length > 0);

        });
    }

    // Assign a device to a patient
    static assignDevice(doctorId, deviceId, patientId, callback) {

        const queryAssignToNewPatient = "UPDATE patients SET current_device_id='" + deviceId + "' WHERE id='" + patientId + "';";

        // Assign the device to another patient
        mysqlModule.queryDB(queryAssignToNewPatient, (resultAssignToNewPatient) => {

            if (resultAssignToNewPatient) {

                let now = new Date().toISOString().slice(0, 19).replace('T', ' ');

                const queryPutNewHistory = "INSERT INTO device_history (device_id, patient_id, start, end) VALUES (" + deviceId + ", " + patientId + ", '" + now + "', NULL)";

                // Open a new entry in history
                mysqlModule.queryDB(queryPutNewHistory, (resultPutNewHistory) => {

                    if (resultPutNewHistory) {
                        callback(true);
                    } else {
                        callback(false);
                    }

                });


            } else {
                callback(false);
            }

        });

    }

    // Get all existing devices
    static getDevices(callback) {

        mysqlModule.selectListDB("device", "*", undefined, "id", (resultDB) => {
            callback(resultDB);
        });

    }

    // Remove a device from a patient
    static removeAssignedDevice(patientId, callback) {

        const queryGetDeviceId = "SELECT current_device_id FROM patients WHERE id='" + patientId + "' ORDER BY id;";

        // Get the id of the device assigned to the patient
        mysqlModule.queryDB(queryGetDeviceId, (result) => {

            let deviceId = result[0].current_device_id;

            const query = "UPDATE patients SET current_device_id=NULL WHERE current_device_id='" + deviceId + "';";

            // Remove a device from the patient
            mysqlModule.queryDB(query, (result) => {

                if (result) {

                    let now = new Date().toISOString().slice(0, 19).replace('T', ' ');

                    const queryFinishHistory = "UPDATE device_history SET end='" + now + "' WHERE device_id='" + deviceId + "' AND end IS NULL;";

                    // Put the finish date on the history
                    mysqlModule.queryDB(queryFinishHistory, (resultFinishHistory) => {

                        if (resultFinishHistory) {
                            callback(true);
                        } else {
                            callback(false);
                        }
                    });

                } else {
                    callback(false);
                }

            });

        });

    }

    // Check for the patient that has a specific device
    static patientWithDevice(deviceId, callback) {


        const query = "SELECT * FROM patients WHERE current_device_id='" + deviceId + "' ORDER BY id;";

        mysqlModule.queryDB(query, (result) => {
            if (result.length > 0) {
                callback(result[0]);
            } else {
                callback(false);
            }
        });


    }

    // Get all the views of one doctor
    static getViews(doctorId, callback) {

        let views = [];

        mysqlModule.selectListDB("dashboard_view", "*", "doctor_id='" + doctorId + "'", "view_id", (resultDB) => {

            for (let i = 0; i < resultDB._object.length; i++) {

                let view = resultDB._object[i];

                let viewId = resultDB._object[i].view_id;

                mysqlModule.selectListDB("dashboard_view_sensors", ["sensor_id"], "view_id='" + viewId + "'", "id", (resultDBSensors) => {

                    let sensors = [];
                    for (let j = 0; j < resultDBSensors._object.length; j++) {
                        sensors.push(resultDBSensors._object[j].sensor_id)
                    }

                    view['sensors'] = sensors;
                    views.push(view);

                    if (i === resultDB._object.length - 1) {
                        callback(views);
                    }

                });

            }
        });

    }

    // Add a new view
    static addView(doctorId, viewName, sensors, callback) {

        const dataToInsert = {
            "doctor_id": doctorId,
            "view_name": viewName
        }

        // Insert the options of the view in the DB
        mysqlModule.insertDB("dashboard_view", dataToInsert, (resultDB) => {

            if (resultDB._result) {
                let id = resultDB._object.insertId;

                for (let i = 0; i < sensors.length; i++) {

                    const dataToInsertSensors = {
                        "view_id": id,
                        "sensor_id": sensors[i]
                    }

                    // Insert the sensors of the view in the DB
                    mysqlModule.insertDB("dashboard_view_sensors", dataToInsertSensors, (resultDBSensors) => {

                        if (i === sensors.length - 1) {
                            let obj = { "view_id": id, "doctor_id": doctorId, "view_name": viewName, "sensors": sensors }
                            resultDB._object = obj;
                            callback(resultDB);
                        }

                    });

                }

            } else {
                callback(resultDB);
            }

        });


    }

    // Delete a view
    static deleteView(viewId, callback) {

        mysqlModule.deleteDB("dashboard_view", "view_id='" + viewId + "'", (resultDB) => {
            callback(resultDB);
        });

    }

    // update a view
    static updateView(viewId, viewName, sensors, callback) {

        const dataToUpdate = {
            "view_name": viewName
        }

        mysqlModule.updateDB("dashboard_view", dataToUpdate, "view_id='" + viewId + "'", (resultDBUpdate) => {

            if (resultDBUpdate._result) {

                mysqlModule.deleteDB("dashboard_view_sensors", "view_id='" + viewId + "'", (resultDBDel) => {

                    if (resultDBDel._result) {

                        for (let i = 0; i < sensors.length; i++) {

                            const dataToInsert = {
                                "view_id": viewId,
                                "sensor_id": sensors[i]
                            }

                            mysqlModule.insertDB("dashboard_view_sensors", dataToInsert, (resultDBInsert) => {

                                if (i === sensors.length - 1) {
                                    let obj = { "view_id": viewId, "doctor_id": '', "view_name": viewName, "sensors": sensors }
                                    resultDBInsert._object = obj;
                                    callback(resultDBInsert);
                                }

                            });

                        }

                    } else {
                        callback(resultDBDel);
                    }

                });

            } else {
                callback(resultDBUpdate);
            }

        });

    }

    // Change the password of the admin
    static changePassword(id, oldPassword, newPassword, callback) {

        mysqlModule.selectOneDB("doctors", "*", "id='" + id + "'", (result) => {

            if (result._result) {
                const storedPassword = result._object.password;

                // Compare the real password with the old password inserted
                bcrypt.compare(oldPassword, storedPassword, function (err, response) {

                    // if password match
                    if (response === true) {

                        const saltRounds = 10;

                        // hash and salt the new password
                        bcrypt.genSalt(saltRounds, function (err, salt) {
                            bcrypt.hash(newPassword, salt, function (err, hash) {
                                newPassword = hash;

                                mysqlModule.updateDB("doctors", { password: newPassword }, "id='" + id + "'", (updated) => {
                                    if (updated._result) {
                                        callback(true);
                                    } else {
                                        callback(false);
                                    }
                                });

                            });

                        });

                    } else {
                        callback(false);
                    }

                });

            } else {
                callback(false);
            }


        })

    }


}
module.exports = Doctor;