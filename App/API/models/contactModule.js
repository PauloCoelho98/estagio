"use strict";

const mysqlModule = require("./mysqlModule");
const Result = require('./resultModule');

/**
 * Class which represents a Contact
 */
class Contact {

    constructor(id, name, number) {
        this._id = id;
        this._name = name;
        this._number = number;
    }

    get id() {
        return this._id
    }

    get name() {
        return this._name;
    }

    get number() {
        return this._number;
    }

    set id(id) {
        this._id = id;
    }

    set name(name) {
        this._name = name;
    }

    set number(number) {
        this._number = number;
    }

    // Get a list of contacts
    static listContacts(callback) {

        mysqlModule.selectListDB("contacts", "*", undefined, "id", (resultDB) => {

            if (resultDB._result) {
                // Push the contacts to an array
                let listContacts = [];
                for (let i = 0; i < resultDB._object.length; i++) {
                    let contact = new Contact(resultDB._object[i].id, resultDB._object[i].name, resultDB._object[i].number);
                    listContacts.push(contact);
                }

                callback(new Result(resultDB._result, resultDB._status, listContacts, resultDB._errorMessage));
            } else {
                callback(resultDB);
            }

        });

    }

    // Get a contacts by id
    static getContact(id, callback) {

        mysqlModule.selectOneDB("contacts", "*", "id='" + id + "'", (resultDB) => {

            if (resultDB._result) {
                let contact = new Contact(resultDB._object.id, resultDB._object.name, resultDB._object.number);

                callback(new Result(resultDB._result, resultDB._status, contact, undefined));

            } else {
                callback(resultDB);
            }

        });

    }

    // Create a contact
    static addContact(name, number, callback) {

        const dataInsertDB = {
            "name": name,
            "number": number
        }

        mysqlModule.insertDB("contacts", dataInsertDB, (resultDB)=>{

            if(resultDB._result) {
            
                let contact = new Contact(resultDB._object.insertId, name, number);

                resultDB._object = contact;

                callback(resultDB);

            }else{
                callback(resultDB);
            }

        });

    }

    // Update a contact
    static updateContact(id, name, number, callback) {

        const dataUpdateDB = {
            "name": name,
            "number": number
        }

        mysqlModule.updateDB("contacts", dataUpdateDB, "id='" + id + "'", (resultDB)=>{
            if(resultDB._result) {

                let contact = new Contact(id, name, number);
                resultDB._object = contact;
                callback(resultDB)

            }else{
                callback(resultDB)
            }
        });

    }

    // Delete a contact
    static deleteContact(id, callback) {

        mysqlModule.deleteDB("contacts", "id='" + id + "'", (resultDB)=>{

            resultDB._object = undefined;

            callback(resultDB);

        });

    }

    // Get the total number of contacts
    static totalContacts(callback) {

        mysqlModule.getCountDB("contacts", "total", undefined, (resultDB) => {
            callback(resultDB);
        });

    }

}
module.exports = Contact;