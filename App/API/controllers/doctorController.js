"use strict";

const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

const fs = require('fs');

const doctor = require("../models/doctorModule");
const patient = require("../models/patientModule");
const doctorPatient = require("../models/doctorPatientModule");
const medicalConsultation = require("../models/medicalConsultationModule");
const emergency = require("../models/emergencyModule");
const alertSignal = require("../models/alertSignalModule");

// user login
function login(req, res) {

    let email = req.body.email;
    let password = req.body.password;

    // Check if the request has the necessary fields
    if (email === undefined || password === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Check if password and doctor email check
        doctor.loginDoctor(email, password, (result, doctorObj) => {
            if (result) { // if introduced valid credentials

                let doctorId = doctorObj._id;
                let role = doctorObj._role;
                let permissions = doctorObj._permissions;

                // Get the secret key from file to generate a token
                fs.readFile('./API/datafiles/jwtsecretkey.json', 'utf8', function (err, secretKey) {
                    if (err) {
                        //throw err;
                        console.log(err)
                        res.status(500).json({ "Message": "Something went wrong" });
                    } else {

                        // Parse the text from the file to json
                        secretKey = JSON.parse(secretKey);

                        // Generate a token
                        jwt.sign({ user: doctorObj }, secretKey.secret_key, { expiresIn: '1h' }, (err, token) => {

                            // Send to the user the token and he's informations: id, type and permissions
                            res.json({
                                token,
                                "userId": doctorId,
                                "userType": role,
                                "permissions": permissions
                            });
                        });
                    }
                });
            } else { // if not introduced valid credentials

                res.status(401).json({ "Error": "Invalid credintials" });

            }
        });
    }
}

// DOCTORS 

// Get one doctor
function getDoctor(req, res) {

    let doctorId = res.locals.authData.user._id;

    // Get the doctor
    doctor.getDoctor(doctorId, (result) => {
        const doctor = result._object;

        if (result._result) {
            res.status(201).json({ doctor });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }
    });

}

// Get all patients of one doctor
function getDoctorPatients(req, res) {

    let doctorId = res.locals.authData.user._id;

    // Get the doctor patients
    doctor.getDoctorPatients(doctorId, (result) => {

        const patients = result._object;

        if (result._result) {
            res.status(201).json({ patients });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }
    });

}

// Get the total patients of one doctor
function getTotalPatients(req, res) {

    let doctorId = res.locals.authData.user._id;

    // Get the total patients of the doctor
    doctor.getDoctorTotalPatients(doctorId, (result) => {

        if (result._result) {
            res.status(200).json(result._object)
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });
}

// Get the picture of a patient
function getPatientPicture(req, res) {

    let patientId = req.params.patientId;

    // Get the patient
    patient.getPatient(patientId, (patient) => {

        // Get the picture path
        const imageName = patient._picture.split("/")[2];

        // Send the picture
        res.sendFile(imageName, { 'root': './API/patients_picture' });

    });

}

// Get all existing sensors
function getSensors(req, res) {

    // Get all existing sensors
    doctor.getSensors((result) => {

        const sensors = result._object;

        if (result._result) {
            res.status(201).json({ sensors });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });
}

// Get all values measured by sensors of a patient of this doctor
function getPatientAllValues(req, res) {

    // Get all values from the patient
    patient.getAllValues(req.params.patientId, (result) => {
        if (result) {
            res.status(200).json({ values: result });
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }
    });

}

// Get one patient
function getPatient(req, res) {

    // Get the patient
    patient.getPatient(req.params.patientId, (result) => {
        if (result) {
            res.status(200).json({ "patient": result })
        } else {
            res.status(404).json({ "Message": "Patient not found" });
        }
    });

}

// Get all values measured by one sensor of a patient of this doctor
function getPatientValuesBySensor(req, res) {

    // Get the values 
    patient.getValuesBySensor(req.params.patientId, req.params.sensor, (result) => {
        if (result) {
            res.status(200).json({ values: result });
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }
    });

}

// Assign a device to a patient
function assignDevice(req, res) {

    let doctorId = req.body.doctorId;
    let deviceId = req.body.deviceId;
    let patientId = req.body.patientId;

    if (doctorId === undefined || deviceId === undefined || patientId === undefined) {
        res.status(400).json({});
    } else {

        // Check if there is any patient with the device
        doctor.patientWithDevice(deviceId, (patientWithDevice) => {

            // If there is a patient with the device, remove the device from him
            if (patientWithDevice) {
                doctor.removeAssignedDevice(patientWithDevice.id, () => { });
            }

            // Remove the assigned device to the patient
            doctor.removeAssignedDevice(patientId, (resultRemove) => {

                // Assign the new device to the patient
                doctor.assignDevice(doctorId, deviceId, patientId, (result) => {
                    res.status(200).json(result);
                });

            });

        });

    }

}

// Get the existing devices
function getDevices(req, res) {

    // Get a list of all devices
    doctor.getDevices((result) => {

        if (result._result) {
            res.status(200).json({ "devices": result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Remove an assigned device
function removeAssignedDevice(req, res) {

    // Remove the assigned device
    doctor.removeAssignedDevice(req.params.patientId, (result) => {
        if (result) {
            res.status(200).json({});
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }
    });

}

// VIEWS

// Get all the doctor views
function getViews(req, res) {

    let doctorId = res.locals.authData.user._id;

    // Get all the views
    doctor.getViews(doctorId, (result) => {

        if (result) {
            res.status(200).json({ views: result });
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }

    });

}

// Add a doctor view
function newView(req, res) {

    let doctorId = req.body.doctorId;
    let viewName = req.body.viewName;
    let sensors = req.body.sensors;

    // Check if the request has the necessary fields
    if (doctorId === undefined || viewName === undefined || sensors === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Add the view
        doctor.addView(doctorId, viewName, sensors, (result) => {
            if (result._result) {
                res.status(201).json(result._object);
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }
        });

    }

}

// Delete a doctor view
function deleteView(req, res) {

    // Check is the request contains the view id
    if (req.params.viewId === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Delete the view
        doctor.deleteView(req.params.viewId, (result) => {

            if (result._result) {
                res.status(204).json();
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }
        });

    }

}

// Update a doctor view
function updateView(req, res) {

    let viewName = req.body.viewName;
    let sensors = req.body.sensors;

    // Check if the request has the necessary fields
    if (req.params.viewId === undefined || viewName === undefined || sensors === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Update the view
        doctor.updateView(req.params.viewId, viewName, sensors, (result) => {

            if (result._result) {
                res.status(201).json(result._object);
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }

        });
    }
}

// MEDICAL CONSULTATION

// Get all medical consultation
function getAllMedicalConsultations(req, res) {

    let doctorId = res.locals.authData.user._id;

    // Get the list of medical consultations
    medicalConsultation.getListOfMedicalConsultations(doctorId, (result) => {

        if (result._result) {
            res.status(201).json({ "medicalConsultation": result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Get all medical consultation of one patient
function getAllMedicalConsultationsByPatient(req, res) {

    let doctorId = res.locals.authData.user._id;

    // Get all medical consultations of a patient
    medicalConsultation.getAllMedicalConsultationsByPatient(doctorId, req.params.patientId, (result) => {

        if (result._result) {
            res.status(200).json({ "medicalConsultation": result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Get one medical consultation
function getMedicalConsultation(req, res) {

    // Get a specific medical consultation
    medicalConsultation.getMedicalConsultation(req.params.idconsultation, (result) => {

        if (result._result) {
            res.status(201).json({ "medicalConsultation": result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Delete one medical consultation
function deleteMedicalConsultation(req, res) {

    // Delete a medical consultation
    medicalConsultation.deleteMedicalConsultation(req.params.idconsultation, (result) => {

        if (result._result) {
            res.status(204).json();
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });
}

// Add one medical consultation
function createMedicalConsultation(req, res) {

    let doctorId = req.body.doctorId;
    let patientId = req.body.patientId;
    let date = req.body.date;

    // Check if the request has the necessary fields
    if (doctorId === undefined || patientId === undefined || date === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        medicalConsultation.createMedicalConsultation(doctorId, patientId, date, (result) => {

            if (result._result) {
                res.status(201).json(result._object);
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }

        });

    }

}

// Update one medical consultation
function updateMedicalConsultation(req, res) {
    let id = req.params.idconsultation;
    let idBody = req.body.id;
    let doctorId = req.body.doctorId;
    let patientId = req.body.patientId;
    let date = req.body.date;

    // Check if the body has the necessary fields
    if (id === undefined || idBody === undefined || doctorId === undefined || patientId === undefined || date === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Check if the id in the params and the id in the body are the same
        if (id == idBody) {

            medicalConsultation.updateMedicalConsultation(id, doctorId, patientId, date, (result) => {

                if (result._result) {
                    res.status(200).json(result._object);
                } else {
                    const errorMessage = result._errorMessage;
                    res.status(result._status).json({ "Message": errorMessage });
                }

            });
        } else {
            res.status(400).json({ "Message": "Request bad formed" });
        }

    }
}

// Get the total medical consultations of one doctor
function getMedicalConsultationTotal(req, res) {

    let doctorId = res.locals.authData.user._id;

    // Get the total of medical consultations
    medicalConsultation.getMedicalConsultationTotal(doctorId, (result) => {

        if (result._result) {
            res.status(200).json({ "total": result._object.total });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// PATIENT MONITORING SENSORS

// List all monitoring sensors of one patient
function listPatientMonitoringSensors(req, res) {

    // Get list of all doctors
    patient.listPatientMonitoringSensors(req.params.patientId, (patientMonitoringSensors) => {

        res.json({ patientMonitoringSensors });

    });

}

// Add one sensor for monitorization to a patient
function addPatientMonitoringSensors(req, res) {

    let patientId = req.body.patientId;
    let sensorId = req.body.sensorId;

    // Check if the request has the necessary fields
    if (patientId === undefined || sensorId === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {
        patient.addPatientMonitoringSensors(patientId, sensorId, (result) => {
            if (result) {
                res.status(201).json(result);
            } else {
                res.status(500).json({ "Message": "Something went wrong" });
            }
        });
    }

}

// Delete all monitoring sensors of one patient
function deletePatientMonitoringSensors(req, res) {

    // Check if the request has the necessary fields
    if (req.params.patientId === undefined) {
        res.status(404).json({ "Message": "Patient not found" });
    } else {

        // Delete the monitoring sensors of this patient
        patient.deletePatientMonitoringSensors(req.params.patientId, (result) => {
            if (result) {
                res.status(204).json();
            } else {
                res.status(404).json({ "Message": "Patient not found" });
            }
        });
    }
}

// PATIENT EMERGENCIES

// Get all active emergencies
function getListOfActiveEmergencies(req, res) {

    // Get the list of the active emergencies
    emergency.getListOfActiveEmergencies((result) => {

        if (result._result) {
            res.status(201).json({ "emergencies": result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Get all responding emergencies of one doctor
function getListOfRespondingEmergencies(req, res) {

    let doctorId = res.locals.authData.user._id;

    // Get the list of responding emergencies by doctor
    emergency.getListOfRespondingEmergencies(doctorId, (result) => {

        if (result._result) {
            res.status(201).json({ "emergencies": result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Respond to an emergency
function respondToEmergency(req, res) {

    let emergencyId = req.body.emergencyId;

    // Check if the request has the necessary fields
    if (emergencyId === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Get the current date
        let date = new Date().toISOString().slice(0, 19).replace('T', ' ');

        let doctorId = res.locals.authData.user._id;

        // Change the state of the emergency to responding
        emergency.respondToEmergency(emergencyId, date, doctorId, (result) => {

            if (result._result) {
                res.status(201).json({ responded: true });
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }

        });

    }

}

// Close an emergency
function closeEmergency(req, res) {

    let emergencyId = req.body.emergencyId;

    // Check if the request has the necessary fields
    if (emergencyId === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Get the current date
        let date = new Date().toISOString().slice(0, 19).replace('T', ' ');

        let doctorId = res.locals.authData.user._id;

        // Close the emergency
        emergency.closeEmergency(emergencyId, date, doctorId, (result) => {

            if (result._result) {
                res.status(200).json({ responded: true });
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }

        });

    }

}

// Get a specific active emergency
function getActiveEmergergencyById(req, res) {

    let emergencyId = req.params.id;

    // Get the emergency
    emergency.getActiveEmergergencyById(emergencyId, (result) => {

        if (result._result) {
            res.status(200).json({ "emergency": result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Get all patient contacts
function getPatientContacts(req, res) {
    let patientId = req.params.id;

    // Get a list of all the contacts of a patient
    patient.listPatientContacts(patientId, (contacts) => {

        if (contacts) {
            res.status(200).json({ contacts });
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }

    });

}

// Get the total number of  emergencies
function getTotalEmergencies(req, res) {

    let doctorId = res.locals.authData.user._id;


    // Get the total patients of the doctor
    emergency.getTotalEmergencies(doctorId, (result) => {


        if (result._result) {
            res.status(200).json({ "total": result._object.total });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}


// PATIENT PLANS

// Get one patient plan
function getPatientPlan(req, res) {

    let patientId = req.params.patientId;

    // Get a patient's plan
    patient.getPatientPlan(patientId, (plan) => {

        if (plan) {
            res.status(200).json({ plan });
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }

    });

}

// Create a plan to the patient
function createPatientPlan(req, res) {

    let patientId = req.body.patientId;
    let plan = req.body.plan;

    // Check if the request has the necessary fields
    if (patientId === undefined || plan === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Delete the patient current plan
        patient.deletePatientPlan(patientId, (deleted) => {

            // Add the new plan
            patient.createPatientPlan(patientId, plan, (result) => {

                if (result) {
                    res.status(200).json({ "edited": "true" });
                } else {
                    res.status(500).json({ "Message": "Something went wrong" });
                }

            });
        });

    }

}

// Delete a patient plan
function deletePatientPlan(req, res) {

    let patientId = req.params.patientId;

    // Delete the plan
    patient.deletePatientPlan(patientId, (deleted) => {

        if (deleted) {
            res.status(204).json();
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }

    });

}

// PATIENT FILES

// Get the patient files
function getPatientFiles(req, res) {

    let patientId = req.params.patientId;

    // Get the patient files
    patient.getFiles(patientId, (files) => {

        if (files) {
            res.status(200).json({ files });
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }

    });

}

// Get a file from the patient
function getPatientFile(req, res) {

    let patientId = req.params.patientId;
    let fileId = req.params.fileId;

    // Get the file
    patient.getFile(patientId, fileId, (file) => {
        if (file) {

            // Get the location of the file
            let fileNameLocation = file.id + file.file_name;

            // Send the file
            res.sendFile(fileNameLocation, { 'root': './API/imgs' });

        } else {
            res.status(404).json({ "Message": "File not found" });
        }
    });


}

// ALERTS SIGNALS

// Get a list of Signals Alerts
function listSignalAlerts(req, res) {

    let patientId = req.params.patientId;

    alertSignal.listSignalAlerts(patientId, (result) => {

        if (result._result) {
            res.status(200).json({ "alerts": result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Delete a signal alert
function deleteSignalAlert(req, res) {

    let alertId = req.params.alertsignalId;

    alertSignal.deleteSignalAlert(alertId, (result) => {

        if (result._result) {
            res.status(200).json({ result: true });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// CHANGE PASSWORD

function changePassword(req, res) {

    let id = res.locals.authData.user._id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    let newPasswordConf = req.body.newPasswordConf;

    // Check if the request has the necessary fields
    if (id === undefined || oldPassword === undefined || newPassword === undefined || newPasswordConf === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        if (newPassword == newPasswordConf) {

            doctor.changePassword(id, oldPassword, newPassword, (result) => {

                if (result) {
                    res.status(200).json({ result: true });
                } else {
                    res.status(500).json({ "Message": "Something went wrong" });
                }

            });

        } else {
            res.status(400).json({ "Message": "Request bad formed" });
        }

    }

}

// CHANGE PATIENT SENSORS LIMITS

function getPatientLimits(req, res) {

    let patientId = req.params.patientId;

    patient.getPatientLimits(patientId, (result) => {

        if (result._result) {
            res.status(200).json({ limits: result._object });
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });


}

function updatePatientLimits(req, res) {

    let patientId = req.params.patientId;
    let limits = req.body.limits;

    if (limits === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        patient.updatePatientLimits(patientId, limits, (result) => {

            if (result._result) {
                res.status(200).json({ result: true });
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }

        });

    }

}



exports.login = login;
exports.getDoctor = getDoctor;
exports.getDoctorPatients = getDoctorPatients;
exports.getTotalPatients = getTotalPatients;
exports.getSensors = getSensors;
exports.getPatientAllValues = getPatientAllValues;
exports.getPatientValuesBySensor = getPatientValuesBySensor;
exports.assignDevice = assignDevice;
exports.getDevices = getDevices;
exports.removeAssignedDevice = removeAssignedDevice;
exports.getViews = getViews;
exports.newView = newView;
exports.deleteView = deleteView;
exports.updateView = updateView;
exports.getAllMedicalConsultations = getAllMedicalConsultations;
exports.getMedicalConsultation = getMedicalConsultation;
exports.deleteMedicalConsultation = deleteMedicalConsultation;
exports.createMedicalConsultation = createMedicalConsultation;
exports.updateMedicalConsultation = updateMedicalConsultation;
exports.getMedicalConsultationTotal = getMedicalConsultationTotal;
exports.listPatientMonitoringSensors = listPatientMonitoringSensors;
exports.addPatientMonitoringSensors = addPatientMonitoringSensors;
exports.deletePatientMonitoringSensors = deletePatientMonitoringSensors;
exports.getPatient = getPatient;
exports.getAllMedicalConsultationsByPatient = getAllMedicalConsultationsByPatient;
exports.getListOfActiveEmergencies = getListOfActiveEmergencies;
exports.getListOfRespondingEmergencies = getListOfRespondingEmergencies;
exports.respondToEmergency = respondToEmergency;
exports.closeEmergency = closeEmergency;
exports.getActiveEmergergencyById = getActiveEmergergencyById;
exports.getPatientContacts = getPatientContacts;
exports.getPatientPlan = getPatientPlan;
exports.createPatientPlan = createPatientPlan;
exports.deletePatientPlan = deletePatientPlan;
exports.getPatientFiles = getPatientFiles;
exports.getPatientFile = getPatientFile;
exports.getPatientPicture = getPatientPicture;
exports.getTotalEmergencies = getTotalEmergencies;
exports.listSignalAlerts = listSignalAlerts;
exports.deleteSignalAlert = deleteSignalAlert;
exports.changePassword = changePassword;
exports.getPatientLimits = getPatientLimits;
exports.updatePatientLimits = updatePatientLimits;