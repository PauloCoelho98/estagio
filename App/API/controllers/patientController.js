"use strict";

const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

const fs = require('fs');

var sanitizer = require('sanitize')();

const async = require("async");
const fileUpload = require('express-fileupload');
app.use(fileUpload());

const patient = require("../models/patientModule");
const medicalConsultation = require("../models/medicalConsultationModule");

// user login
function login(req, res) {

    let email = req.body.email;
    let password = req.body.password;

    // Check if the request has the necessary fields
    if (email === undefined || password === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Check if password and doctor email check
        patient.loginPatient(email, password, (result, patientObj) => {
            if (result) { // if introduced valid credentials

                let patientId = patientObj._id;
                let role = 'Patient';

                // Add role to the token
                patientObj['_role'] = role;

                // Get the secret key from file to generate a token
                fs.readFile('./API/datafiles/jwtsecretkey.json', 'utf8', function (err, secretKey) {
                    if (err) {
                        //throw err;
                        console.log(err)
                        res.status(500).json({});
                    } else {

                        // Parse file content to json
                        secretKey = JSON.parse(secretKey);

                        // Generate a token
                        jwt.sign({ user: patientObj }, secretKey.secret_key, { expiresIn: '1h' }, (err, token) => {

                            // Send to the user the token the it's info: id and type
                            res.json({
                                token,
                                "userId": patientId,
                                "userType": role
                            });
                        });
                    }
                });
            } else { // if not introduced valid credentials

                res.status(401).json({ "Error": "Invalid credintials" });

            }
        });
    }
}

// Get the patient object
function getPatient(req, res) {

    let id = res.locals.authData.user._id;

    // Get the patient info
    patient.getPatient(id, (patient) => {

        if (patient) {
            res.status(200).json(patient);
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }

    });

}

// Get Patient plan
function getPatientPlan(req, res) {

    let id = res.locals.authData.user._id;

    // Get the patient plan
    patient.getPatientPlan(id, (plan) => {
        if (plan) {
            res.status(200).json(plan);
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }
    });

}

// Get patient medical consultations
function getPatientMedicalConsultation(req, res) {

    let id = res.locals.authData.user._id;

    // Get the medical consultations
    medicalConsultation.getListOfMedicalConsultationsByPatient(id, (result) => {

        if (result._result) {
            res.status(200).json(result._object);
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Get all existing sensors
function getAllSensors(req, res) {

    // Get all existing sensors
    patient.getSensors((sensors) => {
        if (sensors) {
            res.status(200).json({ sensors });
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }
    });

}

// Upload a file
function uploadFile(req, res) {

    let id = res.locals.authData.user._id;
    let description = req.body.description;

    // Get the current date
    let now = new Date().toISOString().slice(0, 19).replace('T', ' ');

    // Check if the request has files
    if (req.files) {

        let files = req.files.myfiles; // name in the input

        if (files.constructor !== Array) {

            // Convert artists into an array
            files = [files];

        }

        // assuming files is an array of files to upload
        async.each(files, function (file, callback) {

            const path = "./API/imgs/";

            // Insert into the database the file location
            patient.insertFile(id, file.name, description, now, (resultInsertDB) => {

                if (resultInsertDB) {

                    // Use the mv() method to place the file somewhere on your server
                    file.mv(path + resultInsertDB + file.name, function (err) {
                        if (err) {
                            callback(err);
                            //return res.status(500).send(err);
                        } else {
                            callback();
                        }

                    });

                } else {
                    res.status(500).json({});
                }

            });

        }, function (err) {
            // if any of the file processing produced an error, err would equal that error
            if (err) {
                res.status(500).json({"Message": "Something went wrong"});
            } else {

                res.status(200).json({});

            }
        });

    } else {
        console.log("NO FILES")
        res.status(400).json({ "error": "No files found" });
    }
}

// Get the user files
function getFiles(req, res) {


    let id = res.locals.authData.user._id;

    patient.getFiles(id, (files) => {

        if (files) {
            res.status(200).json({ files });
        } else {
            res.status(500).json({"Message": "Something went wrong"});
        }

    });

}

// Get a file
function getFile(req, res) {

    let id = res.locals.authData.user._id;

    // Get the file info from the db
    patient.getFile(id, req.params.fileId, (file) => {

        if (file) {

            // Get the location of the file
            let fileNameLocation = file.id + file.file_name;

            // Send the file
            res.sendFile(fileNameLocation, { 'root': './API/imgs' });

        } else {
            res.status(404).json({"Message": "File not found"});
        }
    });

}

function getDoctorLink(req, res) {
    let doctorId = req.params.doctorId;

    patient.getDoctorLink(doctorId, (result)=>{

        if(result) {
            res.status(200).json(result);
        }else{
            res.status(404).json({"Message": "Doctor not found"});
        }

    });

}

// CHANGE PASSWORD

function changePassword(req, res) {

    let id = res.locals.authData.user._id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    let newPasswordConf = req.body.newPasswordConf;

    // Check if the request has the necessary fields
    if (id === undefined || oldPassword === undefined || newPassword === undefined || newPasswordConf === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        if(newPassword == newPasswordConf) {

            patient.changePassword(id, oldPassword, newPassword, (result)=>{

                if(result) {
                    res.status(200).json({result:true});
                }else{
                    res.status(500).json({"Message": "Something went wrong"});
                }

            });

        }else{
            res.status(400).json({ "Message": "Request bad formed" });
        }
       
    }

}

exports.login = login;
exports.getPatient = getPatient;
exports.getPatientPlan = getPatientPlan;
exports.getPatientMedicalConsultation = getPatientMedicalConsultation;
exports.uploadFile = uploadFile;
exports.getFiles = getFiles;
exports.getFile = getFile;
exports.getAllSensors = getAllSensors;
exports.getDoctorLink = getDoctorLink;
exports.changePassword = changePassword;