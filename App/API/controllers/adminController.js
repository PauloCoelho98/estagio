"use strict";

const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

const fs = require('fs');

const spawn = require("child_process").spawn;

const async = require("async");
const fileUpload = require('express-fileupload');
app.use(fileUpload());

const admin = require("../models/adminModule");
const doctor = require("../models/doctorModule");
const patient = require("../models/patientModule");
const doctorPatient = require("../models/doctorPatientModule");
const contact = require("../models/contactModule");

// user login
function login(req, res) {

    let email = req.body.email;
    let password = req.body.password;

    // Check if the request has the necessary fields
    if (email === undefined || password === undefined) {
        res.status(400).json();
    } else {

        // Check if password and admin email check
        admin.loginAdmin(email, password, (result, adminObj) => {
            if (result) { // if introduced valid credentials

                let adminId = adminObj._id;
                let role = adminObj._role;
                let permissions = adminObj._permissions;

                // Get the secret key from a file to generate a token
                fs.readFile('./API/datafiles/jwtsecretkey.json', 'utf8', function (err, secretKey) {
                    if (err) {
                        //throw err;
                        console.log(err)
                        res.status(500).json({ "Message": "Something went wrong" });
                    } else {

                        // Parse the file content to json
                        secretKey = JSON.parse(secretKey);

                        // Create a token with the admin object
                        jwt.sign({ user: adminObj }, secretKey.secret_key, { expiresIn: '1h' }, (err, token) => {

                            // Send to the client the token and the information about the user: id, type and permissions
                            res.json({
                                token,
                                "userId": adminId,
                                "userType": role,
                                "permissions": permissions
                            });
                        });
                    }
                });

            } else { // if not introduced valid credentials

                res.status(401).json({ "Error": "Invalid credintials" });

            }
        });
    }
}

// DOCTORS 

// List all the doctors
function listDoctors(req, res) {

    // Get list of all doctors
    doctor.getListOfDoctors((result) => {

        const doctors = result._object;

        if (result._result) {
            res.status(201).json({doctors});
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Get one doctor
function getDoctor(req, res) {

    // Get the doctor
    doctor.getDoctor(req.params.id, (result) => {

        const doctor = result._object;

        if (result._result) {
            res.status(201).json({doctor});
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Add a new doctor
function addDoctor(req, res) {

    let name = req.body.name;
    let email = req.body.email;
    let password = req.body.password;
    let permissions = req.body.permissions;
    let linkCall = req.body.linkCall;

    // Check if the request has the necessary fields
    if (name === undefined || email === undefined || password === undefined || permissions === undefined || linkCall === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Check if password and 'check password' fields match
        if (req.body.password === req.body.passwordconf) {

            // Add new user information into database
            doctor.signin(name, email, password, permissions, linkCall, (result) => {

                // If inserted into database successefully
                if (result._result) {
                    
                    // Send email with the credentials
                    const pythonProcess = spawn('python',["API/sendemail.py", email, password]);

                    res.status(201).json(result._object);
                } else {
                    const errorMessage = result._errorMessage;
                    res.status(result._status).json({ "Message": errorMessage });
                }

            });
        } else {
            res.status(400).json({ "Message": "Request bad formed" });
        }
    }

}

// Delete an existing doctor
function deleteDoctor(req, res) {

    // Check if the request has the necessary fields
    if (req.params.id === undefined) {
        res.status(404).json();
    } else {

        // Delete the doctor
        doctor.deleteDoctor(req.params.id, (result) => {

            if (result._result) {
                res.status(204).json();
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }

        });
    }

}

// Update an existing doctor
function updateDoctor(req, res) {

    let id = req.params.id;
    let email = req.body.email;
    let name = req.body.name;
    let password = req.body.password;
    let passwordconf = req.body.passwordconf;
    let permissions = req.body.permissions;
    let linkCall = req.body.linkCall;

    let updatePassword = true;

    // Check if the request has the necessary fields
    if (id === undefined || email === undefined || name === undefined || permissions === undefined || linkCall === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // If both password and passwordconf are undefined, then it is not to update the password
        if (password === undefined && passwordconf === undefined) {
            updatePassword = false;
        }

        // If it is to update the password
        if (updatePassword) {
            // Check if both passwords check
            if (password === passwordconf) {

                let updatedDoctor = new doctor(id, name, email, "Doctor", permissions, linkCall);

                doctor.updateDoctor(updatedDoctor, password, (result) => {

                    if (result._result) {
                        res.status(202).json({ updatedDoctor })
                    } else {
                        const errorMessage = result._errorMessage;
                        res.status(result._status).json({ "Message": errorMessage });
                    }

                });

            } else {
                res.status(400).json({ "Message:": "Password doesn't match" });
            }
        } else { // If it is not to update the password
            let updatedDoctor = new doctor(id, name, email, "Doctor", permissions, linkCall);

            // Update the doctor
            doctor.updateDoctor(updatedDoctor, undefined, (result) => {

                if (result._result) {
                    res.status(202).json({ updatedDoctor })
                } else {
                    const errorMessage = result._errorMessage;
                    res.status(result._status).json({ "Message": errorMessage });
                }

            });

        }
    }

}

// Get the total number of doctors
function totalDoctors(req, res) {

    // Get the total number of doctors
    doctor.totalDoctors((result) => {

        if (result._result) {
            res.status(200).json(result._object)
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });
}


// PATIENTS

// List all the patients
function listPatients(req, res) {

    // Get the list of patients
    patient.getListOfPatients((patients) => {

        res.json({ patients });

    });


}

// Get one patient
function getPatient(req, res) {

    // Check if the request has the necessary fields
    if (req.params.id === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Get the patient
        patient.getPatient(req.params.id, (result) => {
            if (result) {
                res.status(200).json({ "patient": result });
            } else {
                res.status(404).json({ "Message": "Patient not found" });
            }
        });
    }

}


// Add a new patient
function addPatient(req, res) {

    let name = req.body.name;
    let contact = req.body.contact;
    let email = req.body.email;
    let password = req.body.password;
    let picture = req.body.picture;
    let description = req.body.description;
    let height = req.body.height;
    let weight = req.body.weight;
    let birthday = req.body.birthday;
    let address = req.body.address;
    birthday = birthday.split("T")[0];

    // Check if the request has the necessary fields
    if (name === undefined || contact === undefined || description === undefined
        || height === undefined || weight === undefined || birthday === undefined || address === undefined
        || email === undefined || password === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        // Check if the password and the password confirmation match
        if (password === req.body.passwordconf) {

            if (req.files) {
                // check if file upload is empty
                if (Object.keys(req.files).length === 0 && req.files.constructor === Object) {

                } else {

                    let files = req.files.newpicture; // name in the input

                    if (files.constructor !== Array) {

                        // Convert artists into an array
                        files = [files];

                    }

                    const path = "./API/patients_picture/"; // folder of the images

                    const pathToSave = "./patients_picture/";

                    let imageLocation; // complete location of the file

                    // assuming files is an array of files to upload
                    async.each(files, function (file, callback) {

                        imageLocation = pathToSave + email + file.name;

                        // Use the mv() method to place the file somewhere on your server
                        file.mv(path + email + file.name, function (err) {
                            if (err) {
                                callback(err);
                                //return res.status(500).send(err);
                            } else {
                                callback();
                            }
                        });

                    }, function (err) {
                        // if any of the file processing produced an error, err would equal that error
                        if (err) {
                            console.log(err)
                            res.status(500).json({ "Message": "Something went wrong" });
                        } else {

                            // Success

                            let newPatient = new patient(undefined, email, name, contact, imageLocation, description,
                                height, weight, birthday, address);

                            // Add new user information into database
                            patient.signin(newPatient, password, (result) => {

                                // If inserted into database successefully
                                if (result) {
                                    
                                    // Send email with the credentials
                                    const pythonProcess = spawn('python',["API/sendemail.py", email, password]);

                                    res.status(201).json(result);
                                } else {
                                    res.status(500).json({ "Message": "Something went wrong" });
                                }

                            });

                        }
                    });
                }
            } else {
                res.status(400).json({ "error": "No files found" });
            }

        } else {
            res.status(400).json({ "Message": "Request bad formed" });
        }
    }

}

// Delete an existing patient
function deletePatient(req, res) {

    // Check if the request has the necessary fields
    if (req.params.id === undefined) {
        res.status(404).json();
    } else {

        patient.deletePatient(req.params.id, (result) => {
            if (result) {
                res.status(204).json();
            } else {
                res.status(404).json({ "Patient": "Doctor not found" });
            }
        });
    }

}

// Update an existing patient
function updatePatient(req, res) {

    let id = req.params.id;
    let email = req.body.email;
    let name = req.body.name;
    let contact = req.body.contact;
    let description = req.body.description;
    let height = req.body.height;
    let weight = req.body.weight;
    let birthday = req.body.birthday;
    birthday = birthday.split("T")[0];
    let address = req.body.address;

    // Check if the request has the necessary fields
    if (id === undefined || name === undefined || contact === undefined
        || description === undefined || height === undefined || weight === undefined
        || birthday === undefined || address === undefined || email === undefined) {

        res.status(400).json({ "Message": "Request bad formed" });

    } else {

        if (req.files) {

            let files = req.files.updatepicture; // name in the input

            if (files.constructor !== Array) {

                // Convert artists into an array
                files = [files];

            }


            const path = "./API/patients_picture/"; // folder of the images

            const pathToSave = "./patients_picture/";

            let imageLocation; // complete location of the image

            // assuming files is an array of files to upload
            async.each(files, function (file, callback) {

                imageLocation = pathToSave + email + file.name;

                // Use the mv() method to place the file somewhere on your server
                file.mv(path + email + file.name, function (err) {
                    if (err) {
                        callback(err);
                        //return res.status(500).send(err);
                    } else {
                        callback();
                    }

                });

            }, function (err) {
                // if any of the file processing produced an error, err would equal that error
                if (err) {
                    console.log(err)
                    res.status(500).json({ "Message": "Something went wrong" });
                } else {

                    // Success

                    let updatedPatient = new patient(id, email, name, contact, imageLocation, description, height, weight, birthday, address);

                    // Update the patient
                    patient.updatePatient(updatedPatient, (result) => {

                        if (result) {
                            res.status(202).json({ updatedPatient })
                        } else {
                            res.status(404).json({ "Message": "Patient not found" });
                        }

                    });

                }
            });

        } else {
            let updatedPatient = new patient(id, email, name, contact, undefined, description, height, weight, birthday, address);

            // Update the patient
            patient.updatePatient(updatedPatient, (result) => {

                if (result) {
                    res.status(202).json({ updatedPatient })
                } else {
                    res.status(404).json({ "Message": "Patient not found" });
                }

            });
        }

    }

}

// Get the total number of patients
function totalPatients(req, res) {

    // Get the total number of patients
    patient.totalPatients((total) => {

        if (total) {
            res.status(200).json({ total });
        } else {
            res.status(500).json({ "Message": "Something went wrong" });
        }

    });

}

// Get the picture of a patient
function getPatientPicture(req, res) {

    // Id of the patient
    let patientId = req.params.patientId;

    // Get the patient
    patient.getPatient(patientId, (patient) => {

        if (patient) {

            // Get the picture path
            const imageName = patient._picture.split("/")[2];

            // Send the picture
            res.sendFile(imageName, { 'root': './API/patients_picture' });

        } else {
            res.status(404).json({ "Message": "Patient not found" });
        }
    });

}

// DOCTORPATIENT

// Get the list of the associations between doctor and patients
function getListOfDoctorPatient(req, res) {

    // Get the list of associations
    doctorPatient.getListOfDoctorPatient((result) => {

        if (result._result) {
            res.status(200).json({ "doctorPatient": result._object })
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }
    });


}

// Add an association between doctor and patient
function addDoctorPatient(req, res) {

    let doctorId = req.body.doctorId;
    let patientId = req.body.patientId;

    // Check if the request has the necessary fields
    if (doctorId === undefined || patientId === undefined) {
        res.status(400).json();
    } else {

        // Add the association
        doctorPatient.addDoctorPatient(doctorId, patientId, (result) => {

            if (result._result) {
                res.status(200).json({ "result": result._object })
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }

        });
    }

}

// Delete an association between doctor and patient
function deleteDoctorPatient(req, res) {

    let doctorId = req.params.doctorId;
    let patientId = req.params.patientId;

    // Check if the request has the necessary fields
    if (doctorId === undefined || patientId === undefined) {
        res.status(400).json();
    } else {

        // Delete the association
        doctorPatient.deleteDoctorPatient(doctorId, patientId, (result) => {
            if (result._result) {
                res.status(204).json()
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }
        });
    }

}

// ADMINS

// List all the admins
function listAdmins(req, res) {

    // Get list of all admins
    admin.getListOfAdmins((result) => {

        const admins = result._object;

        if (result._result) {
            res.status(202).json({ admins })
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Get one admin
function getAdmin(req, res) {

    admin.getAdmin(req.params.id, (result) => {

        const admin = result._object;

        if (result._result) {
            res.status(202).json({ admin })
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Add a new admin
function addAdmin(req, res) {

    let username = req.body.username;
    let email = req.body.email;
    let password = req.body.password;
    let permissions = req.body.permissions;

    // Check if the request has the necessary fields
    if (username === undefined || email === undefined || password === undefined || permissions === undefined) {
        res.status(400).json();
    } else {

        // Check if password and 'check password' fields match
        if (req.body.password === req.body.passwordconf) {

            // Object of the admin to create
            let adminObj = new admin(undefined, username, email, "Admin", permissions);

            // Add new user information into database
            admin.signin(adminObj, password, (result) => {

                // If inserted into database successefully
                if (result._result) {

                    // Send email with the credentials
                    const pythonProcess = spawn('python',["API/sendemail.py", email, password]);

                    res.status(201).json(result._object);
                } else {
                    const errorMessage = result._errorMessage;
                    res.status(result._status).json({ "Message": errorMessage });
                }

            });
        } else {
            res.status(400).json({ "Message": "Request bad formed" });
        }
    }
}

// Delete an existing admin
function deleteAdmin(req, res) {

    // Check if the request has the necessary fields
    if (req.params.id === undefined) {
        res.status(404).json({ "Message": "Admin not found" });
    } else {

        // Get the admin to delete
        admin.getAdmin(req.params.id, (adminObj) => {

            // If the admin to delete exists
            if (adminObj) {

                // If the admin to delete is super admin, then don't remove
                if (adminObj._role === "S_Admin") {

                    res.status(403).json({ "Message": "You don't have permissions" });

                } else { // If the admin to delete is a regular admin

                    // Delete him
                    admin.deleteAdmin(req.params.id, (result) => {
                        if (result._result) {
                            res.status(204).json();
                        } else {
                            const errorMessage = result._errorMessage;
                            res.status(result._status).json({ "Message": errorMessage });
                        }
                    });
                }
            } else {
                res.status(404).json({ "Message": "Admin not found" });
            }
        });

    }
}

// Update an existing admin
function updateAdmin(req, res) {
    let id = req.params.id;
    let email = req.body.email;
    let username = req.body.username;
    let password = req.body.password;
    let passwordconf = req.body.passwordconf;
    let permissions = req.body.permissions;

    let updatePassword = true;

    // Check if the request has the necessary fields
    if (id === undefined || email === undefined || username === undefined || permissions === undefined) {
        res.status(400).json();
    } else {

        // If both password and passwordconf are undefined, then it is not to update the password
        if (password === undefined && passwordconf === undefined) {
            updatePassword = false;
        }

        // If it is to update the password
        if (updatePassword) {
            // Check if both passwords check
            if (password === passwordconf) {

                let updatedAdmin = new admin(id, username, email, 'Admin', permissions);

                // Update the admin
                admin.updateAdmin(updatedAdmin, password, (result) => {

                    if (result._result) {
                        res.status(202).json({ updatedAdmin })
                    } else {
                        const errorMessage = result._errorMessage;
                        res.status(result._status).json({ "Message": errorMessage });
                    }

                });

            } else {
                res.status(400).json({ "Error:": "Password doesn't match" });
            }
        } else { // If it is not to update the password
            let updatedAdmin = new admin(id, username, email, 'Admin', permissions);

            // Update the admin
            admin.updateAdmin(updatedAdmin, undefined, (result) => {

                if (result._result) {
                    res.status(202).json({ updatedAdmin })
                } else {
                    const errorMessage = result._errorMessage;
                    res.status(result._status).json({ "Message": errorMessage });
                }

            });

        }
    }
}

// Get the total number of admins
function totalAdmins(req, res) {

    // Get the total number of admins
    admin.totalAdmins((result) => {

        if (result._result) {
            res.status(200).json(result._object)
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });
}

// CONTACTS

// List all the contacts
function listContacts(req, res) {

    // Get list of all doctors
    contact.listContacts((result) => {

        const contacts = result._object;

        if (result._result) {
            res.status(200).json({ contacts })
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });


}

// Get one Contact
function getContact(req, res) {

    // Get the contact
    contact.getContact(req.params.id, (result) => {
        const contact = result._object;

        if (result._result) {
            res.status(200).json({ contact })
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }
    });

}

// Add a new Contact
function addContact(req, res) {

    let name = req.body.name;
    let number = req.body.number;

    // Check if the request has the necessary fields
    if (name === undefined || number === undefined) {
        res.status(400).json();
    } else {

        // Add the contact
        contact.addContact(name, number, (result) => {

            const contact = result._object;

            // If inserted into database successefully
            if (result._result) {
                res.status(200).json({ contact })
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }

        });
    }

}

// Delete an existing Contact
function deleteContact(req, res) {

    // Check if the request has the necessary fields
    if (req.params.id === undefined) {
        res.status(404).json({ "Message": "Contact not found" });
    } else {

        // Delete the contact
        contact.deleteContact(req.params.id, (result) => {
            if (result._result) {
                res.status(204).json()
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }
        });
    }
}

// Update an existing Contact
function updateContact(req, res) {

    let id = req.params.id;
    let idBody = req.body.id;
    let name = req.body.name;
    let number = req.body.number;

    // Check if the request has the necessary fields
    if (id === undefined || name === undefined || number === undefined) {
        res.status(400).json();
    } else {

        // Check if the id of the route and the id in the body are the same
        if (id == idBody) {

            // Update the contact
            contact.updateContact(id, name, number, (result) => {

                const contact = result._object;

                // If inserted into database successefully
                if (result._result) {
                    res.status(200).json({ contact })
                } else {
                    const errorMessage = result._errorMessage;
                    res.status(result._status).json({ "Message": errorMessage });
                }

            });

        } else {
            res.status(400).json({ "Message": "Request bad formed" });
        }


    }

}

// Get the total number of contacts
function totalContacts(req, res) {

    // Get the total number of contacts
    contact.totalContacts((result) => {

        if (result._result) {
            res.status(200).json(result._object)
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// PATIENT CONTACTS

// List all the patientcontacts
function listPatientContacts(req, res) {

    // Get list of all doctors
    patient.listPatientContacts(req.params.patientId, (patientContacts) => {

        res.json({ patientContacts });

    });
}

// Get one patient Contact
function getPatientContact(req, res) {

    // Get the patient contact
    patient.getPatientContact(req.params.patientId, req.params.patientContactId, (result) => {
        if (result) {
            res.status(200).json({ "patientcontact": result });
        } else {
            res.status(404).json({ "Message": "PatientContact not found" });
        }
    });

}

// Add a new Patient Contact
function addPatientContact(req, res) {

    let patientId = req.body.patientId;
    let contactId = req.body.contactId;
    let name = req.body.name;
    let number = req.body.number;

    // Check if the request has the necessary fields
    if (patientId === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else if (contactId === undefined && name !== undefined && number !== undefined) {

        // Add standard contact
        patient.addPatientContact(patientId, contactId, name, number, (result) => {

            if (result) {
                res.status(201).json(result);
            } else {
                res.status(500).json({ "Message": "Something went wrong" });
            }

        });

    } else if (contactId !== undefined && name === undefined && number === undefined) {

        // Add new contact
        patient.addPatientContact(patientId, contactId, name, number, (result) => {

            if (result) {
                res.status(201).json(result);
            } else {
                res.status(500).json({ "Message": "Something went wrong" });
            }

        });

    } else {
        res.status(400).json({ "Message": "Request bad formed" });
    }

}

// Delete the contacts of an existing Patient
function deletePatientContacts(req, res) {

    // Check if the request has the necessary fields
    if (req.params.patientId === undefined) {
        res.status(404).json();
    } else {

        // Delete the patient contacts
        patient.deletePatientContacts(req.params.patientId, (result) => {
            if (result) {
                res.status(204).json();
            } else {
                res.status(404).json({ "Message": "Doctor not found" });
            }
        });
    }
}


// DEVICES

// List all devices
function listDevices(req, res) {

    admin.listDevices((result) => {

        let devices = result._object;

        if (result._result) {
            res.status(200).json({ devices })
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// Add a new device
function addDevice(req, res) {

    let id = req.body.id;

    if (id === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        admin.addDevice(id, (result) => {
            if (result._result) {
                res.status(201).json(result._object);
            } else {
                const errorMessage = result._errorMessage;
                res.status(result._status).json({ "Message": errorMessage });
            }
        });
    }

}

// Delete a device
function deleteDevice(req, res) {

    admin.deleteDevice(req.params.deviceId, (result) => {
        if (result._result) {
            res.status(204).json();
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }
    });


}

// Get the total number of devices
function totalDevices(req, res) {

    // Get the total number of devices
    admin.totalDevices((result) => {

        if (result._result) {
            res.status(200).json(result._object)
        } else {
            const errorMessage = result._errorMessage;
            res.status(result._status).json({ "Message": errorMessage });
        }

    });

}

// CHANGE PASSWORD

function changePassword(req, res) {

    let id = res.locals.authData.user._id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    let newPasswordConf = req.body.newPasswordConf;

    // Check if the request has the necessary fields
    if (id === undefined || oldPassword === undefined || newPassword === undefined || newPasswordConf === undefined) {
        res.status(400).json({ "Message": "Request bad formed" });
    } else {

        if(newPassword == newPasswordConf) {

            admin.changePassword(id, oldPassword, newPassword, (result)=>{

                if(result) {
                    res.status(200).json({result:true});
                }else{
                    res.status(500).json({"Message": "Something went wrong"});
                }

            });

        }else{
            res.status(400).json({ "Message": "Request bad formed" });
        }
       
    }

}

exports.login = login;
exports.listDoctors = listDoctors;
exports.addDoctor = addDoctor;
exports.deleteDoctor = deleteDoctor;
exports.updateDoctor = updateDoctor;
exports.getDoctor = getDoctor;
exports.totalDoctors = totalDoctors;
exports.listPatients = listPatients;
exports.getPatient = getPatient;
exports.addPatient = addPatient;
exports.deletePatient = deletePatient;
exports.updatePatient = updatePatient;
exports.totalPatients = totalPatients;
exports.getListOfDoctorPatient = getListOfDoctorPatient;
exports.addDoctorPatient = addDoctorPatient;
exports.deleteDoctorPatient = deleteDoctorPatient;
exports.listAdmins = listAdmins;
exports.getAdmin = getAdmin;
exports.addAdmin = addAdmin;
exports.deleteAdmin = deleteAdmin;
exports.updateAdmin = updateAdmin;
exports.totalAdmins = totalAdmins;
exports.listContacts = listContacts;
exports.getContact = getContact;
exports.addContact = addContact;
exports.updateContact = updateContact;
exports.deleteContact = deleteContact;
exports.totalContacts = totalContacts;
exports.listPatientContacts = listPatientContacts;
exports.getPatientContact = getPatientContact;
exports.addPatientContact = addPatientContact;
exports.deletePatientContacts = deletePatientContacts;
exports.getPatientPicture = getPatientPicture;
exports.listDevices = listDevices;
exports.deleteDevice = deleteDevice;
exports.addDevice = addDevice;
exports.totalDevices = totalDevices;
exports.changePassword = changePassword;