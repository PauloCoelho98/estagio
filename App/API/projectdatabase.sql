-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 25-Jun-2019 às 10:45
-- Versão do servidor: 5.7.24
-- versão do PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projectdatabase`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(300) NOT NULL,
  `role` enum('S_Admin','Admin') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `admins`
--

INSERT INTO `admins` (`id`, `username`, `email`, `password`, `role`) VALUES
(1, 'Super Admin', 'admin@email.com', '$2b$10$w4OJFlraHXxsokQ0UltV5u70HdoXsJFuZcGxw4Y/se6agNPcLFCHy', 'S_Admin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `admins_permissions`
--

DROP TABLE IF EXISTS `admins_permissions`;
CREATE TABLE IF NOT EXISTS `admins_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `permission` enum('doctors','patients','assigndoctors','contacts','devices') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adminId` (`adminId`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `admins_permissions`
--

INSERT INTO `admins_permissions` (`id`, `adminId`, `permission`) VALUES
(108, 1, 'doctors'),
(109, 1, 'patients'),
(110, 1, 'assigndoctors'),
(111, 1, 'contacts'),
(112, 1, 'devices');

-- --------------------------------------------------------

--
-- Estrutura da tabela `alert_active`
--

DROP TABLE IF EXISTS `alert_active`;
CREATE TABLE IF NOT EXISTS `alert_active` (
  `id` int(11) NOT NULL,
  `ts_start` datetime NOT NULL,
  `ts_response` datetime DEFAULT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `state` enum('waiting','responding') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `doctor_id` (`doctor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `alert_closed`
--

DROP TABLE IF EXISTS `alert_closed`;
CREATE TABLE IF NOT EXISTS `alert_closed` (
  `id` int(11) NOT NULL,
  `ts_start` datetime NOT NULL,
  `ts_response` datetime NOT NULL,
  `ts_end` datetime NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `doctor_id` (`doctor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `alert_signal`
--

DROP TABLE IF EXISTS `alert_signal`;
CREATE TABLE IF NOT EXISTS `alert_signal` (
  `id` int(11) NOT NULL,
  `sensor_id` enum('position','position_x','position_y','position_z','temp','emg_cpm','ecg_bpm','airflow_ppm','gsr_us','gsr_ohms','blood_dias','blood_syst','blood_bpm','spo2_oxy','spo2_bpm','gluco_mg','gluco_mol','spir_pef','spir_fev','snore_spm','scale_ble_weight','scale_ble_bodyfat','scale_ble_bonemass','scale_ble_musclemass','scale_ble_visceralfat','scale_ble_water','scale_ble_calories','blood_ble_dias','blood_ble_syst','blood_ble_bpm','spo2_ble_oxy','spo2_ble_bpm','gluco_ble_mg','gluco_ble_mmol','eeg_ble_attention','eeg_ble_meditation','temp_ble','button_ble') NOT NULL,
  `patient_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `number` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dashboard_view`
--

DROP TABLE IF EXISTS `dashboard_view`;
CREATE TABLE IF NOT EXISTS `dashboard_view` (
  `view_id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `view_name` varchar(50) NOT NULL,
  PRIMARY KEY (`view_id`),
  KEY `doctor_id` (`doctor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dashboard_view_sensors`
--

DROP TABLE IF EXISTS `dashboard_view_sensors`;
CREATE TABLE IF NOT EXISTS `dashboard_view_sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view_id` int(11) NOT NULL,
  `sensor_id` enum('position','position_x','position_y','position_z','temp','emg_cpm','ecg_bpm','airflow_ppm','gsr_us','gsr_ohms','blood_dias','blood_syst','blood_bpm','spo2_oxy','spo2_bpm','gluco_mg','gluco_mol','spir_pef','spir_fev','snore_spm','scale_ble_weight','scale_ble_bodyfat','scale_ble_bonemass','scale_ble_musclemass','scale_ble_visceralfat','scale_ble_water','scale_ble_calories','blood_ble_dias','blood_ble_syst','blood_ble_bpm','spo2_ble_oxy','spo2_ble_bpm','gluco_ble_mg','gluco_ble_mmol','eeg_ble_attention','eeg_ble_meditation','temp_ble','button_ble') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `view_id` (`view_id`),
  KEY `sensor_id` (`sensor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `device`
--

DROP TABLE IF EXISTS `device`;
CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `device_history`
--

DROP TABLE IF EXISTS `device_history`;
CREATE TABLE IF NOT EXISTS `device_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `doctors`
--

DROP TABLE IF EXISTS `doctors`;
CREATE TABLE IF NOT EXISTS `doctors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` enum('Doctor') NOT NULL,
  `link_call` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `doctors_patients`
--

DROP TABLE IF EXISTS `doctors_patients`;
CREATE TABLE IF NOT EXISTS `doctors_patients` (
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  PRIMARY KEY (`doctor_id`,`patient_id`),
  KEY `doctor_id` (`doctor_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `doctors_permissions`
--

DROP TABLE IF EXISTS `doctors_permissions`;
CREATE TABLE IF NOT EXISTS `doctors_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `permission` enum('doctor','emergency') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doctor_id` (`doctor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `medical_consultation`
--

DROP TABLE IF EXISTS `medical_consultation`;
CREATE TABLE IF NOT EXISTS `medical_consultation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doctor_id` (`doctor_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `patients`
--

DROP TABLE IF EXISTS `patients`;
CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(300) NOT NULL,
  `name` varchar(50) NOT NULL,
  `contact` bigint(20) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `height` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(300) NOT NULL,
  `current_device_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `current_device_id` (`current_device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `patient_contacts`
--

DROP TABLE IF EXISTS `patient_contacts`;
CREATE TABLE IF NOT EXISTS `patient_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `number` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `patient_files`
--

DROP TABLE IF EXISTS `patient_files`;
CREATE TABLE IF NOT EXISTS `patient_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `patient_monitoring_sensors`
--

DROP TABLE IF EXISTS `patient_monitoring_sensors`;
CREATE TABLE IF NOT EXISTS `patient_monitoring_sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `sensor_id` enum('position','position_x','position_y','position_z','temp','emg_cpm','ecg_bpm','airflow_ppm','gsr_us','gsr_ohms','blood_dias','blood_syst','blood_bpm','spo2_oxy','spo2_bpm','gluco_mg','gluco_mol','spir_pef','spir_fev','snore_spm','scale_ble_weight','scale_ble_bodyfat','scale_ble_bonemass','scale_ble_musclemass','scale_ble_visceralfat','scale_ble_water','scale_ble_calories','blood_ble_dias','blood_ble_syst','blood_ble_bpm','spo2_ble_oxy','spo2_ble_bpm','gluco_ble_mg','gluco_ble_mmol','eeg_ble_attention','eeg_ble_meditation','temp_ble','button_ble') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`),
  KEY `sensor_id` (`sensor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `patient_plan`
--

DROP TABLE IF EXISTS `patient_plan`;
CREATE TABLE IF NOT EXISTS `patient_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `sensor_id` enum('position','position_x','position_y','position_z','temp','emg_cpm','ecg_bpm','airflow_ppm','gsr_us','gsr_ohms','blood_dias','blood_syst','blood_bpm','spo2_oxy','spo2_bpm','gluco_mg','gluco_mol','spir_pef','spir_fev','snore_spm','scale_ble_weight','scale_ble_bodyfat','scale_ble_bonemass','scale_ble_musclemass','scale_ble_visceralfat','scale_ble_water','scale_ble_calories','blood_ble_dias','blood_ble_syst','blood_ble_bpm','spo2_ble_oxy','spo2_ble_bpm','gluco_ble_mg','gluco_ble_mmol','eeg_ble_attention','eeg_ble_meditation','temp_ble','button_ble') NOT NULL,
  `day_week` enum('monday','tuesday','wednesday','thursday','friday','saturday','sunday') NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `patient_sensors_limits`
--

DROP TABLE IF EXISTS `patient_sensors_limits`;
CREATE TABLE IF NOT EXISTS `patient_sensors_limits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `sensor_id` enum('position','position_x','position_y','position_z','temp','emg_cpm','ecg_bpm','airflow_ppm','gsr_us','gsr_ohms','blood_dias','blood_syst','blood_bpm','spo2_oxy','spo2_bpm','gluco_mg','gluco_mol','spir_pef','spir_fev','snore_spm','scale_ble_weight','scale_ble_bodyfat','scale_ble_bonemass','scale_ble_musclemass','scale_ble_visceralfat','scale_ble_water','scale_ble_calories','blood_ble_dias','blood_ble_syst','blood_ble_bpm','spo2_ble_oxy','spo2_ble_bpm','gluco_ble_mg','gluco_ble_mmol','eeg_ble_attention','eeg_ble_meditation','temp_ble','button_ble') NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sensors`
--

DROP TABLE IF EXISTS `sensors`;
CREATE TABLE IF NOT EXISTS `sensors` (
  `sensor_id` enum('position','position_x','position_y','position_z','temp','emg_cpm','ecg_bpm','airflow_ppm','gsr_us','gsr_ohms','blood_dias','blood_syst','blood_bpm','spo2_oxy','spo2_bpm','gluco_mg','gluco_mol','spir_pef','spir_fev','snore_spm','scale_ble_weight','scale_ble_bodyfat','scale_ble_bonemass','scale_ble_musclemass','scale_ble_visceralfat','scale_ble_water','scale_ble_calories','blood_ble_dias','blood_ble_syst','blood_ble_bpm','spo2_ble_oxy','spo2_ble_bpm','gluco_ble_mg','gluco_ble_mmol','eeg_ble_attention','eeg_ble_meditation','temp_ble','button_ble') NOT NULL,
  `name` varchar(50) NOT NULL,
  `units` varchar(100) NOT NULL,
  `active` enum('no','yes') NOT NULL,
  `min` int(11) DEFAULT NULL,
  `max` int(11) DEFAULT NULL,
  PRIMARY KEY (`sensor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sensors`
--

INSERT INTO `sensors` (`sensor_id`, `name`, `units`, `active`, `min`, `max`) VALUES
('position', 'Body position', '1 supine, 2 left, 3 right, 4 prone, 5 stand or sit, 6 non-defined', 'no', NULL, NULL),
('position_x', 'X axis acc', 'g', 'no', NULL, NULL),
('position_y', 'X axis acc', 'g', 'no', NULL, NULL),
('position_z', 'X axis acc', 'g', 'no', NULL, NULL),
('temp', 'Temperature', 'º C', 'no', 36, 37),
('emg_cpm', 'Muscle contraction', 'cpm', 'no', 0, 10),
('ecg_bpm', 'Heart rate', 'bpm', 'no', 60, 120),
('airflow_ppm', 'Respiratory rate', 'ppm', 'no', 12, 25),
('gsr_us', 'Conductance', 'us', 'no', 2, 7),
('gsr_ohms', 'Resistance', 'ohms', 'no', NULL, NULL),
('blood_dias', 'Diastolic pressure', 'mmHg', 'no', 40, 80),
('blood_syst', 'Systolic pressure', 'mmHg', 'no', 80, 120),
('blood_bpm', 'Heart rate', 'bpm', 'no', 60, 120),
('spo2_oxy', 'Oxygen saturation', '%', 'no', 95, 98),
('spo2_bpm', 'Heart rate', 'bpm', 'no', 60, 120),
('gluco_mg', 'Glucose mg', 'mg/dl', 'no', 72, 114),
('gluco_mol', 'Glucose mmol', 'mmol/l', 'no', NULL, NULL),
('spir_pef', 'PEF', 'spir_pef', 'no', 540, 780),
('spir_fev', 'FEV1', 'spir_fev', 'no', 180, 300),
('snore_spm', 'Snore rate', 'spm', 'no', 12, 25),
('scale_ble_weight', 'Wheight', 'kg', 'no', 40, 120),
('scale_ble_bodyfat', 'Bodyfat', '%', 'no', NULL, NULL),
('scale_ble_bonemass', 'Bonemass', '%', 'no', NULL, NULL),
('scale_ble_musclemass', 'Musclemass', '%', 'no', NULL, NULL),
('scale_ble_visceralfat', 'Visceralfat', '%', 'no', NULL, NULL),
('scale_ble_water', 'Water', '%', 'no', NULL, NULL),
('scale_ble_calories', 'Calories', 'kcal', 'no', NULL, NULL),
('blood_ble_dias', 'Diastolic pressure', 'mmHg', 'no', NULL, NULL),
('blood_ble_syst', 'Systolic pressure', 'mmHg', 'no', NULL, NULL),
('blood_ble_bpm', 'Heart rate', 'bpm', 'no', 40, 120),
('spo2_ble_oxy', 'Oxygen saturation', '%', 'no', NULL, NULL),
('spo2_ble_bpm', 'Heart rate', 'bpm', 'no', 40, 120),
('gluco_ble_mg', 'Glucose', 'mg/dl', 'no', NULL, NULL),
('gluco_ble_mmol', 'Glucose mmol', 'mmol/l', 'no', NULL, NULL),
('eeg_ble_attention', 'EEG Attention', '%', 'no', NULL, NULL),
('eeg_ble_meditation', 'EEG meditation', '%', 'no', NULL, NULL),
('temp_ble', 'Temperature', 'ºC', 'no', NULL, NULL),
('button_ble', 'Alarm button', '0 off, 1 on', 'no', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sensors_values`
--

DROP TABLE IF EXISTS `sensors_values`;
CREATE TABLE IF NOT EXISTS `sensors_values` (
  `id` int(11) NOT NULL,
  `value` float NOT NULL,
  `ts` datetime NOT NULL,
  `sensor_id` enum('position','position_x','position_y','position_z','temp','emg_cpm','ecg_bpm','airflow_ppm','gsr_us','gsr_ohms','blood_dias','blood_syst','blood_bpm','spo2_oxy','spo2_bpm','gluco_mg','gluco_mol','spir_pef','spir_fev','snore_spm','scale_ble_weight','scale_ble_bodyfat','scale_ble_bonemass','scale_ble_musclemass','scale_ble_visceralfat','scale_ble_water','scale_ble_calories','blood_ble_dias','blood_ble_syst','blood_ble_bpm','spo2_ble_oxy','spo2_ble_bpm','gluco_ble_mg','gluco_ble_mmol','eeg_ble_attention','eeg_ble_meditation','temp_ble','button_ble') NOT NULL,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sensor_id` (`sensor_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `admins_permissions`
--
ALTER TABLE `admins_permissions`
  ADD CONSTRAINT `admins_permissions_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `alert_active`
--
ALTER TABLE `alert_active`
  ADD CONSTRAINT `alert_active_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alert_active_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `alert_closed`
--
ALTER TABLE `alert_closed`
  ADD CONSTRAINT `alert_closed_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alert_closed_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `alert_signal`
--
ALTER TABLE `alert_signal`
  ADD CONSTRAINT `alert_signal_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `dashboard_view`
--
ALTER TABLE `dashboard_view`
  ADD CONSTRAINT `dashboard_view_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `dashboard_view_sensors`
--
ALTER TABLE `dashboard_view_sensors`
  ADD CONSTRAINT `dashboard_view_sensors_ibfk_2` FOREIGN KEY (`view_id`) REFERENCES `dashboard_view` (`view_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `device_history`
--
ALTER TABLE `device_history`
  ADD CONSTRAINT `device_history_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `device_history_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `doctors_patients`
--
ALTER TABLE `doctors_patients`
  ADD CONSTRAINT `fk_doctors_patients_doc_id` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_doctors_patients_pat_id` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `doctors_permissions`
--
ALTER TABLE `doctors_permissions`
  ADD CONSTRAINT `doctors_permissions_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `medical_consultation`
--
ALTER TABLE `medical_consultation`
  ADD CONSTRAINT `medical_consultation_doctor_id_fk` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `medical_consultation_patient_id_fk` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `patients`
--
ALTER TABLE `patients`
  ADD CONSTRAINT `patients_ibfk_1` FOREIGN KEY (`current_device_id`) REFERENCES `device` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `patient_contacts`
--
ALTER TABLE `patient_contacts`
  ADD CONSTRAINT `patient_contacts_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `patient_contacts_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `patient_files`
--
ALTER TABLE `patient_files`
  ADD CONSTRAINT `patient_files_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `patient_monitoring_sensors`
--
ALTER TABLE `patient_monitoring_sensors`
  ADD CONSTRAINT `patient_monitoring_sensors_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `patient_plan`
--
ALTER TABLE `patient_plan`
  ADD CONSTRAINT `patient_plan_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `patient_sensors_limits`
--
ALTER TABLE `patient_sensors_limits`
  ADD CONSTRAINT `patient_sensors_limits_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `sensors_values`
--
ALTER TABLE `sensors_values`
  ADD CONSTRAINT `sensors_values_member_id` FOREIGN KEY (`member_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
