import { loginUser } from '../js/pedidos.js'
import { checkLogin, newLogin, logout } from '../js/myutil.js';

var app = angular.module('myAppLogin', []);

// Controller do login
app.controller('loginCtrl', function ($scope, $http, $window) {

    let thereIsLogin = checkLogin();

    if (thereIsLogin) {

        // Redirect do the index page
        $window.location.href = '/patient/index';

    } else {

        // When click in the submit button
        $scope.submitLogin = function () {

            const loginData = $scope.login;

            // Request login
            loginUser(loginData, (response) => {

                // login success
                if (response) {

                    // Loading dialog
                    let dialog = bootbox.dialog({
                        title: 'Successful login',
                        message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i> Loading...</p>',
                        closeButton: false
                    });

                    let expiration = new Date();
                    const hoursTokenIsValid = 1;
                    expiration.setTime(expiration.getTime() + (hoursTokenIsValid * 60 * 60 * 1000));

                    // Save login data into cookies
                    newLogin(response.userType, response.token, response.userId, expiration, () => {

                        // Closes the loading dialog
                        dialog.modal('hide');

                        // Redirect do the index page
                        $window.location.href = '/patient/index';
                    });

                } else { // login fail
                    bootbox.alert({
                        message: "Login failed, please try again!",
                        size: 'small',
                        backdrop: true
                    });
                }
            });
        }
    }
});