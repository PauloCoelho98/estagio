import { checkLogin, logout } from '../js/myutil.js';
import { getPatient, getPatientPlan, getPatientConsultations, uploadFile, getPatientFiles, getPatientSingleFile, getAllSensors, getDoctorLink } from '../js/pedidos.js'

var app = angular.module('myApp');

// Home controller
app.controller('homeCtrl', function ($scope, $window) {

    let thereIsLogin = checkLogin();

    // Check if the user has permitions to access this page
    function hasPermission() {
        if (thereIsLogin.userType === "Patient") {
            return true;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/patient/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        let monday = [];
        let tuesday = [];
        let wednesday = [];
        let thursday = [];
        let friday = [];
        let saturday = [];
        let sunday = [];

        let patientPlan = { monday, tuesday, wednesday, thursday, friday, saturday, sunday };

        // Get the patient plan
        getPatientPlan(thereIsLogin.token, (plan) => {

            // Get all sensors
            getAllSensors(thereIsLogin.token, (sensors) => {

                // For each line of the plan, append the comple object of the sensor
                for (let i = 0; i < plan.length; i++) {
                    let sensorId = plan[i].sensor_id;
                    let time = plan[i].time;

                    const sensor = sensors.sensors.filter(sensor => sensor.sensor_id == sensorId)[0];

                    patientPlan[plan[i].day_week].push({ sensorId, sensor, time });
                }

                $scope.patientPlan = patientPlan;
                $scope.$apply();

            });

        });

        // Get all consultations of the patient
        getPatientConsultations(thereIsLogin.token, (consultations) => {

            let today = new Date();

            let consultationsToGo = [];
            let consultationsDone = [];

            today = new Date(today.getTime() + today.getTimezoneOffset()*-1*60000);

            // Iterate over all consultations
            for (let i = 0; i < consultations.length; i++) {

                // Get the difference between today and the consultation day
                let date = new Date(consultations[i].date);

                // Get the number of days between consultation date and today's date
                const diffTime = date.getTime() - today.getTime();
                let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                diffDays--;

                let diffMinutes = Math.ceil(diffTime / (1000 * 60));

                // Append the difference between the current day and the consultation date to the object
                consultations[i]['daysToGo'] = diffDays;
                consultations[i]['minutesToGo'] = diffMinutes;

                // Format the date
                let dateFormatted = consultations[i].date.split("T")[0] + " - " + consultations[i].date.split("T")[1].slice(0, 5);
                consultations[i]['formattedDate'] = dateFormatted;

                // Separate the consultations by old and new
                if (diffDays < 0) {
                    consultationsDone.push(consultations[i]);
                } else {
                    consultationsToGo.push(consultations[i]);
                }

            }

            consultationsDone = consultationsDone.reverse();
            $scope.consultationsToGo = consultationsToGo;
            $scope.consultationsDone = consultationsDone;
            $scope.$apply();

        });

        // Get all patient uplouded files
        getPatientFiles(thereIsLogin.token, (files) => {

            for (let i = 0; i < files.files.length; i++) {
                let dateFormatted = files.files[i].date.split("T")[0] + " " + files.files[i].date.split("T")[1].slice(0, 8);
                files.files[i].date = dateFormatted
            }

            $scope.files = files.files;
            $scope.$apply();
        });

        // Get a patient file
        $scope.getFileData = function (fileId) {

            getPatientSingleFile(thereIsLogin.token, fileId)

        }

        // Get the day of the week based on a numeric value
        function getTodayDayOfTheWeek() {
            let d = new Date();
            let n = d.getDay();
            let day = '';

            switch (n) {
                case 0:
                    day = 'sunday';
                    break;
                case 1:
                    day = 'monday';
                    break;
                case 2:
                    day = 'tuesday';
                    break;
                case 3:
                    day = 'wednesday';
                    break;
                case 4:
                    day = 'thursday';
                    break;
                case 5:
                    day = 'friday';
                    break;
                case 6:
                    day = 'saturday';
                    break;
            }

            return day;

        }

        // Remove all styles from the plan
        let elementsPlan = document.getElementsByClassName("planday");
        for (let i = 0; i < elementsPlan.length; i++) {
            elementsPlan[i].classList.remove("bg-primary");
            elementsPlan[i].classList.remove("text-white");
        }

        // Add styles to identify the today's plan
        let today = getTodayDayOfTheWeek();
        document.getElementById("planday" + today).classList.add("bg-primary");
        document.getElementById("planday" + today).classList.add("text-white");

        // When submit a file
        $(document).ready(function () {
            // evento de "submit"
            $("#submit").click(function (event) {
                // parar o envio para que possamos faze-lo manualmente.
                event.preventDefault();

                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        // capture o formulário
                        var form = $('#upload')[0];
                        // crie um FormData {Object}
                        var data = new FormData(form);
                        // caso queira adicionar um campo extra ao FormData
                        // data.append("customfield", "Este é um campo extra para teste");
                        // desabilitar o botão de "submit" para evitar multiplos envios até receber uma resposta
                        $("#submit").prop("disabled", true);
                        // processar

                        uploadFile(thereIsLogin.token, data, (result) => {
                            if (result) {
                                document.getElementById("upload").reset();
                                bootbox.alert({
                                    message: "Done!",
                                    backdrop: true
                                });
                            } else {
                                bootbox.alert({
                                    message: "Failed",
                                    backdrop: true
                                });
                            }
                        })
                    }
                });


            });
        });

        // When click to call
        $scope.makeCall = function(doctorId) {

            // Get the link of the doctor to call
            getDoctorLink(thereIsLogin.token, doctorId, (link)=>{

                // Link of the call
                let linkToCall = link.link_call;

                // Add parameter to the link: name of the patient
                linkToCall = linkToCall + "?name=" + $scope.name;

                let win = window.open(linkToCall, '_blank');
                win.focus();

            });

        }


    }

});