var app = angular.module('myApp', ["ngRoute"]);

// Routes of the web app (patient)
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "/views/home.htm"
        })
        .when("/403", {
            templateUrl: "/views/403.htm"
        })
        .otherwise({
            templateUrl: "/views/404.htm"
        })
});