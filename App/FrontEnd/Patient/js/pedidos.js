import { checkLogin } from './myutil.js'

export const domain = "http://localhost:8080";       // -> Para usar a API local
//export const domain = "https://localhost:8080";      // -> Para usar a API local em https com self-signed certificate

/**
 * Admin client
 * @param {*} dataSend data to send with the request
 * @param {*} callback response after the api request
 */
export function loginUser(dataSend, callback) {
    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/patient/login",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json"
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the patient object
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function getPatient(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/patient/patient/",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get the patient plan
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function getPatientPlan(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/patient/plan/",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get the patient consultations
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function getPatientConsultations(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/patient/medicalconsultation/",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Uplouad a file
 * @param {*} token access token
 * @param {*} data data to send with the request
 * @param {*} callback response after the api request
 */
export function uploadFile(token, data, callback) {
    //var data = JSON.stringify(dataSend);

    /*$.ajax({
        type: "POST",
        url: domain + "/api/doctor/login",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json"
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });*/

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: domain + "/upload",
        data: data,
        headers: {
            "Authorization": "Bearer " + token
        },
        processData: false, // impedir que o jQuery tranforma a "data" em querystring
        contentType: false, // desabilitar o cabeçalho "Content-Type"
        //cache: false, // desabilitar o "cache"
        //timeout: 600000, // definir um tempo limite (opcional)
        // manipular o sucesso da requisição
        success: function (data) {
            // reativar o botão de "submit"
            $("#submit").prop("disabled", false);
            callback(true);
        },
        // manipular erros da requisição
        error: function (e) {
            // reativar o botão de "submit"
            $("#submit").prop("disabled", false);
            callback(false);
        }
    });

}

/**
 * Get all patient files info
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function getPatientFiles(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/patient/getfiles",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get a file of the patient
 * @param {*} token access token
 * @param {*} fileId file id
 */
export function getPatientSingleFile(token, fileId) {

    var request = new XMLHttpRequest();
    request.open('GET', domain + "/patient/getfile/" + fileId, true);
    request.setRequestHeader('Authorization', 'Bearer ' + token);
    request.responseType = 'arraybuffer';
    request.onload = function (e) {
        var data = new Uint8Array(this.response);
        var raw = String.fromCharCode.apply(null, data);
        var base64 = btoa(raw);
        var src = "data:image;base64," + base64;

        document.getElementById("file" + fileId).src = src;
    };

    request.send();

}

/**
 * Get all sensors
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function getAllSensors(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/patient/allsensors",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get the link of a doctor to make a call
 * @param {*} token access token
 * @param {*} doctorId id of the doctor
 * @param {*} callback response after the api request
 */
export function getDoctorLink(token, doctorId, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/patient/doctorlink/" + doctorId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Request to change the password of the current user
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function changePassword(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "PATCH",
        url: domain + "/api/patient/password",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}