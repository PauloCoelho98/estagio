import { checkLogin } from './myutil.js'

export const domain = "http://localhost:8080";       // -> Para usar a API local
//export const domain = "https://localhost:8080";      // -> Para usar a API local em https com self-signed certificate


// DOCTORS REQUESTS

/**
 * Admin login
 * @param {*} dataSend data to send with the request
 * @param {*} callback response after the api request
 */
export function loginUser($http, dataSend, callback) {

    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/admin/login",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json"
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all doctors in the database
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getDoctors(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/doctor",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get a specific doctor
 * @param {*} token  access token
 * @param {*} id id of the doctor
 * @param {*} callback response after api request
 */
export function getDoctor(token, id, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/doctor/" + id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Add a new doctor to the database
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function addNewDoctor(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "POST",
        url: domain + "/api/admin/doctor",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete a doctor
 * @param {*} token access token 
 * @param {*} id doctor id
 * @param {*} callback response after the api request
 */
export function deleteDoctor(token, id, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/admin/doctor/" + id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Update a doctor
 * @param {*} dataSend data to send with the request 
 * @param {*} id doctor id
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function updateDoctor(dataSend, id, token , callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "PUT",
        url: domain + "/api/admin/doctor/" + id,
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the total number of doctor
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function totalDoctors(token , callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/admin/totaldoctors",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

// PATIENTS

/**
 * Get all patients in the database
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getPatients(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/patient",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get a patient
 * @param {*} token access token 
 * @param {*} id id of the patient
 * @param {*} callback response after the api request
 */
export function getPatient(token, id, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/patient/" + id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Add a new patient to the database
 * @param {*} data data to send with the request
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function addNewPatient(data, token, callback) {
    /*var data = JSON.stringify(dataSend);
    $.ajax({
        type: "POST",
        url: domain + "/api/admin/patient",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });*/

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: domain + "/api/admin/patient",
        data: data,
        headers: {
            "Authorization": "Bearer " + token
        },
        processData: false, // impedir que o jQuery tranforma a "data" em querystring
        contentType: false, // desabilitar o cabeçalho "Content-Type"
        //cache: false, // desabilitar o "cache"
        //timeout: 600000, // definir um tempo limite (opcional)
        // manipular o sucesso da requisição
        success: function (data) {
            // reativar o botão de "submit"
            callback(data);
        },
        // manipular erros da requisição
        error: function (e) {
            // reativar o botão de "submit"
            callback(false);
        }
    });

}

/**
 * Delete a patient
 * @param {*} token access token
 * @param {*} id patient
 * @param {*} callback response after the api request
 */
export function deletePatient(token, id, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/admin/patient/" + id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Update a patient
 * @param {*} dataSend data to send with the request
 * @param {*} id patient
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function updatePatient(data, id, token , callback) {
    /*var data = JSON.stringify(dataSend);
    $.ajax({
        type: "PUT",
        url: domain + "/api/admin/patient/" + id,
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });*/
    $.ajax({
        type: "PUT",
        enctype: 'multipart/form-data',
        url: domain + "/api/admin/patient/" + id,
        data: data,
        headers: {
            "Authorization": "Bearer " + token
        },
        processData: false, // impedir que o jQuery tranforma a "data" em querystring
        contentType: false, // desabilitar o cabeçalho "Content-Type"
        //cache: false, // desabilitar o "cache"
        //timeout: 600000, // definir um tempo limite (opcional)
        // manipular o sucesso da requisição
        success: function (data) {
            // reativar o botão de "submit"
            callback(data);
        },
        // manipular erros da requisição
        error: function (e) {
            // reativar o botão de "submit"
            callback(false);
        }
    });
}

/**
 * Get the total number of patients
 * @param {*} token access token
 * @param {*} callback api response after request
 */
export function totalPatients(token , callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/admin/totalpatients",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

// DOCTOR PATIENTS

/**
 * Get all doctors and his patients in the database
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getDoctorsPatients(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/doctorpatient",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}


/**
 * Add a new doctor and his patient to the database
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function addDoctorPatient(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "POST",
        url: domain + "/api/admin/doctorpatient",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete an association between doctor and patient
 * @param {*} token access token
 * @param {*} doctorId if of the doctor
 * @param {*} patientId id of the patient
 * @param {*} callback response after api request
 */
export function deleteDoctorPatient(token, doctorId, patientId, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/admin/doctorpatient/" + doctorId + "/" + patientId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}


// ADMINS

/**
 * Get all admins in the database
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getAdmins(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/admin",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Add a new admin to the database
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function addNewAdmin(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "POST",
        url: domain + "/api/admin/admin",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete an admin
 * @param {*} token access token
 * @param {*} admin id
 * @param {*} callback response after the api request
 */
export function deleteAdmin(token, id, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/admin/admin/" + id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Update an admin
 * @param {*} dataSend data to send with the request
 * @param {*} admin id
 * @param {*} token access token
 * @param {*} callback response after the api request
 */
export function updateAdmin(dataSend, id, token , callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "PUT",
        url: domain + "/api/admin/admin/" + id,
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the total number of admins
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function totalAdmins(token , callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/admin/totaladmins",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}


// CONTACTS


/**
 * Get all contacts in the database
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getContacts(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/contacts",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get a contact
 * @param {*} token access token
 * @param {*} id id of the contact
 * @param {*} callback response after api request
 */
export function getContact(token, id, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/contacts/" + id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Add a new contact
 * @param {*} dataSend data to send with the request 
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function addNewContact(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "POST",
        url: domain + "/api/admin/contacts",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete a contact
 * @param {*} token access token
 * @param {*} id id of the contact
 * @param {*} callback response after api request
 */
export function deleteContact(token, id, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/admin/contacts/" + id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Update a contact
 * @param {*} dataSend data to send with the request
 * @param {*} id id of the contact
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function updateContact(dataSend, id, token , callback) {
    dataSend['id'] = id;
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "PUT",
        url: domain + "/api/admin/contacts/" + id,
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the total number of contacts
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function totalContacts(token , callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/admin/totalcontacts",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}


// PATIENTS CONTACTS

/**
 * Add a new patient contact
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function addNewPatientContact(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "POST",
        url: domain + "/api/admin/patientcontacts/",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {console.log(errorThrown)
            callback(false);
        }
    });
}

/**
 * Get all the contacts of a patient
 * @param {*} token access token
 * @param {*} patientId id of the patient
 * @param {*} callback response after api request
 */
export function getPatientContacts(token, patientId, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/admin/patientcontacts/" + patientId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete all the contacts of a patient
 * @param {*} id id of the patient
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function deletePatientContacts(id, token , callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/admin/patientcontacts/" +id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}


// DEVICES REQUESTS

/**
 * Get all devices
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getDevices(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/device",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Add a new device
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function addNewDevice(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "POST",
        url: domain + "/api/admin/device",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete a device
 * @param {*} id device id
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function deleteDevice(id, token , callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/admin/device/" +id,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the total number of devices
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function totalDevices(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/admin/totaldevices",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Request to change the password of the current user
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function changePassword(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "PATCH",
        url: domain + "/api/admin/password",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}