var app = angular.module('myApp', ["ngRoute"]);

// Routes of the web app (Hospital Backoffice)
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "/Backoffice/views/home.htm"
        })
        .when("/doctors", {
            templateUrl: "/Backoffice/views/doctors.htm"
        })
        .when("/patients", {
            templateUrl: "/Backoffice/views/patients.htm"
        })
        .when("/assigndoctors", {
            templateUrl: "/Backoffice/views/assigndoctors.htm"
        })
        .when("/admins", {
            templateUrl: "/Backoffice/views/admins.htm"
        })
        .when("/contacts", {
            templateUrl: "/Backoffice/views/contacts.htm"
        })
        .when("/devices", {
            templateUrl: "/Backoffice/views/devices.htm"
        })
        .when("/admins/403", {
            templateUrl: "/Backoffice/views/403.htm"
        })
        .otherwise({
            templateUrl: "/Backoffice/views/404.htm"
        })
});