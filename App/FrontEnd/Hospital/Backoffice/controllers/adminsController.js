import { checkLogin, logout, paginationSplitInChuncks } from '../js/myutil.js';
import { getAdmins, addNewAdmin, deleteAdmin, updateAdmin } from '../js/pedidos.js';

var app = angular.module('myApp');

// Admins controller
app.controller('adminsCtrl', function ($scope, $window) {
    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasAccess() {

        let access = false;

        // The user has access if he has super admin permissions
        if(thereIsLogin.userType === "S_Admin") {
            access = true;
        }

        return access;

    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/admin/login';

    } else if (!hasAccess()) {

        $window.location.href = '#!admins/403';

    } else {

        let admins = [];

        let adminsChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        let currentIndexEditing = 0;
        let currentIdEditing = 0;

        // Get all admins from the DB and update the view
        getAdmins(thereIsLogin.token, (response) => {

            admins = response.admins;
            $scope.admins = admins;
            $scope.$apply();

            setPagination();

        });

        // When click to add a new admin
        $scope.addNewAdmin = function () {

            // Request to add new admin
            addNewAdmin($scope.newAdmin, thereIsLogin.token, (response) => {

                if (response) {

                    // Push the new admin to the array of admins
                    admins.push(response);

                    // Hide the modal
                    $('#addAdminModal').modal('hide');

                    // Clean the modal fields
                    $scope.newAdmin.username = "";
                    $scope.newAdmin.email = "";
                    $scope.newAdmin.password = "";
                    $scope.newAdmin.passwordconf = "";
                    $scope.newAdmin.permissions = "";
                    //$scope.newAdmin.role = "";

                    // Apply the changes/ update the view
                    $scope.$apply();

                    // Update the chuncks
                    setPagination();

                    // Dialog box that informs the success of the operation
                    bootbox.alert({
                        message: "Added",
                        backdrop: true
                    });

                } else { // If didn't add the admin
                    bootbox.alert({
                        message: "Failed",
                        backdrop: true
                    });
                }
            });
        }

        // When click to update the admin
        $scope.edit = function (id) {
            $('#updateAdminModal').modal('show');

            // Search for the admin clicked in the array on admins
            for (let i = 0; i < admins.length; i++) {

                // if found
                if (admins[i]._id == id) {

                    // Update variables that contain information about index of array and id of the admin
                    currentIndexEditing = i;
                    currentIdEditing = admins[i]._id;
                }
            }

            // Update the update modal values
            $scope.updateAdmin = { username: admins[currentIndexEditing]._username, email: admins[currentIndexEditing]._email, permissions:  admins[currentIndexEditing]._permissions};

        }

        // When click to submit the updated admins
        $scope.updateAdminForm = function () {

            // Confirm that really wants to update
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to update the admin
                    updateAdmin($scope.updateAdmin, currentIdEditing, thereIsLogin.token, (response) => {

                        // If updated
                        if (response) {

                            // Update the view
                            admins[currentIndexEditing] = response.updatedAdmin;

                            // Obter qual o chunck que se localiza o exercicio editado
                            let chunckNumber = Math.floor(currentIndexEditing / elementsPerChunck);

                            // Dentro do chunck obter a posição onde se localiza o exercicio editado
                            let chunckPosition = currentIndexEditing % elementsPerChunck;

                            // Alterar o exercicio dentro da lista de chuncks
                            adminsChunks[chunckNumber][chunckPosition] = response.updatedAdmin;

                            $scope.$apply();

                            bootbox.alert({
                                message: "Updated",
                                backdrop: true
                            });

                            // Clean the modal fields
                            $scope.updateAdmin.username = "";
                            $scope.updateAdmin.email = "";
                            $scope.updateAdmin.password = "";
                            $scope.updateAdmin.passwordconf = "";
                            //$scope.updateAdmin.role = "";

                            // Hide the modal
                            $('#updateAdminModal').modal('hide');

                        } else {
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }

                    });

                }
            });

        }

        // When click to delete a new admin
        $scope.delete = function (id) {

            // Confirm that really wants to delete
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to delete the admin
                    deleteAdmin(thereIsLogin.token, id, (response) => {

                        // If deleted
                        if (response) {

                            // Informs that deleted
                            bootbox.alert({
                                message: "Deleted",
                                backdrop: true
                            });

                            // Iterate all over the array, delete the admin and update the view
                            for (let i = 0; i < admins.length; i++) {

                                if (admins[i]._id == id) {
                                    admins.splice(i, 1);
                                }
                            }

                            $scope.$apply();

                            // Update the chuncks
                            setPagination();

                        } else { // If did not deleted
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }
                    });
                }
            });
        }

        // Check if the password field and the confirm password field match
        $scope.checkPasswords = function (origin) {

            // Check the origin
            if (origin === "new") { // origin is add new admin

                // Check if the two fields match
                if ($scope.newAdmin.password === $scope.newAdmin.passwordconf) {
                    document.getElementById("newpasswordconf").setCustomValidity("");
                } else {
                    document.getElementById("newpasswordconf").setCustomValidity("Passwords must match");
                }
            } else if (origin === "update") { // origin is update a admin

                // Check if the two fields match
                if ($scope.updateAdmin.password === $scope.updateAdmin.passwordconf) {
                    document.getElementById("updatepasswordconf").setCustomValidity("");
                } else {
                    document.getElementById("updatepasswordconf").setCustomValidity("Passwords must match");
                }
            }
        }

        // Check if a password is strong enough
        $scope.checkPasswordStrength = function (origin) {

            // Id of the DOM element that contains the password to check
            let id = "";

            // Check the origin
            if (origin === "new") { // origin is add new admin
                id = "newpassword";
            } else if (origin === "update") { // origin is update admin
                id = "updatepassword";
            }

            if (id === "updatepassword" && document.getElementById(id).value === "") {
                return;
            } else {
                // Password inserted by the user
                var term = document.getElementById(id).value;

                // Pattern that checks if the inserted password has at least
                // a capital letter, a number and at least 8 characters
                var patt = /(?=^.{8,}$)(?=.*\d)(?=.*[A-Z])/;

                // Check if the password has all the requirements
                if (patt.exec(term)) {

                    document.getElementById(id).setCustomValidity("");

                } else {
                    let message = "The password must have at least 8 characters, a number and a capital letter";
                    document.getElementById(id).setCustomValidity(message);
                }
            }

        }

        // Add pagination to the page
        function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(admins, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            adminsChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.admins = adminsChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= adminsChunks.length) {
                page = adminsChunks.length - 1;
            }

            currentPage = page;
            $scope.admins = adminsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < adminsChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }

    }
});