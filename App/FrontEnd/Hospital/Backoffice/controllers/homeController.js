import { checkLogin, logout } from '../js/myutil.js';
import { totalDoctors, totalPatients, totalAdmins, totalDevices, totalContacts } from '../js/pedidos.js';

var app = angular.module('myApp');

// Home controller
app.controller('homeCtrl', function ($scope, $window) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasAccess() {
        
        // The user has access if he has super admin or admin permissions
        if (thereIsLogin.userType === "S_Admin" || thereIsLogin.userType === "Admin") {
            return true;
        } else {
            return false;
        }
    }

    // Check the statistic cards that the user can see based on his permissions
    if (thereIsLogin.userType === "S_Admin") {
        $scope.doctorsVisible = true;
        $scope.patientsVisible = true;
        $scope.adminsVisible = true;
        $scope.contactsVisible = true;
        $scope.devicesVisible = true;
    } else if(thereIsLogin.userType === "Admin"){
        
        $scope.adminsVisible = false;

        if (thereIsLogin.userPermissions.includes("doctors")) {
            $scope.doctorsVisible = true;
        } else {
            $scope.doctorsVisible = false;
        }

        if (thereIsLogin.userPermissions.includes("patients")) {
            $scope.patientsVisible = true;
        } else {
            $scope.patientsVisible = false;
        }

        if (thereIsLogin.userPermissions.includes("contacts")) {
            $scope.contactsVisible = true;
        } else {
            $scope.contactsVisible = false;
        }

        if (thereIsLogin.userPermissions.includes("devices")) {
            $scope.devicesVisible = true;
        } else {
            $scope.devicesVisible = false;
        }

    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/admin/login';

    } if (!hasAccess()) {

        $window.location.href = '#!admins/403';

    } else {

        // Request to get the total of admins
        totalAdmins(thereIsLogin.token, (result) => {
            if (result) {
                $scope.numAdmins = result.total;
                $scope.$apply();
            }else{
                $scope.numAdmins = 0;
                $scope.$apply();
            }
        });

        // Request to get the total of doctors
        totalDoctors(thereIsLogin.token, (result) => {
            if (result) {
                $scope.numDoctors = result.total;
                $scope.$apply();
            }else{
                $scope.numDoctors = 0;
                $scope.$apply();
            }
        });

        // Request to get the total of patients
        totalPatients(thereIsLogin.token, (result) => {
            if (result) {
                $scope.numPatients = result.total;
                $scope.$apply();
            }else{
                $scope.numPatients = 0;
                $scope.$apply();
            }
        });

        // Request to get the total os contacts
        totalContacts(thereIsLogin.token, (result) => {

            if (result) {
                $scope.numContacts = result.total;
                $scope.$apply();
            }else{
                $scope.numContacts = 0;
                $scope.$apply();
            }

        });

        // Request to get the total of devices
        totalDevices(thereIsLogin.token, (result)=>{
            if (result) {
                $scope.numDevices = result.total;
                $scope.$apply();
            }else{
                $scope.numDevices = 0;
                $scope.$apply();
            }
        });
        
    }

});