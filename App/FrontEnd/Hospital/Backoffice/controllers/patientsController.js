import { checkLogin, logout, paginationSplitInChuncks } from '../js/myutil.js';
import { getPatients, addNewPatient, deletePatient, updatePatient, getContacts, addNewPatientContact, getPatientContacts, deletePatientContacts } from '../js/pedidos.js';

var app = angular.module('myApp');

// Home controller
app.controller('patientsCtrl', function ($scope, $window) {
    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasAccess() {

        let access = false;

        // The user has access if he has super admin or admin permissions
        if (thereIsLogin.userType === "S_Admin") {
            access = true;
        } else if (thereIsLogin.userType === "Admin") {

            if (thereIsLogin.userPermissions.includes("patients")) {
                access = true;
            } else {
                access = false;
            }

        }

        return access;

    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/admin/login';

    } else if (!hasAccess()) {

        $window.location.href = '#!admins/403';

    } else {

        let patients = [];

        let patientsChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        let currentIndexEditing = 0;
        let currentIdEditing = 0;

        // Standard contacts when adding a new patient
        let newStandardContacts = [];
        $scope.newStandardContacts = newStandardContacts;

        // Standard contacts when updating a new patient
        let updateStandardContacts = [];
        $scope.updateStandardContacts = updateStandardContacts;

        // Own contacts when adding a new patient
        let newOwnContacts = [];
        $scope.newOwnContacts = newOwnContacts;

        // Own contacts when updating a new patient
        let updateOwnContacts = [];
        $scope.updateOwnContacts = updateOwnContacts;

        // Get all patients from the DB and update the view
        getPatients(thereIsLogin.token, (response) => {

            patients = response.patients;
            $scope.patients = patients;
            $scope.$apply();

            setPagination();

        });

        // Get all the standard contacts
        getContacts(thereIsLogin.token, (response) => {
            $scope.standardContacts = response.contacts;
            $scope.$apply();
        });

        // When click to add another standard contact to a new patient
        $scope.newAddStandardContact = function () {
            newStandardContacts.push({});
        }

        // When click to add another standard contact to an updating patient
        $scope.newUpdateStandardContact = function () {
            updateStandardContacts.push({});
        }

        // When click to remove a standard contact from a new patient
        $scope.removeAddStandardContact = function (i) {
            newStandardContacts.splice(i, 1);
            $scope.newStandardContacts = newStandardContacts;
        }

        // When click to remove a standard contact from an updating patient
        $scope.removeUpdateStandardContact = function (i) {
            updateStandardContacts.splice(i, 1);
            $scope.updateStandardContacts = updateStandardContacts;
        }

        // When click to add an own contact to a new patient
        $scope.newAddOwnContact = function () {
            newOwnContacts.push({});
            $scope.newOwnContacts = newOwnContacts;
        }

        // When click to delete an own contact from a new patient
        $scope.deleteNewOwnContact = function (i) {
            newOwnContacts.splice(i, 1);
            $scope.newOwnContacts = newOwnContacts;
        }

        // When click to add an own contact to an updating patient
        $scope.updateAddOwnContact = function () {
            updateOwnContacts.push({});
            $scope.updateOwnContacts = updateOwnContacts;
        }

        // When click to delete an own contact from an updating patient
        $scope.deleteUpdateOwnContact = function (i) {
            updateOwnContacts.splice(i, 1);
            $scope.updateOwnContacts = updateOwnContacts;
        }

        // When click to add a new patient
        $(document).ready(function () {
            // evento de "submit"
            $("#submitNewPatient").click(function (event) {
                // parar o envio para que possamos faze-lo manualmente.
                event.preventDefault();

                let listStandardContacts = [];
                let listOwnContacts = [];

                // Get all standard contacts and add to an array
                for (let i = 0; i < newStandardContacts.length; i++) {
                    let contactElement = document.getElementById("contactStandardChoose" + i);
                    let contactId = contactElement.options[contactElement.selectedIndex].value;
                    listStandardContacts.push(contactId);
                }

                // Get all own contacts and add to an array
                for (let i = 0; i < newOwnContacts.length; i++) {
                    let name = document.getElementById("owncontactname" + i).value;
                    let number = document.getElementById("owncontactnumber" + i).value;
                    let contact = { name, number };
                    listOwnContacts.push(contact);
                }

                var form = $('#addNewPatientForm')[0];
                // crie um FormData {Object}
                var data = new FormData(form);

                // Request to add new patient
                addNewPatient(data, thereIsLogin.token, (response) => {

                    var today = new Date();

                    // If added
                    if (response) {

                        // Add all standard contacts
                        for (let i = 0; i < listStandardContacts.length; i++) {
                            addNewPatientContact({ "patientId": response._id, "contactId": listStandardContacts[i] }, thereIsLogin.token, (responseContact) => { })
                        }

                        // Add all own contacts
                        for (let i = 0; i < listOwnContacts.length; i++) {
                            addNewPatientContact({ "patientId": response._id, "name": listOwnContacts[i].name, "number": listOwnContacts[i].number }, thereIsLogin.token, (responseContact) => { })
                        }

                        response._weight = parseFloat(response._weight);
                        response._height = parseFloat(response._height);
                        response._birthday = new Date(response._birthday);

                        // Push the new patient to the array of patients
                        patients.push(response);
                        $scope.patients = patients;

                        // Hide the modal
                        $('#addPatientModal').modal('hide');

                        // Clean the modal fields
                        $scope.newPatient.name = "";
                        $scope.newPatient.email = "";
                        $scope.newPatient.contact = "";
                        $scope.newPatient.picture = "";
                        $scope.newPatient.description = "";
                        $scope.newPatient.height = "";
                        $scope.newPatient.weight = "";
                        $scope.newPatient.birthday = "";
                        $scope.newPatient.address = "";
                        $scope.newPatient.password = "";
                        $scope.newPatient.passwordconf = "";
                        newStandardContacts = [];
                        $scope.newStandardContacts = newStandardContacts;
                        newOwnContacts = [];
                        $scope.newOwnContacts = newOwnContacts;

                        // Apply the changes/ update the view
                        $scope.$apply();

                        // Update the chuncks
                        setPagination();

                        // Dialog box that informs the success of the operation
                        bootbox.alert({
                            message: "Added",
                            backdrop: true
                        });

                    } else { // If didn't add the patient
                        bootbox.alert({
                            message: "Failed",
                            backdrop: true
                        });
                    }
                });
            });
        });

        // When click to update the patient
        $scope.edit = function (id) {
            $('#updatePatientModal').modal('show');

            // Search for the patient clicked in the array on patient
            for (let i = 0; i < patients.length; i++) {

                // if found
                if (patients[i]._id == id) {

                    // Update variables that contain information about index of array and id of the patient
                    currentIndexEditing = i;
                    currentIdEditing = patients[i]._id;
                }
            }

            // Put the birthday date on the input
            var dateBirthday = patients[currentIndexEditing]._birthday;
            dateBirthday = new Date(dateBirthday);

            var day = ("0" + dateBirthday.getDate()).slice(-2);
            var month = ("0" + (dateBirthday.getMonth() + 1)).slice(-2);

            dateBirthday = dateBirthday.getFullYear() + "-" + (month) + "-" + (day);

            $('#updatebirthday').val(dateBirthday);


            // Request to get the patient contacts
            getPatientContacts(thereIsLogin.token, id, (result) => {

                updateStandardContacts = [];
                updateOwnContacts = [];

                // Distribute all contacts to the corresponding arrays of own and standard contacts
                for (let i = 0; i < result.patientContacts.length; i++) {
                    if (result.patientContacts[i].hasOwnProperty('contact')) {
                        updateStandardContacts.push(result.patientContacts[i]);
                    } else {
                        updateOwnContacts.push(result.patientContacts[i]);
                    }
                }

                // Update the scope with the contacts
                $scope.updateStandardContacts = updateStandardContacts;
                $scope.updateOwnContacts = updateOwnContacts;
                $scope.$apply();


                let countStandard = 0;
                let countOwn = 0;

                // Put all the contacts in the page
                for (let i = 0; i < result.patientContacts.length; i++) {
                    if (result.patientContacts[i].hasOwnProperty('contact')) {
                        document.getElementById("contactUpdateStandardChoose" + countStandard).value = result.patientContacts[i].contact_id;
                        countStandard++;
                    } else {
                        document.getElementById("updateowncontactname" + countOwn).value = result.patientContacts[i].name;
                        document.getElementById("updateowncontactnumber" + countOwn).value = result.patientContacts[i].number;
                        countOwn++;
                    }
                }

            });

            // Update the update modal values
            $scope.updatePatient = { name: patients[currentIndexEditing]._name, email: patients[currentIndexEditing]._email, contact: patients[currentIndexEditing]._contact, picture: patients[currentIndexEditing]._picture, description: patients[currentIndexEditing]._description, height: patients[currentIndexEditing]._height, weight: patients[currentIndexEditing]._weight, address: patients[currentIndexEditing]._address };

        }

        // When click to submit the updated patient
        $(document).ready(function () {
            // evento de "submit"
            $("#submitUpdatedPatient").click(function (event) {
                // parar o envio para que possamos faze-lo manualmente.
                event.preventDefault();

                // Confirm that really wants to update
                bootbox.confirm("Are you sure?", function (result) {

                    if (result) {

                        var form = $('#updatePatientForm')[0];
                        // crie um FormData {Object}
                        var data = new FormData(form);

                        // Request to update the patient
                        updatePatient(data, currentIdEditing, thereIsLogin.token, (response) => {

                            // If updated
                            if (response) {

                                response.updatedPatient._weight = parseFloat(response.updatedPatient._weight);
                                response.updatedPatient._height = parseFloat(response.updatedPatient._height);
                                response.updatedPatient._birthday = new Date(response.updatedPatient._birthday);

                                // Delete all patient contacts
                                deletePatientContacts(currentIdEditing, thereIsLogin.token, (responseDelete) => {

                                    // Insert all standard patient contacts
                                    for (let i = 0; i < updateStandardContacts.length; i++) {

                                        let contactElement = document.getElementById("contactUpdateStandardChoose" + i);
                                        let contactId = contactElement.options[contactElement.selectedIndex].value;

                                        addNewPatientContact({ "patientId": response.updatedPatient._id, "contactId": contactId }, thereIsLogin.token, (responseInsert) => { });
                                    }

                                    // Insert all own patient contacts
                                    for (let i = 0; i < updateOwnContacts.length; i++) {
                                        let nameElement = document.getElementById("updateowncontactname" + i);
                                        let numberElement = document.getElementById("updateowncontactnumber" + i);
                                        let name = nameElement.value;
                                        let number = numberElement.value;
                                        addNewPatientContact({ "patientId": response.updatedPatient._id, "name": name, "number": number }, thereIsLogin.token, (responseInsert) => { });
                                    }

                                });

                                // Update the view
                                patients[currentIndexEditing] = response.updatedPatient;

                                // Obter qual o chunck que se localiza o exercicio editado
                                let chunckNumber = Math.floor(currentIndexEditing / elementsPerChunck);

                                // Dentro do chunck obter a posição onde se localiza o exercicio editado
                                let chunckPosition = currentIndexEditing % elementsPerChunck;

                                // Alterar o exercicio dentro da lista de chuncks
                                patientsChunks[chunckNumber][chunckPosition] = response.updatedPatient;

                                $scope.$apply();

                                bootbox.alert({
                                    message: "Updated",
                                    backdrop: true
                                });

                                // Clean the modal fields
                                $scope.updatePatient.name = "";
                                $scope.updatePatient.email = "";
                                $scope.updatePatient.contact = "";
                                $scope.updatePatient.picture = "";
                                $scope.updatePatient.description = "";
                                $scope.updatePatient.height = "";
                                $scope.updatePatient.weight = "";
                                $scope.updatePatient.birthday = "";
                                $scope.updatePatient.address = "";

                                // Hide the modal
                                $('#updatePatientModal').modal('hide');
                            } else {
                                bootbox.alert({
                                    message: "Failed",
                                    backdrop: true
                                });
                            }

                        });

                    }
                });
            });
        });


        // When click to delete a new patient
        $scope.delete = function (id) {

            // Confirm that really wants to delete
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to delete the patient
                    deletePatient(thereIsLogin.token, id, (response) => {

                        // If deleted
                        if (response) {

                            // Informs that deleted
                            bootbox.alert({
                                message: "Deleted",
                                backdrop: true
                            });

                            // Iterate all over the array, delete the patient and update the view
                            for (let i = 0; i < patients.length; i++) {

                                if (patients[i]._id == id) {
                                    patients.splice(i, 1);
                                }
                            }

                            $scope.$apply();

                            // Update the chuncks
                            setPagination();

                        } else { // If did not deleted
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }
                    });
                }
            });
        }

        // Check if the password field and the confirm password field match
        $scope.checkPasswords = function (origin) {

            // Check the origin
            if (origin === "new") { // origin is add new doctor

                // Check if the two fields match
                if ($scope.newPatient.password === $scope.newPatient.passwordconf) {
                    document.getElementById("newpasswordconf").setCustomValidity("");
                } else {
                    document.getElementById("newpasswordconf").setCustomValidity("Passwords must match");
                }
            } else if (origin === "update") { // origin is update a doctor

                // Check if the two fields match
                if ($scope.newPatient.password === $scope.newPatient.passwordconf) {
                    document.getElementById("updatepasswordconf").setCustomValidity("");
                } else {
                    document.getElementById("updatepasswordconf").setCustomValidity("Passwords must match");
                }
            }
        }

        // Check if a password is strong enough
        $scope.checkPasswordStrength = function (origin) {

            // Id of the DOM element that contains the password to check
            let id = "";

            // Check the origin
            if (origin === "new") { // origin is add new doctor
                id = "newpassword";
            } else if (origin === "update") { // origin is update doctor
                id = "updatepassword";
            }

            if (id === "updatepassword" && document.getElementById(id).value === "") {
                return;
            } else {
                // Password inserted by the user
                var term = document.getElementById(id).value;

                // Pattern that checks if the inserted password has at least 
                // a capital letter, a number and at least 8 characters
                var patt = /(?=^.{8,}$)(?=.*\d)(?=.*[A-Z])/;

                // Check if the password has all the requirements
                if (patt.exec(term)) {

                    document.getElementById(id).setCustomValidity("");

                } else {
                    let message = "The password must have at least 8 characters, a number and a capital letter";
                    document.getElementById(id).setCustomValidity(message);
                }
            }

        }

        // Add pagination to the page
        function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(patients, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            patientsChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.patients = patientsChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= patientsChunks.length) {
                page = patientsChunks.length - 1;
            }

            currentPage = page;
            $scope.patients = patientsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < patientsChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }
    }
});