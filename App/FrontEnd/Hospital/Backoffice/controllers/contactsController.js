import { checkLogin, logout, paginationSplitInChuncks } from '../js/myutil.js';
import { getContacts, getContact, updateContact, addNewContact, deleteContact } from '../js/pedidos.js';

var app = angular.module('myApp');

// Home controller
app.controller('contactsCtrl', function ($scope, $window) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasAccess() {

        let access = false;

        // The user has access if he has super admin or admin permissions
        if (thereIsLogin.userType === "S_Admin") {
            access = true;
        } else if (thereIsLogin.userType === "Admin") {

            if (thereIsLogin.userPermissions.includes("contacts")) {
                access = true;
            } else {
                access = false;
            }

        }

        return access;

    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/admin/login';

    } else if (!hasAccess()) {

        $window.location.href = '#!admins/403';

    } else {

        let contacts = [];

        let contactsChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        let currentIndexEditing = 0;
        let currentIdEditing = 0;

        // Get all contacts from the DB and update the view
        getContacts(thereIsLogin.token, (response) => {

            contacts = response.contacts;
            $scope.contacts = contacts;
            $scope.$apply();

            setPagination();

        });

        
        // When click to add a new contact
        $scope.addNewContact = function () {

            // Request to add new contact
            addNewContact($scope.newContact, thereIsLogin.token, (response) => {

                // If added 
                if (response) {

                    // Push the new contact to the array of contacts
                    contacts.push(response.contact);

                    // Hide the modal
                    $('#addContactModal').modal('hide');

                    // Clean the modal fields
                    $scope.newContact.name = "";
                    $scope.newContact.number = "";

                    // Apply the changes/ update the view
                    $scope.$apply();

                    // Update the chuncks
                    setPagination();

                    // Dialog box that informs the success of the operation
                    bootbox.alert({
                        message: "Added",
                        backdrop: true
                    });

                } else { // If didn't add the contact
                    bootbox.alert({
                        message: "Failed",
                        backdrop: true
                    });
                }
            });
        }

        
        // When click to update the contact
        $scope.edit = function (id) {
            $('#updateContactModal').modal('show');

            // Search for the contact clicked in the array on contacts
            for (let i = 0; i < contacts.length; i++) {

                // if found
                if (contacts[i]._id == id) {

                    // Update variables that contain information about index of array and id of the contact
                    currentIndexEditing = i;
                    currentIdEditing = contacts[i]._id;
                }
            }

            // Update the update modal values
            $scope.updateContact = { name: contacts[currentIndexEditing]._name, number: contacts[currentIndexEditing]._number };

        }

        
        // When click to submit the updated contact
        $scope.updateContactForm = function () {

            // Confirm that really wants to update
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to update the contact
                    updateContact($scope.updateContact, currentIdEditing, thereIsLogin.token, (response) => {

                        // If updated
                        if (response) {

                            // Update the view
                            contacts[currentIndexEditing] = response.contact;

                            // Obter qual o chunck que se localiza o exercicio editado
                            let chunckNumber = Math.floor(currentIndexEditing / elementsPerChunck);

                            // Dentro do chunck obter a posição onde se localiza o exercicio editado
                            let chunckPosition = currentIndexEditing % elementsPerChunck;

                            // Alterar o exercicio dentro da lista de chuncks
                            contactsChunks[chunckNumber][chunckPosition] = response.contact;

                            $scope.$apply();

                            bootbox.alert({
                                message: "Updated",
                                backdrop: true
                            });

                            // Clean the modal fields
                            $scope.updateContact.name = "";
                            $scope.updateContact.number = "";

                            // Hide the modal
                            $('#updateContactModal').modal('hide');
                        } else {
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }

                    });

                }
            });

        }

        
        // When click to delete a contact
        $scope.delete = function (id) {

            // Confirm that really wants to delete
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to delete the contact
                    deleteContact(thereIsLogin.token, id, (response) => {

                        // If deleted
                        if (response) {

                            // Informs that deleted
                            bootbox.alert({
                                message: "Deleted",
                                backdrop: true
                            });

                            // Iterate all over the array, delete the contact and update the view
                            for (let i = 0; i < contacts.length; i++) {

                                if (contacts[i]._id == id) {
                                    contacts.splice(i, 1);
                                }
                            }

                            $scope.$apply();

                            // Update the chuncks
                            setPagination();

                        } else { // If did not deleted
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }
                    });
                }
            });
        }

        // Add pagination to the page
        function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(contacts, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            contactsChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.contacts = contactsChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= contactsChunks.length) {
                page = contactsChunks.length - 1;
            }

            currentPage = page;
            $scope.contacts = contactsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < contactsChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }


    }
});