import { checkLogin, logout, paginationSplitInChuncks } from '../js/myutil.js';
import { getDevices, addNewDevice, deleteDevice } from '../js/pedidos.js';

var app = angular.module('myApp');

// Home controller
app.controller('devicesCtrl', function ($scope, $window) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasAccess() {

        let access = false;

        // The user has access if he has super admin or admin permissions
        if (thereIsLogin.userType === "S_Admin") {
            access = true;
        } else if (thereIsLogin.userType === "Admin") {

            if (thereIsLogin.userPermissions.includes("devices")) {
                access = true;
            } else {
                access = false;
            }

        }

        return access;

    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/admin/login';

    } else if (!hasAccess()) {

        $window.location.href = '#!admins/403';

    } else {


        let devices = [];

        /*let devicesChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        let currentIndexEditing = 0;
        let currentIdEditing = 0;*/

        // Get the devices
        getDevices(thereIsLogin.token, (devicesResp)=> {
           
            devices = devicesResp.devices;
            $scope.devices = devices;
            $scope.$apply();
            //setPagination();

        });


        // When click to submit a new device
        $scope.addNewDevice = function() {
            
            // Id of the new device
            const id = $scope.newDevice.id;

            // API request to add a new device
            addNewDevice({id}, thereIsLogin.token, (result)=>{
                
                // If added with success
                if(result) {

                    // Clear the field
                    $scope.newDevice.id = '';
                    
                    // Inform of success
                    bootbox.alert({
                        message: "Added",
                        backdrop: true
                    });

                    // Updade scope
                    devices.push({id});
                    $scope.devices = devices;
                    $scope.$apply();

                    //setPagination();

                    // Hide the modal
                    $('#addDeviceModal').modal('hide');

                }else{

                    bootbox.alert({
                        message: "Failed",
                        backdrop: true
                    });

                }

            });
        }

        // When click to delete a device
        $scope.delete = function(id) {

            // API request to delete the device
            deleteDevice(id, thereIsLogin.token, (response)=>{

                // if deleted with success
                if(response){

                    // Iterate all over the array, delete the device and update the array
                    for (let i = 0; i < devices.length; i++) {

                        if (devices[i].id == id) {
                            devices.splice(i, 1);
                        }
                    }

                    // Update the scope
                    $scope.devices = devices;
                    $scope.$apply();

                    // Inform success
                    bootbox.alert({
                        message: "Deleted",
                        backdrop: true
                    });

                }else{

                    bootbox.alert({
                        message: "Failed",
                        backdrop: true
                    });

                }

            });

        }

        // Add pagination to the page
        /*function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(devices, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            devicesChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.devices = devicesChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= devicesChunks.length) {
                page = devicesChunks.length - 1;
            }

            currentPage = page;
            $scope.devices = devicesChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < devicesChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }*/


    }
});