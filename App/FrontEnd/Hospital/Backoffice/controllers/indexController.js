import { checkLogin, logout } from '../js/myutil.js';
import { changePassword } from '../js/pedidos.js';

var app = angular.module('myApp');

// Controller do index
app.controller('indexCtrl', function ($scope, $window) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasAccess() {

        // The user has access if he has super admin or admin permissions
        if (thereIsLogin.userType === "S_Admin" || thereIsLogin.userType === "Admin") {
            return true;
        } else {
            return false;
        }
    }

    // Check the menu options that the user can see based on his permissions
    if (thereIsLogin.userType === "S_Admin") {
        $scope.doctorsVisible = true;
        $scope.patientsVisible = true;
        $scope.adminsVisible = true;
        $scope.assignDoctorsVisible = true;
        $scope.contactsVisible = true;
        $scope.devicesVisible = true;
    } else if (thereIsLogin.userType === "Admin") {

        $scope.adminsVisible = false;

        if (thereIsLogin.userPermissions.includes("doctors")) {
            $scope.doctorsVisible = true;
        } else {
            $scope.doctorsVisible = false;
        }

        if (thereIsLogin.userPermissions.includes("patients")) {
            $scope.patientsVisible = true;
        } else {
            $scope.patientsVisible = false;
        }

        if (thereIsLogin.userPermissions.includes("assigndoctors")) {
            $scope.assignDoctorsVisible = true;
        } else {
            $scope.assignDoctorsVisible = false;
        }

        if (thereIsLogin.userPermissions.includes("contacts")) {
            $scope.contactsVisible = true;
        } else {
            $scope.contactsVisible = false;
        }

        if (thereIsLogin.userPermissions.includes("devices")) {
            $scope.devicesVisible = true;
        } else {
            $scope.devicesVisible = false;
        }

    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the index page
        $window.location.href = '/admin/login';

    } if (!hasAccess()) {

        $window.location.href = '#!admins/403';

    } else {

        // When click to logout
        $scope.logoutBtn = function () {

            logout();
        }
    }

    // When click to change the password
    $scope.changePassword = function () {
        
        let data = {
            oldPassword: $scope.changePasswordField.oldPassword,
            newPassword: $scope.changePasswordField.newPassword,
            newPasswordConf: $scope.changePasswordField.newPasswordConf
        }

        // Check for confirmation
        bootbox.confirm("Are you sure?", function(confirm){ 
            if(confirm) {

                // Request to change the password
                changePassword(data, thereIsLogin.token, (result)=>{
                    if(result) {
                        bootbox.alert({
                            message: "Password updated",
                            backdrop: true
                        });

                        $scope.changePasswordField.oldPassword = "";
                        $scope.changePasswordField.newPassword = "";
                        $scope.changePasswordField.newPasswordConf = "";
                        $scope.$apply();

                         // Hide the modal
                         $('#updatePasswordModal').modal('hide');

                    }else{
                        bootbox.alert({
                            message: "Something went wrong",
                            backdrop: true
                        });
                    }
                });

            }
        });
        
        

    }

    // Check if the password field and the confirm password field match
    $scope.checkPasswords = function () {

        // Check if the two fields match
        if ($scope.changePasswordField.newPassword === $scope.changePasswordField.newPasswordConf) {
            document.getElementById("newpasswordconf").setCustomValidity("");
        } else {
            document.getElementById("newpasswordconf").setCustomValidity("Passwords must match");
        }

    }

    // Check if a password is strong enough
    $scope.checkPasswordStrength = function () {

        // Id of the DOM element that contains the password to check
        let id = "newpassword";

        // Password inserted by the user
        var term = document.getElementById(id).value;

        // Pattern that checks if the inserted password has at least
        // a capital letter, a number and at least 8 characters
        var patt = /(?=^.{8,}$)(?=.*\d)(?=.*[A-Z])/;

        // Check if the password has all the requirements
        if (patt.exec(term)) {

            document.getElementById(id).setCustomValidity("");

        } else {
            let message = "The password must have at least 8 characters, a number and a capital letter";
            document.getElementById(id).setCustomValidity(message);
        }

    }

});