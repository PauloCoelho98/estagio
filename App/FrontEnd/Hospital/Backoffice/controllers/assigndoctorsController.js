import { checkLogin, logout, paginationSplitInChuncks } from '../js/myutil.js';
import { getDoctorsPatients, addDoctorPatient, getPatients, getDoctors, getDoctor, getPatient, deleteDoctorPatient } from '../js/pedidos.js';

import { domain } from '../js/pedidos.js'

var app = angular.module('myApp');


// Home controller
app.controller('assigndoctorsCtrl', function ($scope, $window) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasAccess() {

        let access = false;
        
        // The user has access if he has super admin or admin permissions
        if (thereIsLogin.userType === "S_Admin") {
            access = true;
        } else if (thereIsLogin.userType === "Admin") {

            if (thereIsLogin.userPermissions.includes("assigndoctors")) {
                access = true;
            } else {
                access = false;
            }

        }

        return access;

    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/admin/login';

    } else if (!hasAccess()) {

        $window.location.href = '#!admins/403';

    } else {

        let doctorPatient = [];

        let doctorPatientChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        let doctors = [];
        let patients = [];

        // Get all the doctors from the database
        getDoctors(thereIsLogin.token, (doctorsResponse) => {
            doctors = doctorsResponse.doctors;
            $scope.doctors = doctors;
            $scope.$apply();
        });

        // Get all the patients from the database
        getPatients(thereIsLogin.token, (patientsResponse) => {
            patients = patientsResponse.patients;
            $scope.patients = patients;
            $scope.$apply();
        });

        // Get all the associations between doctors and patients
        getDoctorsPatients(thereIsLogin.token, (doctorPatientResponse) => {

            // Iterate all over the array of associations
            for (let i = 0; i < doctorPatientResponse.doctorPatient.length; i++) {

                let doctorId = doctorPatientResponse.doctorPatient[i].doctor_id;
                let patientId = doctorPatientResponse.doctorPatient[i].patient_id;

                // the patient of this associaton
                getPatient(thereIsLogin.token, patientId, (patientResponse) => {

                    let thisDoctorPatient = {};

                    // Add the patient complete object to the association object
                    thisDoctorPatient['patient'] = patientResponse.patient;

                    // Get the doctor of this association
                    getDoctor(thereIsLogin.token, doctorId, (doctorResponse) => {

                        
                        // Add the doctor complete object to the association object
                        thisDoctorPatient['doctor'] = doctorResponse.doctor;

                        doctorPatient.push(thisDoctorPatient);

                        // If it's the last iteration
                        if (i === doctorPatientResponse.doctorPatient.length - 1) {

                            // Update the scope
                            $scope.doctorPatient = doctorPatient;
                            $scope.$apply();

                            // Create pagination
                            setPagination();

                        }


                    });

                });

            }

        });

        // Get the picture of a patient
        $scope.getFileData = function (patientId) {

            // Get the location of the picture
            let imgSrc = document.getElementsByClassName("file" + patientId)[0].src;

            // If it's still empty, then get the picture from the server
            if (imgSrc == '') {

                var request = new XMLHttpRequest();
                request.open('GET', domain + "/api/admin/patient/" + patientId + "/picture", true);
                request.setRequestHeader('Authorization', 'Bearer ' + thereIsLogin.token);
                request.responseType = 'arraybuffer';
                request.onload = function (e) {
                    var data = new Uint8Array(this.response);
                    var raw = String.fromCharCode.apply(null, data);
                    var base64 = btoa(raw);
                    var src = "data:image;base64," + base64;

                    // Get all patients with this picture
                    let patientsWithPicure = document.getElementsByClassName("file" + patientId);

                    //Iterate over all this patient instances and put the picture
                    for(let i=0; i<patientsWithPicure.length; i++) {
                        patientsWithPicure[i].src = src
                    }

                };

                // Send the request to the server
                request.send();

            }

        }

        // When click to delete a doctorPatient
        $scope.delete = function (doctorId, patientId) {

            // Confirm that really wants to delete
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to delete the doctor
                    deleteDoctorPatient(thereIsLogin.token, doctorId, patientId, (response) => {

                        // If deleted
                        if (response) {

                            // Informs that deleted
                            bootbox.alert({
                                message: "Deleted",
                                backdrop: true
                            });

                            // Iterate all over the array, delete the doctor and update the view
                            for (let i = 0; i < doctorPatient.length; i++) {

                                if (doctorPatient[i].doctor._id == doctorId && doctorPatient[i].patient._id == patientId) {
                                    doctorPatient.splice(i, 1);
                                }
                            }

                            $scope.$apply();

                            // Update the chuncks
                            setPagination();

                        } else { // If did not deleted
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }
                    });
                }
            });
        }

        // When click to add a new association between doctor and patient
        $scope.addNewDoctorPatient = function () {

            // Get the id of the selected doctor
            let doctorElement = document.getElementById("doctorChoose");
            let doctorId = doctorElement.options[doctorElement.selectedIndex].value;

            // Get the if of the selected patient
            let patientElement = document.getElementById("patientChoose");
            let patientId = patientElement.options[patientElement.selectedIndex].value;

            // Request to the API to add the association
            addDoctorPatient({ "doctorId": doctorId, "patientId": patientId }, thereIsLogin.token, (response) => {
                if (response) {

                    // Get the object of the doctor
                    let doctor = doctors.filter(doctor => doctor._id == doctorId)[0];

                    // Get the object of the patient
                    let patient = patients.filter(patient => patient._id == patientId)[0];

                    let thisDoctorPatient = {};

                    thisDoctorPatient['patient'] = patient;
                    thisDoctorPatient['doctor'] = doctor;

                    doctorPatient.push(thisDoctorPatient);

                    // Update the scope
                    $scope.doctorPatient = doctorPatient;
                    $scope.$apply();

                    bootbox.alert({
                        message: "Success",
                        backdrop: true
                    });

                    // Hide the modal
                    $('#addDoctorPatientModal').modal('hide');

                    setPagination();

                } else {
                    bootbox.alert({
                        message: "Failed! This might happen if this assignment already exists.",
                        backdrop: true
                    });
                }
            });

        }

        // Add pagination to the page
        function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(doctorPatient, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            doctorPatientChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.doctorPatient = doctorPatientChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= doctorPatientChunks.length) {
                page = doctorPatientChunks.length - 1;
            }

            currentPage = page;
            $scope.doctorPatient = doctorPatientChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < doctorPatientChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }


    }
});