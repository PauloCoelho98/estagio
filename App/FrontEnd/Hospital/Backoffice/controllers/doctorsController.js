import { checkLogin, logout, paginationSplitInChuncks } from '../js/myutil.js';
import { getDoctors, addNewDoctor, deleteDoctor, updateDoctor } from '../js/pedidos.js';

var app = angular.module('myApp');

// Doctors controller
app.controller('doctorsCtrl', function ($scope, $window) {
    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasAccess() {

        let access = false;

        // The user has access if he has super admin or admin permissions
        if (thereIsLogin.userType === "S_Admin") {
            access = true;
        } else if (thereIsLogin.userType === "Admin") {

            if (thereIsLogin.userPermissions.includes("doctors")) {
                access = true;
            } else {
                access = false;
            }

        }

        return access;

    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/admin/login';

    } else if (!hasAccess()) {

        $window.location.href = '#!admins/403';

    } else {

        let doctors = [];

        let doctorsChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        let currentIndexEditing = 0;
        let currentIdEditing = 0;

        // Get all doctors from the DB and update the view
        getDoctors(thereIsLogin.token, (response) => {

            doctors = response.doctors;
            $scope.doctors = doctors;
            $scope.$apply();

            setPagination();

        });

        // When click to add a new doctor
        $scope.addNewDoctor = function () {

            // Request to add new doctor
            addNewDoctor($scope.newDoctor, thereIsLogin.token, (response) => {

                // If added 
                if (response) {

                    // Push the new doctor to the array of doctors
                    doctors.push(response);

                    // Hide the modal
                    $('#addDoctorModal').modal('hide');

                    // Clean the modal fields
                    $scope.newDoctor.name = "";
                    $scope.newDoctor.email = "";
                    $scope.newDoctor.password = "";
                    $scope.newDoctor.passwordconf = "";
                    $scope.newDoctor.role = "";
                    $scope.newDoctor.permissions = "";
                    $scope.newDoctor.linkCall = "";

                    // Apply the changes/ update the view
                    $scope.$apply();

                    // Update the chuncks
                    setPagination();

                    // Dialog box that informs the success of the operation
                    bootbox.alert({
                        message: "Added",
                        backdrop: true
                    });

                } else { // If didn't add the doctor
                    bootbox.alert({
                        message: "Failed",
                        backdrop: true
                    });
                }
            });
        }

        // When click to update the doctor
        $scope.edit = function (id) {
            $('#updateDoctorModal').modal('show');

            // Search for the doctor clicked in the array on doctors
            for (let i = 0; i < doctors.length; i++) {

                // if found
                if (doctors[i]._id == id) {

                    // Update variables that contain information about index of array and id of the doctor
                    currentIndexEditing = i;
                    currentIdEditing = doctors[i]._id;
                }
            }

            // Update the update modal values
            $scope.updateDoctor = { name: doctors[currentIndexEditing]._name, email: doctors[currentIndexEditing]._email, permissions: doctors[currentIndexEditing]._permissions, linkCall: doctors[currentIndexEditing]._linkCall };

        }

        // When click to submit the updated doctor
        $scope.updateDoctorForm = function () {

            // Confirm that really wants to update
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to update the doctor
                    updateDoctor($scope.updateDoctor, currentIdEditing, thereIsLogin.token, (response) => {

                        // If updated
                        if (response) {

                            // Update the view
                            doctors[currentIndexEditing] = response.updatedDoctor;

                            // Obter qual o chunck que se localiza o exercicio editado
                            let chunckNumber = Math.floor(currentIndexEditing / elementsPerChunck);

                            // Dentro do chunck obter a posição onde se localiza o exercicio editado
                            let chunckPosition = currentIndexEditing % elementsPerChunck;

                            // Alterar o exercicio dentro da lista de chuncks
                            doctorsChunks[chunckNumber][chunckPosition] = response.updatedDoctor;

                            $scope.$apply();

                            bootbox.alert({
                                message: "Updated",
                                backdrop: true
                            });

                            // Clean the modal fields
                            $scope.updateDoctor.name = "";
                            $scope.updateDoctor.email = "";
                            $scope.updateDoctor.password = "";
                            $scope.updateDoctor.passwordconf = "";
                            $scope.updateDoctor.role = "";
                            $scope.updateDoctor.linkCall = "";

                            // Hide the modal
                            $('#updateDoctorModal').modal('hide');
                        } else {
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }

                    });

                }
            });

        }

        // When click to delete a new doctor
        $scope.delete = function (id) {

            // Confirm that really wants to delete
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to delete the doctor
                    deleteDoctor(thereIsLogin.token, id, (response) => {

                        // If deleted
                        if (response) {

                            // Informs that deleted
                            bootbox.alert({
                                message: "Deleted",
                                backdrop: true
                            });

                            // Iterate all over the array, delete the doctor and update the view
                            for (let i = 0; i < doctors.length; i++) {

                                if (doctors[i]._id == id) {
                                    doctors.splice(i, 1);
                                }
                            }

                            $scope.$apply();

                            // Update the chuncks
                            setPagination();

                        } else { // If did not deleted
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }
                    });
                }
            });
        }

        // Check if the password field and the confirm password field match
        $scope.checkPasswords = function (origin) {

            // Check the origin
            if (origin === "new") { // origin is add new doctor

                // Check if the two fields match
                if ($scope.newDoctor.password === $scope.newDoctor.passwordconf) {
                    document.getElementById("newpasswordconf").setCustomValidity("");
                } else {
                    document.getElementById("newpasswordconf").setCustomValidity("Passwords must match");
                }
            } else if (origin === "update") { // origin is update a doctor

                // Check if the two fields match
                if ($scope.updateDoctor.password === $scope.updateDoctor.passwordconf) {
                    document.getElementById("updatepasswordconf").setCustomValidity("");
                } else {
                    document.getElementById("updatepasswordconf").setCustomValidity("Passwords must match");
                }
            }
        }

        // Check if a password is strong enough
        $scope.checkPasswordStrength = function (origin) {

            // Id of the DOM element that contains the password to check
            let id = "";

            // Check the origin
            if (origin === "new") { // origin is add new doctor
                id = "newpassword";
            } else if (origin === "update") { // origin is update doctor
                id = "updatepassword";
            }

            if (id === "updatepassword" && document.getElementById(id).value === "") {
                return;
            } else {
                // Password inserted by the user
                var term = document.getElementById(id).value;

                // Pattern that checks if the inserted password has at least 
                // a capital letter, a number and at least 8 characters
                var patt = /(?=^.{8,}$)(?=.*\d)(?=.*[A-Z])/;

                // Check if the password has all the requirements
                if (patt.exec(term)) {

                    document.getElementById(id).setCustomValidity("");

                } else {
                    let message = "The password must have at least 8 characters, a number and a capital letter";
                    document.getElementById(id).setCustomValidity(message);
                }
            }

        }

        // Add pagination to the page
        function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(doctors, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            doctorsChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.doctors = doctorsChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= doctorsChunks.length) {
                page = doctorsChunks.length - 1;
            }

            currentPage = page;
            $scope.doctors = doctorsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < doctorsChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }

    }
});