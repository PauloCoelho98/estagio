import { getPatientValuesBySensor } from './pedidos.js';
import { checkLogin } from './myutil.js';

// Array of charts
let charts = [];

let chartDetailObj;

let currentPatientId = -1;

/**
 * Get the options of a chart
 * @param {*} data the data that the chart will contain
 * @param {*} chartId Id of the charter
 * @param {*} seriesName the name of the series
 * @param {*} subtitle subtitle of the chart
 * @param {*} colors colors of the chart
 */
function getSparklineChart($scope, data, chartId, seriesName, subtitle, colors, selectedPatient) {

    let spark = {
        chart: {
            id: chartId,
            type: 'line',
            height: 140,
            sparkline: {
                enabled: true
            },
            //group: 'sparklines',
            events: {
                click: function (event, chartContext, config) {

                    // Get the chartId -> sensor_id
                    let chartId = config.globals.chartID;

                    // Show the chartId at the top of the big chart
                    $scope.bigChartHeader = chartId;
                    $scope.$apply();

                    // Show the modal with the chart
                    $('#myModal').modal('show');
                    chartDetail(seriesName, chartId, selectedPatient);
                }
            }
        },
        grid: {
            padding: {
                right: 10,
                left: 10,
                bottom: 10
            }
        },
        dataLabels: {
            enabled: true,
            formatter: v => v
        },
        markers: {
            size: 5
        },
        series: [{
            name: seriesName,
            data: data
        }],
        stroke: {
            curve: 'smooth'
        },
        markers: {
            size: 0
        },
        tooltip: {
            /*fixed: {
                enabled: true,
                position: 'right'
            },*/
            x: {
                show: true,
                format: 'dd-MM-yyyy HH:mm',
                /*https://apexcharts.com/docs/datetime/  ->  link to formate date */
            }
        },
        title: {
            //text: data[data.length - 1][1], // Last value known 
            style: {
                fontSize: '26px'
            }
        },
        subtitle: {
            text: subtitle,
            offsetX: 0,
            style: {
                fontSize: '14px',
                cssClass: 'apexcharts-yaxis-title'
            }
        },
        colors: colors,
        xaxis: {
            type: "datetime"
        }
    }

    return spark;

}

/**
 * Generate a chart based on its id
 * @param {*} chartId the id of the charted
 * @param {*} chartName the name of the charter
 * @param {*} units the units of this chart
 */
export function generalChart($scope, chartId, chartName, units) {

    // Generate random data to represent the chart
    let data = [];
    /*let timestamp = 1553436000000;
    for (let i = 0; i < 6; i++) {
        let thisIt = [timestamp, Math.floor((Math.random() * 100) + 1)];
        timestamp += 300000;
        data.push(thisIt);
    }*/

    // pick a random color
    let randomColor = "#000000".replace(/0/g, function () { return (~~(Math.random() * 16)).toString(16); });
    let options = getSparklineChart($scope, data, chartId, chartName, chartName + " (" + units + ")", [randomColor])

    // render the chart
    let spark = new ApexCharts(document.querySelector("#" + chartId), options);
    spark.render();

    // Add the chart to the charts array
    charts.push(spark)

}

/**
 * Update a chart with new values
 * @param {*} allValues values for all the charts
 */
export function updateChartsSeries(allValues, patientId) {

    currentPatientId = patientId;

    // Iterate all over the charts array
    for (let i = 0; i < charts.length; i++) {

        let chartId = charts[i].el.id; // -> Id of the this chart (same as sensor_id)

        // Get the sensor data
        let data = allValues[chartId];

        // If there are data, update the series with the data
        if (data != undefined) {

            // Show only the last 5 values for each chart
            data = data.slice(-5, data.length);

            // Update the title of the chart (last known value)
            charts[i].updateOptions({
                title: {
                    text: data[data.length - 1][1], // Last value known 
                }
            });

            // Update the chart series
            charts[i].updateSeries([{
                data: data
            }]);

        } else { // If there are no data, clean the charts

            // Update the title of the chart (last known value)
            charts[i].updateOptions({
                title: {
                    text: ""
                }
            });

            // Update the chart series
            charts[i].updateSeries([{
                data: []
            }]);

        }
    }

}

/**
 * Generate a detailed chart of a specific sensor
 * 
 * @param {*} seriesName name of the sensor
 * @param {*} chartId sensor_id
 */
export function chartDetail(seriesName, chartId, selectedPatient) {

    // **********
    // chartId contains the sensor_id
    // **********

    let login = checkLogin();

    // Get all the values of the selected sensor
    getPatientValuesBySensor(login.token, selectedPatient, chartId, (result) => {

        // Delete the current detailed chart
        var chartToDelete = document.getElementById("chartHistory");
        while (chartToDelete.firstChild) {
            chartToDelete.removeChild(chartToDelete.firstChild);
        }

        let data = [];

        // Iterate all over the array to parse the data to the right type
        for(let i=0; i<result.values.length; i++) {
            data.push([new Date(result.values[i].ts).getTime(), result.values[i].value])
        }

        let options = {
            chart: {
                toolbar: {
                    show: true
                },
                width: '100%',
                height: '400px'
            },
            grid: {
                padding: {
                    right: 30,
                    left: 30
                }
            },
            dataLabels: {
                enabled: true,
                formatter: v => v
            },
            stroke: {
                curve: "smooth"
            },
            xaxis: {
                type: "datetime",
                //min: 1553436000000,
                //max: 1553436600000
                //range: 1800000
            },
            markers: {
                size: 5
            },
            series: [{
                name: seriesName,
                data: data
            }]
        }

        let chart = new ApexCharts(
            document.querySelector("#chartHistory"),
            options
        );

        // Render the chart when the modal is available
        let renderLoop = setInterval(()=>{

            if($('#chartHistory').is(':visible')){
                chart.render();
                clearInterval(renderLoop)
            }

        }, 100);

    });

}


export function createSparkChart($scope, chartId, chartName, units, data, selectedPatient, alert) {

    // Container of the chart and name
    let containerDiv = document.createElement("div");
    containerDiv.classList.add("col-md-4");
    containerDiv.style.margin = "20px 0";

    // Chart name
    let chartNamePa = document.createElement("p");
    chartNamePa.classList.add("h4");
    chartNamePa.innerHTML = chartName + "(" + chartId + ")";

    containerDiv.appendChild(chartNamePa);

    // Container of the chart
    let chartContainer = document.createElement("div");
    chartContainer.classList.add("box");
    chartContainer.classList.add("shadow");

    containerDiv.appendChild(chartContainer);

    // The chart
    let chartElement = document.createElement("div");
    chartElement.id = chartId;

    chartContainer.appendChild(chartElement);

    document.getElementById("chartList").appendChild(containerDiv);

    if(alert) {
        chartElement.classList.add("alertsignal");
    }



    // pick a random color
    let randomColor = "#000000".replace(/0/g, function () { return (~~(Math.random() * 16)).toString(16); });
    let options = getSparklineChart($scope, data, chartId, chartName, chartName + " (" + units + ")", [randomColor], selectedPatient)

    // render the chart
    let spark = new ApexCharts(document.querySelector("#" + chartId), options);
    spark.render();



}