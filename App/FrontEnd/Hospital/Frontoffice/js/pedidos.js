import { checkLogin } from './myutil.js'

export const domain = "http://localhost:8080";       // -> Para usar a API local
//export const domain = "https://localhost:8080";      // -> Para usar a API local em https com self-signed certificate

/**
 * Admin client
 * @param {*} dataSend data to send with the request
 * @param {*} callback response after the api request
 */
export function loginUser(dataSend, callback) {
    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/doctor/login",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json"
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get a doctor in the database
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getDoctor(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all patients of one doctor
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getDoctorPatients(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patients",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the total number of patients of a doctor
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getDoctorTotalPatients(token, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/totalPatients",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all sensors
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getSensors(token, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/getSensors",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all values of a patent
 * @param {*} token access token
 * @param {*} patientId id of the patient
 * @param {*} callback response after api request
 */
export function getPatientAllValues(token, patientId, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patient/" + patientId + "/allvalues",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all values of sensor of a patient
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} sensor sensor id
 * @param {*} callback response after api request
 */
export function getPatientValuesBySensor(token, patientId, sensor, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patient/" + patientId + "/values/" + sensor,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all devices
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getDevices(token, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/getdevices",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Assign a device to a patient
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function assignDevice(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/doctor/assigndevice",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete an assigned device from a patient
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function deleteAssignedDevice(token, patientId, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/doctor/removeassigneddevice/" + patientId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the views of a doctor
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getViews(token, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/views",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Add a view to a doctor
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function addView(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/doctor/views",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete a view of doctor
 * @param {*} token access token
 * @param {*} viewId view id
 * @param {*} callback response after api request
 */
export function deleteView(token, viewId, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/doctor/views/" + viewId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Update a view of a doctor
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} viewId view id
 * @param {*} callback response after api request
 */
export function updateView(dataSend, token, viewId, callback) {
    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "PUT",
        url: domain + "/api/doctor/views/" + viewId,
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all medical consultations
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getMedicalConsultations(token, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/medicalconsultation",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all medical consultations of a patient
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function getMedicalConsultationsByPatient(token, patientId, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/medicalconsultation/patient/" + patientId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Add a new medical consultation
 * @param {*} token access token
 * @param {*} dataSend data to send with the request
 * @param {*} callback response after api request
 */
export function addNewMedicalConsultation(token, dataSend, callback) {

    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/doctor/medicalconsultation",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Updatate a medical consultation
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} consultationId consultation id
 * @param {*} callback response after api request
 */
export function updateMedicalConsultation(dataSend, token, consultationId, callback) {
    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "PUT",
        url: domain + "/api/doctor/medicalconsultation/" + consultationId,
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete a medical consultation
 * @param {*} token access token
 * @param {*} consultationId consultation id
 * @param {*} callback response after api request
 */
export function deleteMedicalConsultation(token, consultationId, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/doctor/medicalconsultation/" + consultationId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the total number of medical consultations
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getTotalMedicalConsultations(token, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/totalmedicalconsultation",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get all the monitoring sensors of a patient
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function getPatientMonitoringSensors(token, patientId, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patientmonitoringsensors/" + patientId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Add a monitoring sensor to a patient
 * @param {*} token access token
 * @param {*} dataSend data to send with the request
 * @param {*} callback response after api request
 */
export function addSensorMonitoring(token, dataSend, callback) {
    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/doctor/patientmonitoringsensors/",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete all monitoring sensors of a patient
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function deleteSensorMonitoring(token, patientId, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/doctor/patientmonitoringsensors/" + patientId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get a patient by id
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function getPatient(token, patientId, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patient/" + patientId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get a patient in an async way
 * @param {*} token access token
 * @param {*} patientId patient id
 */
export function getPatientAsync(token, patientId) {

    // Promise
    return new Promise(

        function (resolve, reject) {

            $.ajax({
                type: "GET",
                url: domain + "/api/doctor/patient/" + patientId,
                "crossDomain": true,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + token
                },
                success: function (response) {
                    resolve(response);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    reject(false);
                }
            });
        }
    )

}

/**
 * Get a list of active emergencies
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getListOfActiveEmergencies(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/emergencies",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get a list of responding emergencies
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getListOfRespondingEmergencies(token, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/respondingemergencies",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Respond to an emergency
 * @param {*} token access token
 * @param {*} dataSend data to send with the request
 * @param {*} callback response after api request
 */
export function respondToEmergency(token, dataSend, callback) {

    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/doctor/respondtoemergency",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get an active emergency by it's id
 * @param {*} token access token
 * @param {*} emergencyId emergency id
 * @param {*} callback response after api request
 */
export function getActiveEmergencyById(token, emergencyId, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/activeemergency/" + emergencyId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get the patient all contacts
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function getPatientContacts(token, patientId, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patient/" + patientId + "/contacts",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Close an emergency
 * @param {*} token access token
 * @param {*} dataSend data to send with the request
 * @param {*} callback response after api request
 */
export function closeEmergency(token, dataSend, callback) {

    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/doctor/closeemergency",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get the plan of a patient
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function getPatientPlan(token, patientId, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patient/" + patientId + "/plan",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Delete the plan of a patient
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function deletePatientPlan(token, patientId, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/doctor/patient/" + patientId + "/plan",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(true);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Create a plan of a patient
 * @param {*} token access token
 * @param {*} dataSend data to send with the request
 * @param {*} callback response after api request
 */
export function createPatientPlan(token, dataSend, callback) {

    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "POST",
        url: domain + "/api/doctor/patient/plan",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get all patient files
 * @param {*} token access tokne
 * @param {*} patientId patient id
 * @param {*} callback response after api request
 */
export function getPatientFiles(token, patientId, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patient/" + patientId + "/files",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}

/**
 * Get a file from the patient
 * @param {*} token access token
 * @param {*} patientId patient id
 * @param {*} fileId file id
 */
export function getPatientSingleFile(token, patientId, fileId) {

    var request = new XMLHttpRequest();
    request.open('GET', domain + "/doctor/patient/" + patientId + '/getfile/' + fileId, true);
    request.setRequestHeader('Authorization', 'Bearer ' + token);
    request.responseType = 'arraybuffer';
    request.onload = function (e) {
        var data = new Uint8Array(this.response);
        var raw = String.fromCharCode.apply(null, data);
        var base64 = btoa(raw);
        var src = "data:image;base64," + base64;

        document.getElementById("file" + fileId).src = src;
    };

    request.send();

}

/**
 * Get the total number of emergencies
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function getTotalEmergencies(token, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/totalemergencies",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Get the Signals Alerts of one patient
 * @param {*} token access token
 * @param {*} patientId patient to the the alerts
 * @param {*} callback response after api request
 */
export function getSignalsAlerts(token, patientId, callback) {
    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patient/" + patientId + "/alertsignals",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Delete an alert signal
 * @param {*} token access token
 * @param {*} alertId alert to delete
 * @param {*} callback response after api request
 */
export function deleteSignalsAlerts(token, alertId, callback) {
    $.ajax({
        type: "DELETE",
        url: domain + "/api/doctor/alertsignals/" + alertId,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Request to change the password of the current user
 * @param {*} dataSend data to send with the request
 * @param {*} token access token
 * @param {*} callback response after api request
 */
export function changePassword(dataSend, token, callback) {
    var data = JSON.stringify(dataSend);
    $.ajax({
        type: "PATCH",
        url: domain + "/api/doctor/password",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

/**
 * Request to get the patient limits
 * @param {*} token access token
 * @param {*} callback  response after api request
 */
export function getPatientLimits(token, patientId, callback) {

    $.ajax({
        type: "GET",
        url: domain + "/api/doctor/patient/" + patientId + "/sensorslimits",
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });
}

export function updatePatientLimits(token, patientId, dataSend, callback) {
    var data = JSON.stringify(dataSend);

    $.ajax({
        type: "PUT",
        url: domain + "/api/doctor/patient/" + patientId + "/sensorslimits",
        data: data,
        "crossDomain": true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        },
        success: function (response) {
            callback(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            callback(false);
        }
    });

}