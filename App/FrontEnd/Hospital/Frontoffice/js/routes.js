var app = angular.module('myApp', ["ngRoute"]);

// Routes of the web app (Hospital - Frontoffice)
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "/Frontoffice/views/home.htm"
        })
        .when("/mypatients", {
            templateUrl: "/Frontoffice/views/mypatients.htm"
        })
        .when("/dashboard", {
            templateUrl: "/Frontoffice/views/dashboard.htm"
        })
        .when("/plans", {
            templateUrl: "/Frontoffice/views/plans.htm"
        })
        .when("/assigndevice", {
            templateUrl: "/Frontoffice/views/assigndevice.htm"
        })
        .when("/medicalconsultation", {
            templateUrl: "/Frontoffice/views/medicalconsultations.htm"
        })
        .when('/403', {
            templateUrl: "/Frontoffice/views/403.htm"
        })
        .when('/monitoringsensors', {
            templateUrl: "/Frontoffice/views/monitoringsensors.htm"
        })
        .when("/listemergencies", {
            templateUrl: "/Frontoffice/views/listemergencies.htm"
        })
        .when("/detailedemergency/:id", {
            templateUrl: "/Frontoffice/views/detailedemergency.htm"
        })
        .when("/limits", {
            templateUrl: "/Frontoffice/views/patientLimits.htm"
        })
        .otherwise({
            templateUrl: "/Frontoffice/views/404.htm"
        })
});