import { checkLogin, logout } from '../js/myutil.js';
import { getDoctorTotalPatients, getTotalMedicalConsultations, getTotalEmergencies, getDoctorPatients, getSignalsAlerts, deleteSignalsAlerts } from '../js/pedidos.js'

var app = angular.module('myApp');

// Home controller
app.controller('homeCtrl', function ($scope, $window, $location, $rootScope) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {
            return true;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        // Get the total number of patients
        getDoctorTotalPatients(thereIsLogin.token, (result) => {
            if (result) {
                $scope.numPatientsAssigned = result.total;
                $scope.$apply();
            } else {
                $scope.numPatientsAssigned = 0;
                $scope.$apply();
            }
        });

        // Get the total number of consultations
        getTotalMedicalConsultations(thereIsLogin.token, (result) => {
            if (result) {
                $scope.numConsultations = result.total;
                $scope.$apply();
            } else {
                $scope.numConsultations = 0;
                $scope.$apply();
            }
        });

        // Check for alerts every 5 seconds
        setInterval(function () {


            // Get the list of patients of the doctor
            getDoctorPatients(thereIsLogin.token, (responsePatients) => {
                for (let i = 0; i < responsePatients.patients.length; i++) {

                    // Get the alerts of each patient
                    getSignalsAlerts(thereIsLogin.token, responsePatients.patients[i].id, (responseAlerts) => {

                        for (let i = 0; i < responseAlerts.alerts.length; i++) {
                            responseAlerts.alerts[i].date = responseAlerts.alerts[i].date.substring(0, 10) + " " + responseAlerts.alerts[i].date.substring(11, 16)
                        }

                        responsePatients.patients[i]['alerts'] = responseAlerts.alerts;

                        if (i == responsePatients.patients.length - 1) {
                            $scope.patients = responsePatients.patients;
                            $scope.$apply();
                        }

                    });

                }
            });

        }, 5000);

        // When click in an alert
        $scope.clickedAlert = function (patientId, sensorId) {

            // Redirects to the dashboard
            $location.path("dashboard");

            //console.log(patientId)
            //console.log(sensorId)

            // Emit event to display the click patient in the dashboard
            let call = setInterval(function () {
                $rootScope.$broadcast('showAlerts', patientId);
                clearInterval(call);
            }, 500);


        }

        // When click to delete an alert
        $scope.deleteAlert = function (alertId) {

            // Ask for confirmation
            bootbox.confirm("Are you sure you want to dismiss this alert?", function (result) {

                if (result) {

                    // API request to delete
                    deleteSignalsAlerts(thereIsLogin.token, alertId, (deleted) => {

                        if (deleted) {

                            // Remove the alert from the page
                            for (let i = 0; i < $scope.patients.length; i++) {
                                for (let j = 0; j < $scope.patients[i].alerts.length; j++) {
                                    if ($scope.patients[i].alerts[j].id == alertId) {
                                        $scope.patients[i].alerts.splice(j, 1);
                                        $scope.$apply();
                                        break;
                                    }
                                }
                            }

                            bootbox.alert({
                                message: "Success",
                                backdrop: true
                            });
                        } else {
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }

                    });
                }

            });

        }


        // vvv  Emergencies  vvv
        // If the user has permissions to emergency
        if (thereIsLogin.userPermissions.includes("emergency")) {
            getTotalNumberEmergencies();

            // Check for emergencies every 2 seconds
            setInterval(function () {
                getTotalNumberEmergencies();
            }, 2000);

            // Get and update the total number of emergencies in the home page statistic card
            function getTotalNumberEmergencies() {

                getTotalEmergencies(thereIsLogin.token, (result) => {
                    if (result) {
                        $scope.numEmergencies = result.total;
                        $scope.$apply();
                    } else {
                        $scope.numEmergencies = 0;
                        $scope.$apply();
                    }
                });

            }
        }
    }

});