import { checkLogin, logout } from '../js/myutil.js';
import { changePassword, getDoctor, getListOfActiveEmergencies, getListOfRespondingEmergencies, getPatient } from '../js/pedidos.js'

var app = angular.module('myApp');

// Controller do index
app.controller('indexCtrl', function ($scope, $window) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            if (thereIsLogin.userPermissions.includes("doctor")) {
                $scope.doctorVisible = true;
            } else {
                $scope.doctorVisible = false;
            }

            if (thereIsLogin.userPermissions.includes("emergency")) {
                $scope.emergencyVisible = true;
            } else {
                $scope.emergencyVisible = false;
            }

            return true;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the index page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        // Get the doctor object
        getDoctor(thereIsLogin.token, (response) => {
            if (response) {
                $scope.name = response.doctor._name;
                $scope.$apply();
            } else {
                $scope.name = "";
                $scope.$apply();
            }

        })

        // When click to logout
        $scope.logoutBtn = function () {

            logout();
        }

        // When click to change the password
        $scope.changePassword = function () {

            let data = {
                oldPassword: $scope.changePasswordField.oldPassword,
                newPassword: $scope.changePasswordField.newPassword,
                newPasswordConf: $scope.changePasswordField.newPasswordConf
            }

            // Check for confirmation
            bootbox.confirm("Are you sure?", function (confirm) {
                if (confirm) {

                    // Request to change the password
                    changePassword(data, thereIsLogin.token, (result) => {
                        if (result) {
                            bootbox.alert({
                                message: "Password updated",
                                backdrop: true
                            });

                            $scope.changePasswordField.oldPassword = "";
                            $scope.changePasswordField.newPassword = "";
                            $scope.changePasswordField.newPasswordConf = "";
                            $scope.$apply();

                            // Hide the modal
                            $('#updatePasswordModal').modal('hide');

                        } else {
                            bootbox.alert({
                                message: "Something went wrong",
                                backdrop: true
                            });
                        }
                    });

                }
            });



        }

        // Check if the password field and the confirm password field match
        $scope.checkPasswords = function () {

            // Check if the two fields match
            if ($scope.changePasswordField.newPassword === $scope.changePasswordField.newPasswordConf) {
                document.getElementById("newpasswordconf").setCustomValidity("");
            } else {
                document.getElementById("newpasswordconf").setCustomValidity("Passwords must match");
            }

        }

        // Check if a password is strong enough
        $scope.checkPasswordStrength = function () {

            // Id of the DOM element that contains the password to check
            let id = "newpassword";

            // Password inserted by the user
            var term = document.getElementById(id).value;

            // Pattern that checks if the inserted password has at least
            // a capital letter, a number and at least 8 characters
            var patt = /(?=^.{8,}$)(?=.*\d)(?=.*[A-Z])/;

            // Check if the password has all the requirements
            if (patt.exec(term)) {

                document.getElementById(id).setCustomValidity("");

            } else {
                let message = "The password must have at least 8 characters, a number and a capital letter";
                document.getElementById(id).setCustomValidity(message);
            }

        }

        // If the user has permissions to emergency
        if (thereIsLogin.userPermissions.includes("emergency")) {

            let accessAlerts = true;

            if (accessAlerts) {

                let newEmergenciesQNT = 0; // Qnt of new emergencies
                let respondingEmergenciesQNT = 0; // Qnt of responding emergencies

                // Get the new emergencies
                getNewEmergencies();

                // Get the respondig emergencies
                getRespondingEmergencies();

                // Update the alert button style
                updateAlertButtonStyle();

                // Check for emergencies every 2 seconds
                setInterval(function () {

                    // Check for new emergencies
                    getNewEmergencies();

                    // Check for responding emergencies
                    getRespondingEmergencies();

                    // Logout automatically when the login expires
                    thereIsLogin = checkLogin();
                    if (!thereIsLogin) {

                        // Redirect do the index page
                        $window.location.href = '/doctor/login';

                    }

                }, 2000);

                // Function that get new emergencies
                function getNewEmergencies() {

                    // Get a list of the active emergencies
                    getListOfActiveEmergencies(thereIsLogin.token, (emergencies) => {
                        let listNewEmergencies = emergencies.emergencies;

                        // Iterate ovwer the array of new emergencies
                        for (let i = 0; i < listNewEmergencies.length; i++) {

                            // Get the date for the current location
                            listNewEmergencies[i].ts_start = new Date(listNewEmergencies[i].ts_start);
                            let diffMinutes = listNewEmergencies[i].ts_start.getTimezoneOffset(); // Get summer time
                            listNewEmergencies[i].ts_start.setMinutes(listNewEmergencies[i].ts_start.getMinutes() - diffMinutes);
                            listNewEmergencies[i].ts_start = listNewEmergencies[i].ts_start.toISOString();

                            // Format the date time
                            let date = listNewEmergencies[i].ts_start.split('T')[0];
                            let time = listNewEmergencies[i].ts_start.split('T')[1].slice(0, 5);
                            listNewEmergencies[i].ts_start = date + " " + time;

                            // Get the patient involved in the emergency
                            getPatient(thereIsLogin.token, listNewEmergencies[i].patient_id, (patient) => {
                                listNewEmergencies[i]['patient'] = patient.patient;

                                // When it reaches to the last iteration
                                if (i == listNewEmergencies.length - 1) {

                                    $scope.listNewEmergencies = listNewEmergencies;
                                    newEmergenciesQNT = $scope.listNewEmergencies.length;
                                    $scope.$apply();
                                    updateAlertButtonStyle();

                                }

                            });

                        }

                        // If there isn't emergencies
                        if (listNewEmergencies.length == 0) {
                            $scope.listNewEmergencies = listNewEmergencies;
                            newEmergenciesQNT = $scope.listNewEmergencies.length;
                            $scope.$apply();
                            updateAlertButtonStyle();
                        }

                    });

                }

                // Function that gets the responding emergencies
                function getRespondingEmergencies() {

                    // Get the list of responding emergencies
                    getListOfRespondingEmergencies(thereIsLogin.token, (emergencies) => {

                        let listRespondingEmergencies = emergencies.emergencies;

                        for (let i = 0; i < listRespondingEmergencies.length; i++) {

                            // Get the date for the current location
                            listRespondingEmergencies[i].ts_start = new Date(listRespondingEmergencies[i].ts_start);
                            let diffMinutes = listRespondingEmergencies[i].ts_start.getTimezoneOffset(); // Get summer time
                            listRespondingEmergencies[i].ts_start.setMinutes(listRespondingEmergencies[i].ts_start.getMinutes() - diffMinutes);
                            listRespondingEmergencies[i].ts_start = listRespondingEmergencies[i].ts_start.toISOString();

                            // Format the date time
                            let date = listRespondingEmergencies[i].ts_start.split('T')[0];
                            let time = listRespondingEmergencies[i].ts_start.split('T')[1].slice(0, 5);
                            listRespondingEmergencies[i].ts_start = date + " " + time;

                            // Get the patient involved in the emergency
                            getPatient(thereIsLogin.token, listRespondingEmergencies[i].patient_id, (patient) => {
                                listRespondingEmergencies[i]['patient'] = patient.patient;

                                // When it reaches to the last iteration
                                if (i == listRespondingEmergencies.length - 1) {
                                    $scope.listRespondingEmergencies = listRespondingEmergencies;
                                    respondingEmergenciesQNT = $scope.listRespondingEmergencies.length;
                                    $scope.$apply();
                                    updateAlertButtonStyle();

                                }

                            });

                        }

                        // If there isn't emergencies
                        if (listRespondingEmergencies.length == 0) {
                            $scope.listRespondingEmergencies = listRespondingEmergencies;
                            respondingEmergenciesQNT = $scope.listRespondingEmergencies.length;
                            $scope.$apply();
                            updateAlertButtonStyle();
                        }


                    });



                }

                // Update alert button styles
                function updateAlertButtonStyle() {

                    let totalEmergencies = newEmergenciesQNT + respondingEmergenciesQNT;

                    if (totalEmergencies > 0) {
                        document.getElementById("alertButton").classList.add("blinking");
                    } else {
                        document.getElementById("alertButton").classList.remove("blinking");
                    }


                    document.getElementById("numberofemergencies").innerHTML = totalEmergencies;
                }
            }
        }

    }
});