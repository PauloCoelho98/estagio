import { checkLogin, logout, formatDate, paginationSplitInChuncks } from '../js/myutil.js';
import { getDoctorPatients } from '../js/pedidos.js';
import { domain } from '../js/pedidos.js'

var app = angular.module('myApp');


// Home controller
app.controller('mypatientsCtrl', function ($scope, $window, $location, $rootScope) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("doctor")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        let patients = [];

        let myPatientsChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        // Get the patients of this doctor
        getDoctorPatients(thereIsLogin.token, (result) => {
            patients = result.patients;

            for (let i = 0; i < patients.length; i++) {
                let date = patients[i].birthday.split('T')[0];
                patients[i].birthday = date;
            }

            $scope.patients = patients;
            $scope.$apply();

            setPagination();

        });

        // Add pagination to the page
        function setPagination(newPatients) {

            let result;

            if (newPatients === undefined) {
                // Object that contain an array of chuncks and the total number of pages
                result = paginationSplitInChuncks(patients, elementsPerChunck);
            } else {
                result = paginationSplitInChuncks(newPatients, elementsPerChunck);
            }

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            myPatientsChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.patients = myPatientsChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // When click to search
        $scope.searchPatient = function () {

            if ($scope.search === undefined) {
                setPagination();
            } else {
                let filtered = [];

                // Iterate over all patients and filter
                for (let i = 0; i < patients.length; i++) {
                    let n = patients[i].name.toLowerCase().search($scope.search.toLowerCase());

                    if (n >= 0) {
                        filtered.push(patients[i]);
                    }

                }

                setPagination(filtered);
            }
        }

        // When click a patient
        $scope.clickedPatient = function (patientId) {

            // Redirects to the dashboard
            $location.path("dashboard");
            
            // Emit event to display the click patient in the dashboard
            let call = setInterval(function () {
                $rootScope.$broadcast('changeData', patientId);
                clearInterval(call);
            }, 500);


        }

        // Get the picture of the patient
        $scope.getFileData = function (patientId) {

            let imgSrc = document.getElementById("file" + patientId).src;

            if (imgSrc == '') {

                var request = new XMLHttpRequest();
                request.open('GET', domain + "/api/doctor/patient/" + patientId + "/picture", true);
                request.setRequestHeader('Authorization', 'Bearer ' + thereIsLogin.token);
                request.responseType = 'arraybuffer';
                request.onload = function (e) {
                    var data = new Uint8Array(this.response);
                    var raw = String.fromCharCode.apply(null, data);
                    var base64 = btoa(raw);
                    var src = "data:image;base64," + base64;

                    document.getElementById("file" + patientId).src = src;
                };

                request.send();

            }

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= myPatientsChunks.length) {
                page = myPatientsChunks.length - 1;
            }

            currentPage = page;
            $scope.patients = myPatientsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < myPatientsChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }

    }

});