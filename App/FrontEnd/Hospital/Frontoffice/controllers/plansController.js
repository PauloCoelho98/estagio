import { checkLogin, logout, formatDate, paginationSplitInChuncks } from '../js/myutil.js';
import { getDoctorPatients, getPatientPlan, deletePatientPlan, getPatientMonitoringSensors, createPatientPlan } from '../js/pedidos.js';

var app = angular.module('myApp');

// Home controller
app.controller('plansCtrl', function ($scope, $window, $location, $rootScope) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("doctor")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        let patients = [];

        let myPatientsChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        let currentActivePatientId = -1;

        document.getElementById("showplandiv").style.display = "none";

        // Get the patients of this doctor
        getDoctorPatients(thereIsLogin.token, (result) => {
            patients = result.patients;

            for (let i = 0; i < patients.length; i++) {
                let date = patients[i].birthday.split('T')[0];
                patients[i].birthday = date;
            }

            if(patients.length>0) {
                $scope.viewPatientPlan(patients[0].id)
            }

            $scope.patients = result.patients;
            $scope.$apply();

            setPagination();

        });

        // When click to view a patient plan
        $scope.viewPatientPlan = function (patientId) {

            document.getElementById("showplandiv").style.display = "block";

            // Get that patient plan
            getPatientPlan(thereIsLogin.token, patientId, (plan) => {

                $scope.showPlan = plan.plan;
                $scope.selectedPatient = patientId;
                $scope.$apply();
                changeListStyle(patientId);

                let planByDays = {};
                for (let i = 0; i < plan.plan.length; i++) {

                    let day = plan.plan[i].day_week;

                    if (planByDays.hasOwnProperty(day)) {
                        planByDays[day].push(plan.plan[i]);
                    } else {
                        planByDays[day] = [plan.plan[i]];
                    }
                }
                
                $scope.planByDays = planByDays;
                $scope.$apply();


            });

            // Get the monitoring sensors of that patient
            getPatientMonitoringSensors(thereIsLogin.token, patientId, (monitoringSensors) => {

                $scope.monitoringSensors = monitoringSensors.patientMonitoringSensors;
                $scope.$apply();

            });

        }

        // When click to delete a plan
        $scope.deletePlan = function (patientId) {

            bootbox.confirm("Are you sure?", function (toDelete) {

                if (toDelete) {

                    // Delete the patient plan
                    deletePatientPlan(thereIsLogin.token, patientId, (result) => {

                        if (result) {

                            bootbox.alert({
                                message: "Deleted",
                                backdrop: true
                            });

                            $scope.showPlan = [];
                            $scope.planByDays = [];
                            $scope.$apply();

                        } else {
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }

                    });
                }
            });
        }

        // When click in the plan edit button
        $scope.editPlanButton = function () {
            //console.log(currentActivePatientId);
            //console.log($scope.showPlan);
            //console.log($scope.monitoringSensors);
        }

        // When click to remove a line of the plan
        $scope.removePlanLine = function (index) {
            $scope.showPlan.splice(index, 1);
        }

        // When click to add a line to the plan
        $scope.addPlanLine = function () {
            if($scope.monitoringSensors.length>0) {
                $scope.showPlan.push({ "sensor_id": $scope.monitoringSensors[0].sensor_id, day_week: "monday" });
            }else{
                bootbox.alert({
                    message: "This patient must have monitoring sensors associated to perform this operation!",
                    backdrop: true
                });
            }
        }

        // When submit an edited plan
        $scope.submitEditPlan = function () {

            let plans = [];
            let newShowPlans = [];

            for (let i = 0; i < $scope.showPlan.length; i++) {
                let day = document.getElementById("editDay" + i).value;
                let sensor = document.getElementById("editSensor" + i).value;
                let time = document.getElementById("editTime" + i).value;

                let plan = {
                    "sensorId": sensor,
                    "dayWeek": day,
                    "time": time
                }

                let showPlan = {
                    "sensor_id": sensor,
                    "day_week": day,
                    "time": time
                }

                plans.push(plan);
                newShowPlans.push(showPlan);
            }

            let dataSend = {
                "patientId": currentActivePatientId,
                "plan": plans
            }

            // Create a new plan
            createPatientPlan(thereIsLogin.token, dataSend, (edited) => {

                if (edited) {
                    bootbox.alert({
                        message: "Updated",
                        backdrop: true
                    });

                    let planByDays = {};
                    for (let i = 0; i < newShowPlans.length; i++) {

                        let day = newShowPlans[i].day_week;

                        if (planByDays.hasOwnProperty(day)) {
                            planByDays[day].push(newShowPlans[i]);
                        } else {
                            planByDays[day] = [newShowPlans[i]];
                        }
                    }

                    $scope.planByDays = planByDays;
                    $scope.$apply();

                    // Hide the modal
                    $('#editPlanModal').modal('hide');

                } else {
                    bootbox.alert({
                        message: "Failed",
                        backdrop: true
                    });
                }

            });

        }

        // Change the list of patients style
        function changeListStyle(patientId) {

            // Timeout of 0 miliseconds, don't work if it's not setted
            let call = setInterval(function () {

                let previousElement = document.getElementById("patientlist" + currentActivePatientId);
                if (previousElement) {
                    previousElement.classList.remove("active");
                }

                let currentElement = document.getElementById("patientlist" + patientId);
                if (currentElement) {
                    currentElement.classList.add("active");
                }

                currentActivePatientId = patientId;

                clearInterval(call);
            }, 0);

        }


        // Add pagination to the page
        function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(patients, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            myPatientsChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.patients = myPatientsChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }


        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= myPatientsChunks.length) {
                page = myPatientsChunks.length - 1;
            }

            currentPage = page;
            $scope.patients = myPatientsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < myPatientsChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

            changeListStyle(currentActivePatientId);

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }

    }

});