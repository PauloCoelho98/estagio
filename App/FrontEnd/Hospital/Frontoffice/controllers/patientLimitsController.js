import { checkLogin, logout, formatDate, paginationSplitInChuncks } from '../js/myutil.js';
import { getDoctorPatients, getPatientLimits, getSensors, updatePatientLimits } from '../js/pedidos.js';

var app = angular.module('myApp');

// Patient limits controller
app.controller('patientLimitsCtrl', function ($scope, $window, $location, $rootScope) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("doctor")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        let currentEditingPatient = 0;

        // Get all the sensors
        getSensors(thereIsLogin.token, (result) => {
            let sensors = result.sensors;
            $scope.sensors = sensors;
            //console.log(sensors)
            $scope.$apply();
        });

        // Get the patients of this doctor
        getDoctorPatients(thereIsLogin.token, (result) => {
            let patients = result.patients;
            $scope.patients = patients;
            $scope.$apply();

            // If the number of patients is greater than 0, selects the first to be displayed
            if (patients.length > 0) {
                document.getElementById("selPatient").value = patients[0].id;

                currentEditingPatient = patients[0].id;

                // Put the limits on the form
                setLimits(patients[0].id);

            }

            // To style only selects with the selPatient id
            $('#selPatient').selectpicker();

        });

        // Get the selected patient
        $(function () {
            $("#selPatient").on("changed.bs.select", function (e, clickedIndex, newValue, oldValue) {
                let selectedDText = $(this).find('option').eq(clickedIndex).text();
                let selectedDValue = $(this).find('option').eq(clickedIndex).val();

                if (selectedDValue !== "") {

                    currentEditingPatient = selectedDValue;

                    // Put the limits on the form
                    setLimits(selectedDValue);

                }
            });
        });

        // Put the limits of the patient on the form
        function setLimits(patientId) {
            // Get the select patient's limits
            getPatientLimits(thereIsLogin.token, patientId, (resultLimits) => {

                for(let i=0; i<$scope.sensors.length; i++) {
                    document.getElementById($scope.sensors[i].sensor_id + "-min").value = '';
                    document.getElementById($scope.sensors[i].sensor_id + "-max").value = '';
                }

                if (resultLimits) {
                    let limits = resultLimits.limits;

                    for (let i = 0; i < limits.length; i++) {
                        document.getElementById(limits[i].sensor_id + "-min").value = limits[i].min;
                        document.getElementById(limits[i].sensor_id + "-max").value = limits[i].max;
                    }
                }

            });
        }

        // When click to submit the limits of a patient
        $scope.submitEditLimits = function() {

            let sensors = $scope.sensors;

            // Array os limts to submit to the server
            let limits = [];

            for(let i=0; i<sensors.length; i++){
                limits.push({"sensor_id": sensors[i].sensor_id, "min": undefined, "max": undefined});
            }

            let mins = document.getElementsByClassName("minVal");
            let maxs = document.getElementsByClassName("maxVal");

            // Iterate over the mins elements
            for(let i=0; i<mins.length; i++) {

                // Get the value and if it is empty change to undefined
                let val = mins[i].value;
                if(val == '') {
                    val = undefined;
                }

                // Get the sensor of this value
                let sensor_id = mins[i].id.split("-")[0];

                // Look for the sensor inside the limits and change the value
                for(let j=0; j<limits.length; j++) {
                    if(limits[j].sensor_id == sensor_id) {
                        limits[j].min = val;
                    }
                }

            }

            // Iterave ober the maxs elements
            for(let i=0; i<maxs.length; i++) {

                // Get the value and if it is empty change to undefined
                let val = maxs[i].value;
                if(val == '') {
                    val = undefined;
                }

                // Get the sensor of this value
                let sensor_id = maxs[i].id.split("-")[0];

                // Look for the sensor inside the limits and change the value
                for(let j=0; j<limits.length; j++) {
                    if(limits[j].sensor_id == sensor_id) {
                        limits[j].max = val;
                    }
                }

            }

            // Iterate over the limits and delete the objects that has min or max undefined
            for(let i=0; i<limits.length; i++) {
                if(limits[i].min == undefined || limits[i].max == undefined) {
                    limits.splice(i, 1);
                    i--;
                }
            }

            // Convert the limits array to an object
            limits = {"limits": limits}

            // Update the limits
            updatePatientLimits(thereIsLogin.token, currentEditingPatient, limits, (updated)=>{
                if(updated) {
                    bootbox.alert({
                        message: "Limits for this patient updated successfully!",
                        backdrop: true
                    });
                }else{
                    bootbox.alert({
                        message: "Failed!",
                        backdrop: true
                    });
                }
            })

        }

    }

});