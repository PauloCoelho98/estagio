import { checkLogin, paginationSplitInChuncks } from '../js/myutil.js';
import { getMedicalConsultations, getDoctorPatients, addNewMedicalConsultation, updateMedicalConsultation, deleteMedicalConsultation } from '../js/pedidos.js'

var app = angular.module('myApp');

// Controller do index
app.controller('medicalConsultationsCtrl', function ($scope, $window, $location, $rootScope) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("doctor")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the index page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {
        let newMedicalConsultations = [];
        let oldMedicalConsultations = [];

        let patients = [];

        let newMedicalConsultationsChunks = [];
        let currentNewMedicalConsultationsPage = 0;
        let elementsPerChunck = 5;

        let currentNewConsultationIndexEditing = 0;
        let currentIdEditing = 0;


        let oldMedicalConsultationsChunks = [];
        let currentOldMedicalConsultationsPage = 0;

        // Get the patients of the doctor
        getDoctorPatients(thereIsLogin.token, (responsePatients) => {
            patients = responsePatients.patients;
            $scope.patients = patients;
            $scope.$apply();

            // Get all medical consultations from the DB and update the view
            getMedicalConsultations(thereIsLogin.token, (response) => {

                if (response.medicalConsultation.length > 0) {

                    let today = new Date();

                    newMedicalConsultations = response.medicalConsultation;
                    let pastMedicalConsultations = [];

                    // Iterate over the medical consultations
                    for (let i = 0; i < newMedicalConsultations.length; i++) {

                        // Get the complete patient object for each patient id
                        let patientId = newMedicalConsultations[i].patient_id;
                        let patient = patients.filter(patient => patient.id === patientId)[0];
                        newMedicalConsultations[i]['patient'] = patient;

                        // Get a more readable format of the consultation date
                        let consultationDate = new Date(newMedicalConsultations[i].date);
                        newMedicalConsultations[i].date = newMedicalConsultations[i].date.split('T')[0] + " " + newMedicalConsultations[i].date.split('T')[1].slice(0, 5)

                        // If the date of this consultation is older than the today's date, 
                        // add this consultation to the past consultations array and remove from the current array
                        if (consultationDate < today) {
                            let consultation = newMedicalConsultations.splice(i, 1);
                            oldMedicalConsultations.push(consultation[0]);
                            i--;
                        }

                    }

                    // Reverse the past medical consultations array
                    oldMedicalConsultations.reverse();

                    // Update the scope
                    $scope.oldMedicalConsultations = oldMedicalConsultations;
                    $scope.newMedicalConsultations = newMedicalConsultations;
                    $scope.$apply();

                    // Set pagination in both pages
                    setNPagination();
                    setOPagination();
                }

            });

        });



        // When click to add a new medicalConsultations
        $scope.addNewMedicalConsultation = function () {

            //$scope.newMedicalConsultation.time.setHours($scope.newMedicalConsultation.time.getHours() + 1);
            //$scope.newMedicalConsultation.date.setDate($scope.newMedicalConsultation.date.getDate() + 1);

            //let date = $scope.newMedicalConsultation.date.toISOString().split('T')[0];
            
            // *** toISOString ignores the summer time, LocaleTimeString don't *** //

            //let time = $scope.newMedicalConsultation.time.toISOString().split('T')[1].slice(0, 5);
            let time = $scope.newMedicalConsultation.time.toLocaleTimeString().slice(0, 5);

            let dateF = $scope.newMedicalConsultation.date.toLocaleDateString();
            let day = dateF.substring(0,2);
            let month = dateF.substring(3,5);
            let year = dateF.substring(6,10);
            
            let date = year + "-" + month + "-" + day;

            let patientElement = document.getElementById("patientChoose");
            let patientId = patientElement.options[patientElement.selectedIndex].value;

            let dataSend = { doctorId: thereIsLogin.userId, patientId: patientId, date: date + " " + time }

            let today = new Date();

            // Check if the date inserted is greatter than the date in the current moment
            if (new Date(dataSend.date) < today) {
                bootbox.alert({
                    message: "Can't add a consultation with a date before the current moment!",
                    backdrop: true
                });

            } else {

                // Request to add new medicalConsultations
                addNewMedicalConsultation(thereIsLogin.token, dataSend, (response) => {

                    // If added 
                    if (response) {

                        let obj = {
                            id: response._id,
                            date: response._date,
                            doctor_id: response._doctorId,
                            patient_id: response._patientId,
                            patient: patients.filter(patient => patient.id == response._patientId)[0]
                        }

                        // Push the new medicalConsultations to the array of medicalConsultations
                        newMedicalConsultations.unshift(obj);

                        // Hide the modal
                        $('#addMedicalConsultationModal').modal('hide');

                        // Clear the form inputs
                        $scope.newMedicalConsultation.date = "";
                        $scope.newMedicalConsultation.time = "";

                        // Apply the changes/ update the view
                        $scope.$apply();

                        // Update the chuncks
                        setNPagination();

                        // Dialog box that informs the success of the operation
                        bootbox.alert({
                            message: "Added",
                            backdrop: true
                        });

                    } else { // If didn't add the doctor
                        bootbox.alert({
                            message: "Failed",
                            backdrop: true
                        });
                    }
                });
            }
        }

        // When click to update the medicalConsultations
        $scope.edit = function (id) {
            $('#updateMedicalConsultationModal').modal('show');

            // Search for the medicalConsultations clicked in the array on medicalConsultations
            for (let i = 0; i < newMedicalConsultations.length; i++) {

                // if found
                if (newMedicalConsultations[i].id == id) {

                    // Update variables that contain information about index of array and id of the medicalConsultations
                    currentNewConsultationIndexEditing = i;
                    currentIdEditing = newMedicalConsultations[i].id;
                }
            }

            // Update the update modal values
            //$scope.updatemedicalConsultations = { name: newMedicalConsultations[currentNewConsultationIndexEditing]._name, email: newMedicalConsultations[currentNewConsultationIndexEditing]._email };

            let medicalConsultationToEdit = newMedicalConsultations.filter(medicalConsultation => medicalConsultation.id == currentIdEditing)[0];

            let consultationPatientId = medicalConsultationToEdit.patient_id;


            document.getElementById('patientUpdateChoose').value = consultationPatientId;

            // Get the consultation date
            let dateConsultation = medicalConsultationToEdit.date;
            dateConsultation = new Date(dateConsultation);

            // Put the consultation date in the modal
            let day = ("0" + dateConsultation.getDate()).slice(-2);
            let month = ("0" + (dateConsultation.getMonth() + 1)).slice(-2);
            let stringDateConsultation = dateConsultation.getFullYear()+"-"+(month)+"-"+(day) ;
            $('#updatedate').val(stringDateConsultation);

            // Put the consultaton time in the modal
            let hour = dateConsultation.getHours();
            let minute = dateConsultation.getMinutes();console.log(minute)
            if(hour<10) { // Fix the format
                hour = '0' + hour;
            }
            if(minute<10) { // Fix the format
                minute = '0' + minute;
            }
            let timeConsultation = hour + ':' + minute;
            document.getElementById('updatetime').value = timeConsultation;

        }

        // When click to submit the updated medicalConsultations
        $scope.updateMedicalConsultationForm = function () {

            // Confirm that really wants to update
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {

                    //$scope.updateMedicalConsultation.time.setHours($scope.updateMedicalConsultation.time.getHours() + 1);

                    let date = document.getElementById("updatedate").value;

                    let time = document.getElementById("updatetime").value;

                    let patientElement = document.getElementById("patientUpdateChoose");
                    let patientId = patientElement.options[patientElement.selectedIndex].value;

                    let dataSend = { id: currentIdEditing, doctorId: thereIsLogin.userId, patientId: patientId, date: date + " " + time }

                    // Request to update the medicalConsultations
                    updateMedicalConsultation(dataSend, thereIsLogin.token, currentIdEditing, (response) => {

                        // If updated
                        if (response) {

                            let obj = {
                                id: response._id,
                                date: response._date,
                                doctor_id: response._doctorId,
                                patient_id: response._patientId,
                                patient: patients.filter(patient => patient.id == response._patientId)[0]
                            }

                            // Update the view
                            newMedicalConsultations[currentNewConsultationIndexEditing] = obj;

                            // Obter qual o chunck que se localiza o exercicio editado
                            let chunckNumber = Math.floor(currentNewConsultationIndexEditing / elementsPerChunck);

                            // Dentro do chunck obter a posição onde se localiza o exercicio editado
                            let chunckPosition = currentNewConsultationIndexEditing % elementsPerChunck;

                            // Alterar o exercicio dentro da lista de chuncks
                            newMedicalConsultationsChunks[chunckNumber][chunckPosition] = obj;

                            $scope.$apply();

                            bootbox.alert({
                                message: "Updated",
                                backdrop: true
                            });

                            // Hide the modal
                            $('#updateMedicalConsultationModal').modal('hide');
                        } else {
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }

                    });

                }
            });

        }

        // When click to delete a medicalConsultations
        $scope.delete = function (id) {

            // Confirm that really wants to delete
            bootbox.confirm("Are you sure?", function (result) {

                if (result) {
                    // Request to delete the medicalConsultations
                    deleteMedicalConsultation(thereIsLogin.token, id, (response) => {

                        // If deleted
                        if (response) {

                            // Informs that deleted
                            bootbox.alert({
                                message: "Deleted",
                                backdrop: true
                            });

                            // Iterate all over the array, delete the medicalConsultations and update the view
                            for (let i = 0; i < newMedicalConsultations.length; i++) {

                                if (newMedicalConsultations[i].id == id) {
                                    newMedicalConsultations.splice(i, 1);
                                }
                            }

                            $scope.$apply();

                            // Update the chuncks
                            setNPagination();

                        } else { // If did not deleted
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }
                    });
                }
            });
        }

        // When click in the name of the patient
        $scope.clickedPatient = function (patientId) {

            // Redirects to the dashboard
            $location.path("dashboard");

            // Emit event to show the clicked patient in the dashboard
            let call = setInterval(function () {
                $rootScope.$broadcast('changeData', patientId);
                clearInterval(call);
            }, 500);


        }

        /* NEW CONSULTATIONS PAGINATION */

        // Add pagination to the page
        function setNPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(newMedicalConsultations, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            newMedicalConsultationsChunks = chuncks;

            $scope.numberNPages = numPages;

            currentNewMedicalConsultationsPage = 0;
            $scope.newMedicalConsultations = newMedicalConsultationsChunks[currentNewMedicalConsultationsPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("pageN0").classList.add("active");

        }

        // Change the page
        $scope.setNPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= newMedicalConsultationsChunks.length) {
                page = newMedicalConsultationsChunks.length - 1;
            }

            currentNewMedicalConsultationsPage = page;
            $scope.newMedicalConsultations = newMedicalConsultationsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < newMedicalConsultationsChunks.length; i++) {
                document.getElementById("pageN" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("pageN" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousNPage = function () {
            $scope.setNPage(currentNewMedicalConsultationsPage - 1);
        }

        // Go to the next page
        $scope.nextNPage = function () {
            $scope.setNPage(currentNewMedicalConsultationsPage + 1);
        }

        /* OLD CONSULTATIONS PAGINATION */

        // Add pagination to the page
        function setOPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(oldMedicalConsultations, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            oldMedicalConsultationsChunks = chuncks;

            $scope.numberOPages = numPages;

            currentOldMedicalConsultationsPage = 0;
            $scope.oldMedicalConsultations = oldMedicalConsultationsChunks[currentOldMedicalConsultationsPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("pageO0").classList.add("active");

        }

        // Change the page
        $scope.setOPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= oldMedicalConsultationsChunks.length) {
                page = oldMedicalConsultationsChunks.length - 1;
            }

            currentOldMedicalConsultationsPage = page;
            $scope.oldMedicalConsultations = oldMedicalConsultationsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < oldMedicalConsultationsChunks.length; i++) {
                document.getElementById("pageO" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("pageO" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousOPage = function () {
            $scope.setOPage(currentOldMedicalConsultationsPage - 1);
        }

        // Go to the next page
        $scope.nextOPage = function () {
            $scope.setOPage(currentOldMedicalConsultationsPage + 1);
        }

    }

});