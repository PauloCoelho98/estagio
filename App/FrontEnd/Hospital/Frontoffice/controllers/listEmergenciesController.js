import { checkLogin } from '../js/myutil.js';
import { respondToEmergency } from '../js/pedidos.js'

var app = angular.module('myApp');

// Controller do index
app.controller('listEmergenciesCtrl', function ($scope, $window, $location) {

    let thereIsLogin = checkLogin();
    
    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("emergency")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the index page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        // When click in a new alert to respond 
        $scope.setResponding = function(id) {
            
            // Respond to the emergency
            respondToEmergency(thereIsLogin.token, {"emergencyId": id}, (result)=>{

                if(result) {
                    $location.path("detailedemergency/" + id);
                } else{
                    bootbox.alert({
                        message: "Failed",
                        backdrop: true
                    });
                }

            });

        }
        
    }
});