import { checkLogin, logout } from '../js/myutil.js';
import { generalChart, updateChartsSeries, createSparkChart } from '../js/charts.js';
import { getSignalsAlerts, getDoctorPatients, getSensors, getPatientAllValues, getViews, addView, deleteView, updateView, getPatient, getMedicalConsultationsByPatient, getPatientMonitoringSensors, getPatientPlan, getPatientFiles, getPatientSingleFile } from '../js/pedidos.js'
import { domain } from '../js/pedidos.js'

var app = angular.module('myApp');


// Home controller
app.controller('dashboardCtrl', function ($scope, $window, $parse) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("doctor")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        let patients = []; // All the patients of this doctor
        let selectedPatient = {}; // The selected patient to see the values
        let valuesBySensor = {}; // All the values for the selected patient

        let views = []; // List of views of charts

        window.Apex = {
            dataLabels: {
                enabled: false
            }
        };

        document.getElementById("patientData").style.display = "none";

        // Receive an event and change the displaying patient
        $scope.$on('changeData', function (event, arg) {
            let patientId = arg;
            document.getElementById('selPatient').value = patientId;
            $scope.selectPatient(patientId);
            showPatientData(patientId);

            // Update the selected option on bootstrap-select
            $("#selPatient").selectpicker("refresh");

        });

        // Receive an event and change the displaying patient and charts
        $scope.$on('showAlerts', function (event, arg) {

            const patientId = arg

            document.getElementById('selPatient').value = arg;
            $scope.selectPatient(patientId);
            showPatientData(patientId);

            // Update the selected option on bootstrap-select
            $("#selPatient").selectpicker("refresh");

            // Get the alerts of this patient
            getAlertsSignals(arg, (alerts) => {

                // Select the charts that have alerts
                for (let i = 0; i < alerts.length; i++) {
                    let element = document.getElementById("selectoption" + alerts[i]);
                    element.selected = 'selected';
                }

                // Show the charts in the page
                selectCharts();

                // Update the selected option on bootstrap-select
                $("#selectCharts").selectpicker("refresh");
            });


        });

        // Get a list of all sensors that exist
        getSensors(thereIsLogin.token, (response) => {

            let sensors = response.sensors; // Sensors that exist

            // Add a property that control if the chart for this sensor is to be displayed
            for (let i = 0; i < sensors.length; i++) {
                sensors[i]['show'] = false;
            }

            $scope.sensors_list = sensors;
            $scope.$apply();

            // To style only selects with the selectCharts id
            $('#selectCharts').selectpicker();

            // Generate the charts for all the sensors
            for (let i = 0; i < $scope.sensors_list.length; i++) {
                //generalChart($scope, $scope.sensors_list[i].sensor_id, $scope.sensors_list[i].name, $scope.sensors_list[i].units);
            }

        });

        // Get the patients of this doctor
        getDoctorPatients(thereIsLogin.token, (result) => {

            patients = result.patients;
            $scope.patients = patients;

            $scope.$apply();

            // If the number of patients is greater than 0, selects the first to be displayed
            if (patients.length > 0) {
                document.getElementById("selPatient").value = patients[0].id;
                $scope.selectPatient(patients[0].id);
                showPatientData(patients[0].id);
                getAlertsSignals(patients[0].id, (alerts) => { });
            }

            // To style only selects with the selPatient id
            $('#selPatient').selectpicker();

        });

        // Get all the views of charts for this doctor
        getViews(thereIsLogin.token, (result) => {

            views = result.views;

            $scope.views = views;
            $scope.$apply();

            // To style only selects with the selView id
            $('#selView').selectpicker();

        });

        // Get the picture of the patient
        $scope.getPatientImage = function (patientId) {

            if (patientId != undefined) {

                let imgSrc = document.getElementById("image" + patientId).src;

                if (imgSrc == '') {

                    var request = new XMLHttpRequest();
                    request.open('GET', domain + "/api/doctor/patient/" + patientId + "/picture", true);
                    request.setRequestHeader('Authorization', 'Bearer ' + thereIsLogin.token);
                    request.responseType = 'arraybuffer';
                    request.onload = function (e) {
                        var data = new Uint8Array(this.response);
                        var raw = String.fromCharCode.apply(null, data);
                        var base64 = btoa(raw);
                        var src = "data:image;base64," + base64;

                        document.getElementById("image" + patientId).src = src;
                    };

                    request.send();

                }

            }

        }

        // Show all the patient data in the dashboard
        function showPatientData(id) {

            // Get the patient data
            getPatient(thereIsLogin.token, id, (patient) => {

                // Parse the date to a more readable format
                let date = patient.patient._birthday.split('T')[0];
                patient.patient._birthday = date;

                // Apply the changes to the page
                $scope.selectedpatient = patient.patient
                $scope.$apply();

            });

            // Get all medical consultations of the displaying patient
            getMedicalConsultationsByPatient(thereIsLogin.token, id, (consultations) => {

                let oldConsultations = [];
                let nextConsultations = [];

                // Iterate all over the medical consultations array
                for (let i = 0; i < consultations.medicalConsultation.length; i++) {

                    // Get the date of the medical consultation
                    let consultationDate = new Date(consultations.medicalConsultation[i].date);

                    // Get the current date
                    let today = new Date();

                    // Get the difference between the current date and the consultation date in days
                    const diffTime = Math.abs(consultationDate.getTime() - today.getTime());
                    let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    diffDays--;

                    // Set a property in the consultation object that contains the difference in days
                    consultations.medicalConsultation[i]['diffDays'] = diffDays;

                    // Check if the consultation date already passed
                    if (consultationDate > today) {

                        // Add the consultations to an array of next consultations
                        nextConsultations.push(consultations.medicalConsultation[i]);

                    } else {

                        // Add the consultations to an array of old consultations
                        oldConsultations.push(consultations.medicalConsultation[i]);
                    }

                }

                // Iterate all over the old consultations and format the date
                for (let i = 0; i < oldConsultations.length; i++) {
                    // Parse the date to a more readable format
                    let date = oldConsultations[i].date.split('T')[0];
                    let time = oldConsultations[i].date.split('T')[1].slice(0, 5);

                    oldConsultations[i].date = date + " " + time;
                }

                // Iterate all over the next consultations and format the date
                for (let i = 0; i < nextConsultations.length; i++) {
                    // Parse the date to a more readable format
                    let date = nextConsultations[i].date.split('T')[0];
                    let time = nextConsultations[i].date.split('T')[1].slice(0, 5);

                    nextConsultations[i].date = date + " " + time;
                }

                // Reverse the old consultations array for displaying purposes
                oldConsultations = oldConsultations.reverse();

                // Update the scope
                $scope.oldConsultations = oldConsultations;
                $scope.nextConsultations = nextConsultations;
                $scope.$apply();

            });

            // Get all monitoring sensors for the current displaying patient
            getPatientMonitoringSensors(thereIsLogin.token, id, (monitoringSensors) => {

                // Iterate over the monitoring sensors and append the complete object of the sensor
                for (let i = 0; i < monitoringSensors.patientMonitoringSensors.length; i++) {

                    for (let j = 0; j < $scope.sensors_list.length; j++) {
                        if (monitoringSensors.patientMonitoringSensors[i].sensor_id == $scope.sensors_list[j].sensor_id) {
                            monitoringSensors.patientMonitoringSensors[i]['sensor'] = $scope.sensors_list[j];
                        }
                    }

                }

                // Update the scope
                $scope.monitoringSensors = monitoringSensors.patientMonitoringSensors;
                $scope.$apply();
            });

            // Get the plan of the current displaying pationt
            getPatientPlan(thereIsLogin.token, id, (plan) => {

                $scope.showPlan = plan.plan;

                // Iterate over the plan and append the complete object of the sensor to each item of the plan
                for (let i = 0; i < plan.plan.length; i++) {

                    for (let j = 0; j < $scope.sensors_list.length; j++) {
                        if (plan.plan[i].sensor_id == $scope.sensors_list[j].sensor_id) {
                            plan.plan[i]['sensor'] = $scope.sensors_list[j];
                        }
                    }

                }

                // Organize the plan by day of the week
                let planByDays = {};
                for (let i = 0; i < plan.plan.length; i++) {

                    let day = plan.plan[i].day_week;

                    if (planByDays.hasOwnProperty(day)) {
                        planByDays[day].push(plan.plan[i]);
                    } else {
                        planByDays[day] = [plan.plan[i]];
                    }
                }

                // Update the scope
                $scope.planByDays = planByDays;
                $scope.$apply();

            });

            // Get the files uplouded by the current displaying patient
            getPatientFiles(thereIsLogin.token, id, (files) => {

                for (let i = 0; i < files.files.length; i++) {
                    let dateFormatted = files.files[i].date.split("T")[0] + " " + files.files[i].date.split("T")[1].slice(0, 8);
                    files.files[i].date = dateFormatted
                }

                $scope.files = files.files;
                $scope.$apply();
            });

            // Get a file by it's id
            $scope.getFileData = function (fileId) {

                getPatientSingleFile(thereIsLogin.token, id, fileId)

            }

        }

        // Update the sensor values of a patient
        function updateValues(id, callback) {

            // If selected the empty option
            if (id === undefined) {
                selectedPatient = {};
            } else {

                // Get the selected patient
                let result = patients.filter(patient => patient.id == id);
                selectedPatient = result[0];

                let selectedPatientId = selectedPatient.id;

                // Clear all the values
                valuesBySensor = {};

                // Get all the values from the selected patient
                getPatientAllValues(thereIsLogin.token, selectedPatientId, (result) => {

                    // Iterate all over the values array
                    for (let i = 0; i < result.values.length; i++) {

                        // Get the sensor id
                        let sensor_id = result.values[i].sensor_id;

                        // If the obj already has this prop then push a new value
                        if (valuesBySensor.hasOwnProperty(sensor_id)) {

                            valuesBySensor[sensor_id].push([new Date(result.values[i].ts).getTime(), result.values[i].value]);

                            // If not, create a new array
                        } else {
                            valuesBySensor[sensor_id] = [[new Date(result.values[i].ts).getTime(), result.values[i].value]];
                        }

                    }

                    callback();

                });
            }
        }

        // Select a chart to be displayed
        function selectCharts() {
            var charts = $('#selectCharts option:selected');
            var selected = [];
            $(charts).each(function (index, chart) {
                selected.push([$(this).val()][0]);
            });

            // If there is charts selected, then displays the charts
            if (selected.length > 0) {

                document.getElementById("patientData").style.display = "none";

            } else { // If there aren't charts selected, then displays the patient information

                document.getElementById("patientData").style.display = "block";
                var e = document.getElementById("selPatient");
                var patientId = e.options[e.selectedIndex].value;
                showPatientData(patientId);

            }

            let sensors_list = $scope.sensors_list;

            // Iterates all over the charts
            for (let i = 0; i < sensors_list.length; i++) {

                // If is selected, show this chart
                if (selected.includes(sensors_list[i].sensor_id)) {
                    $scope.sensors_list[i].show = true;
                } else {
                    $scope.sensors_list[i].show = false;
                }

            }

            $scope.$apply();

            // Only show charts if there is selected charts
            if (selected.length > 0) {

                updateValues(selectedPatient.id, () => {

                    // Delete all charts
                    var chartsToDelete = document.getElementById("chartList");
                    while (chartsToDelete.firstChild) {
                        chartsToDelete.removeChild(chartsToDelete.firstChild);
                    }

                    // Create the new charts
                    for (let i = 0; i < selected.length; i++) {

                        const sensorInfo = $scope.sensors_list.filter(sensor => sensor.sensor_id == selected[i])[0];

                        let selectedPatient = document.getElementById("selPatient").value;

                        let alert = false;
                        if($scope.signalsalerts.includes(sensorInfo.sensor_id)) {
                            alert = true;
                        }

                        createSparkChart($scope, selected[i], sensorInfo.name, sensorInfo.units, valuesBySensor[selected[i]], selectedPatient, alert);

                    }


                });

            } else {
                // If there isn't charts selected and there is a patient selected, show patient data
                if (selectedPatient != undefined && selectedPatient.id != undefined) {
                    showPatientData(selectedPatient.id)
                }
            }
        }

        // Get the alerts signals from a patient
        function getAlertsSignals(patientId, callback) {
            getSignalsAlerts(thereIsLogin.token, patientId, (responseAlerts) => {

                let alerts = [];
                for (let i = 0; i < responseAlerts.alerts.length; i++) {
                    alerts.push(responseAlerts.alerts[i].sensor_id);
                }

                $scope.signalsalerts = alerts;
                $scope.$apply();

                callback(alerts);

            });
        }

        // Get the selected patient
        $(function () {
            $("#selPatient").on("changed.bs.select", function (e, clickedIndex, newValue, oldValue) {
                let selectedDText = $(this).find('option').eq(clickedIndex).text();
                let selectedDValue = $(this).find('option').eq(clickedIndex).val();

                if (selectedDValue !== "") {

                    // Get the alerts of this patient
                    getAlertsSignals(selectedDValue, (alerts) => { });

                    // Select the patient
                    $scope.selectPatient(selectedDValue);
                }
            });
        });

        // Get the selected chart
        $(function () {
            $("#selectCharts").on("changed.bs.select", function (e, clickedIndex, newValue, oldValue) {

                selectCharts();

            });
        });


        // Get the selected view
        $(function () {
            $("#selView").on("changed.bs.select", function (e, clickedIndex, newValue, oldValue) {
                let selectedDText = $(this).find('option').eq(clickedIndex).text();
                let selectedDValue = $(this).find('option').eq(clickedIndex).val();

                // Get the sensors of the view from the array of views
                let view = views.filter(view => view.view_id == selectedDValue);

                let sensors = view[0].sensors

                // Clean all the selected charts
                for (let i = 0; i < $scope.sensors_list.length; i++) {
                    let element = document.getElementById("selectoption" + $scope.sensors_list[i].sensor_id);
                    element.selected = '';
                }

                // Select the charts from the view
                for (let i = 0; i < sensors.length; i++) {
                    let element = document.getElementById("selectoption" + sensors[i]);
                    element.selected = 'selected';
                }

                selectCharts();

                // Update the selected option on bootstrap-select
                $("#selectCharts").selectpicker("refresh");


            });
        });

        // When click to select a chart to display(Select box)
        $scope.selectPatient = function (id) {

            if (id !== undefined) {
                var select1 = document.getElementById("selectCharts");
                var selected1 = []; // contains all the selected values of the select box

                // Put in an array all the selected charts
                for (var i = 0; i < select1.length; i++) {

                    if (select1.options[i].selected) {
                        selected1.push(select1.options[i].value)
                    };
                }

                if (selected1.length > 0) {
                    document.getElementById("patientData").style.display = "none";
                } else {
                    document.getElementById("patientData").style.display = "block";
                    showPatientData(id)
                }

                updateValues(id, () => {

                    if (selected1.length > 0) {

                        // Delete all charts
                        var chartsToDelete = document.getElementById("chartList");
                        while (chartsToDelete.firstChild) {
                            chartsToDelete.removeChild(chartsToDelete.firstChild);
                        }

                        // Create the new charts
                        for (let i = 0; i < selected1.length; i++) {

                            const sensorInfo = $scope.sensors_list.filter(sensor => sensor.sensor_id == selected1[i])[0];

                            let selectedPatient = document.getElementById("selPatient").value;

                            let alert = false;
                            if($scope.signalsalerts.includes(sensorInfo.sensor_id)) {
                                alert = true;
                            }

                            createSparkChart($scope, selected1[i], sensorInfo.name, sensorInfo.units, valuesBySensor[selected1[i]], selectedPatient, alert);

                        }
                    }

                });
            }

        }

        // When click to open an image
        $scope.openImage = function(fileId) {
            
            let fileSrc = document.getElementById("file" + fileId).src;
            let w = window.open("");
            w.document.write("<html><head><head><body><img style='display:block; margin:auto' src='" + fileSrc + "'></img> <button style='margin:auto; display:block; font-size: 25px;' onclick='window.close()'>Close</button></body></html>");
            
        }

        // When click to add a new view
        $scope.addNewView = function () {

            var charts = $('#selectCharts option:selected');
            var selected = [];
            $(charts).each(function (index, chart) {
                selected.push([$(this).val()][0]);
            });


            // Asks for the name of the new view
            bootbox.prompt("Set the name for the new view: ", function (name) {

                // If the name was set
                if (name) {

                    // API request to add a new view
                    addView({ "doctorId": thereIsLogin.userId, "viewName": name, "sensors": selected }, thereIsLogin.token, (result) => {

                        // 
                        if (result) {

                            bootbox.alert({
                                message: "Success",
                                backdrop: true
                            });

                            // $scope.views is undefined if the doctor does not have views yet
                            if ($scope.views == undefined) {
                                $scope.views = [{ "doctor_id": thereIsLogin.userId, "sensors": selected, "view_id": result.view_id, "view_name": name }];
                            } else {
                                $scope.views.push({ "doctor_id": thereIsLogin.userId, "sensors": selected, "view_id": result.view_id, "view_name": name });
                            }

                            $scope.$apply();

                        } else {
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }

                        // Update the options on bootstrap-select
                        $("#selView").selectpicker("refresh");

                    });
                }
            });

        }

        // When click to delete a view
        $scope.deleteView = function () {
            // Get the selected view
            var charts = $('#selView option:selected');
            var selected = [];
            $(charts).each(function (index, chart) {
                selected.push([$(this).val()][0]);
            });
            selected = selected[0];

            // Confirm that really wants to delete
            bootbox.confirm("Are you sure?", function (result) {
                if (result) {
                    // API request to delete the view
                    deleteView(thereIsLogin.token, selected, (result) => {

                        if (result) {

                            let views = $scope.views.filter(view => view.view_id != selected);
                            $scope.views = views;
                            $scope.$apply();

                            bootbox.alert({
                                message: "Success",
                                backdrop: true
                            });

                        } else {

                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });

                        }

                        // Update the options on bootstrap-select
                        $("#selView").selectpicker("refresh");

                    });
                }
            });

        }

        // When click to update the view
        $scope.updateView = function () {

            // Get the selected view
            var charts = $('#selView option:selected');
            var selected = [];
            $(charts).each(function (index, chart) {
                selected.push([$(this).val()][0]);
            });
            selected = selected[0];

            // Get the selected charts
            var charts = $('#selectCharts option:selected');
            var selectedCharts = [];
            $(charts).each(function (index, chart) {
                selectedCharts.push([$(this).val()][0]);
            });

            // Asks for the name of the new view
            bootbox.prompt("Set the new name for the new view: ", function (name) {

                // If the name was set
                if (name) {

                    // Asks for confirmation
                    bootbox.confirm("Are you sure?", function (sure) {

                        // If confirmed
                        if (sure) {

                            // API request to update the view
                            updateView({ viewName: name, sensors: selectedCharts }, thereIsLogin.token, selected, (updated) => {

                                // If updated
                                if (updated) {

                                    // Searches for the view in the array of views
                                    for (let i = 0; i < $scope.views.length; i++) {

                                        // If found, updates with the new data
                                        if ($scope.views[i].view_id == selected) {
                                            $scope.views[i] = { "doctor_id": thereIsLogin.userId, "sensors": selectedCharts, "view_id": selected, "view_name": name }
                                            $scope.$apply();
                                        }

                                    }

                                    bootbox.alert({
                                        message: "Updated",
                                        backdrop: true
                                    });

                                } else {

                                    bootbox.alert({
                                        message: "Failed",
                                        backdrop: true
                                    });

                                }

                                // Update the options on bootstrap-select
                                $("#selView").selectpicker("refresh");

                            });
                        }
                    });
                }
            });


        }
    }
});