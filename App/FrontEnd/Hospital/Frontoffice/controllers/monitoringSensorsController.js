import { checkLogin, logout, paginationSplitInChuncks } from '../js/myutil.js';
import { getDoctorPatients, getPatientMonitoringSensors, getSensors, addSensorMonitoring, deleteSensorMonitoring } from '../js/pedidos.js'

var app = angular.module('myApp');

// Controller do index
app.controller('monitoringSensorsCtrl', function ($scope, $window, $location, $rootScope) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("doctor")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the index page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        let patients = [];

        let myPatientsChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        // Get the list of sensors
        getSensors(thereIsLogin.token, (sensors) => {
            $scope.sensors = sensors.sensors;
            $scope.$apply();

            // Get all the patients of this doctor
            getDoctorPatients(thereIsLogin.token, (result) => {

                // Iterate over the patients arary
                for (let i = 0; i < result.patients.length; i++) {

                    // Get the monitoring sensors for each patient
                    getPatientMonitoringSensors(thereIsLogin.token, result.patients[i].id, (resultMonitoringSensors) => {

                        // Get only the sensor_id instead of the complete object
                        for (let j = 0; j < resultMonitoringSensors.patientMonitoringSensors.length; j++) {
                            resultMonitoringSensors.patientMonitoringSensors[j] = resultMonitoringSensors.patientMonitoringSensors[j].sensor_id;
                        }

                        // Add the monitoring sensors array to the patient object
                        result.patients[i]['monitoringSensors'] = resultMonitoringSensors.patientMonitoringSensors;

                        // If it is the last iteration
                        if (i == result.patients.length - 1) {

                            // Necessary timeout, or it will fail
                            setTimeout(function () {
                                patients = result.patients;
                                $scope.patients = result.patients;
                                $scope.$apply();

                                setPagination();

                                // Iterate over the patients
                                for (let k = 0; k < $scope.patients.length; k++) {

                                    // Iterate over the monitoring sensors of the patient
                                    for (let m = 0; m < $scope.patients[k].monitoringSensors.length; m++) {

                                        // Put the option selected
                                        document.getElementById("selectoption" + $scope.patients[k].id + $scope.patients[k].monitoringSensors[m]).selected = 'selected';

                                    }

                                }

                                // To style only selects with the selectCharts id
                                $('select').selectpicker();

                            }, 100);

                        }

                    });

                }

            });
        });

        // When click to change the monitoring sensors
        $scope.changeMonitoringSensors = function (patientId) {

            let selectId = "select" + patientId;

            var charts = $('#' + selectId + ' ' + 'option:selected');
            var selected = [];
            $(charts).each(function (index, chart) {
                selected.push([$(this).val()][0]);
            });

            // Delete All current sensors monitoring
            deleteSensorMonitoring(thereIsLogin.token, patientId, (resultDeleted) => {

                // Iterate over the sensors to add and add to the database
                for (let i = 0; i < selected.length; i++) {

                    let data = { "patientId": patientId, "sensorId": selected[i] };

                    // Add a monitoring sensor
                    addSensorMonitoring(thereIsLogin.token, data, (result) => {

                        if (i == selected.length - 1) {
                            bootbox.alert({
                                message: "Done",
                                backdrop: true
                            });
                        }

                    })
                }

                // If there isn't options selected informs that all monitoring sensors were remove
                if (selected.length == 0) {
                    bootbox.alert({
                        message: "Removed all monitoring sensors",
                        backdrop: true
                    });
                }

            });

        }

        // When click in a patient name
        $scope.clickedPatient = function (patientId) {

            // Redirects to the dashboard
            $location.path("dashboard");

            // Emit event to display the clicked patient in the dashboard
            let call = setInterval(function () {
                $rootScope.$broadcast('changeData', patientId);
                clearInterval(call);
            }, 500);

        }

        // Add pagination to the page
        function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(patients, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            myPatientsChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.patients = myPatientsChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= myPatientsChunks.length) {
                page = myPatientsChunks.length - 1;
            }

            currentPage = page;
            $scope.patients = myPatientsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < myPatientsChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");


            // Timeout of 0 miliseconds, don't work if it's not setted
            let call = setInterval(function () {

                // Iterate over the patients
                for (let k = 0; k < $scope.patients.length; k++) {

                    // Iterate over the monitoring sensors of the patient
                    for (let m = 0; m < $scope.patients[k].monitoringSensors.length; m++) {

                        // Put the option selected
                        document.getElementById("selectoption" + $scope.patients[k].id + $scope.patients[k].monitoringSensors[m]).selected = 'selected';

                    }

                }
                clearInterval(call);
            }, 0);

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }

    }
});