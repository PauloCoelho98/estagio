import { checkLogin, logout, formatDate, paginationSplitInChuncks } from '../js/myutil.js';
import { getDoctorPatients, getDevices, assignDevice, deleteAssignedDevice } from '../js/pedidos.js';

var app = angular.module('myApp');

// Home controller
app.controller('assignDeviceCtrl', function ($scope, $location, $rootScope) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("doctor")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the login page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        let patients = [];

        let myPatientsChunks = [];
        let currentPage = 0;
        let elementsPerChunck = 5;

        // Get the patients of this doctor
        getDoctorPatients(thereIsLogin.token, (result) => {

            patients = result.patients;
            $scope.patients = result.patients;
            $scope.$apply();

            setPagination();

            // Get all existing devices
            getDevices(thereIsLogin.token, (result) => {
                $scope.devices = result.devices;
                $scope.$apply();

                // To style all selects
                $('select').selectpicker();

                // When select an option in the select box
                $(function () {
                    $("select").on("changed.bs.select", function (e, clickedIndex, newValue, oldValue) {
                        let selectedDText = $(this).find('option').eq(clickedIndex).text();
                        let selectedDValue = $(this).find('option').eq(clickedIndex).val(); // Option selected

                        let idFromSelect = $(this).find("select").prevObject[0].id;
                        
                        let patientId = idFromSelect.substr(3,idFromSelect.length); // Id of the patient
                        
                        // If selected an empty option, sets the selecteDValue to undefined
                        if(selectedDValue == "") {
                            selectedDValue = undefined;
                        }

                        // Call the function to assign the device
                        $scope.assignDevice(patientId, selectedDValue);

                    });
                });

            });

        });



        // When click to assign a device
        $scope.assignDevice = function (patientId, deviceId) {

            // If the device if is undefined
            if (deviceId === undefined) {

                // Request to delete the association between device and patient
                deleteAssignedDevice(thereIsLogin.token, patientId, (result) => {
                    if (result) {
                        bootbox.alert({
                            message: "Removed assignment",
                            backdrop: true
                        });
                    } else {
                        bootbox.alert({
                            message: "Failed",
                            backdrop: true
                        });
                    }
                });

            } else {

                let dataSend = { doctorId: thereIsLogin.userId, deviceId: deviceId, patientId: patientId };

                // Request to assign a device
                assignDevice(dataSend, thereIsLogin.token, (result) => {
                    if (result) {
                        bootbox.alert({
                            message: "Device assigned",
                            backdrop: true
                        });

                        let idCurrentSelected = -1;

                        // Iterate over all patients
                        for (let i = 0; i < patients.length; i++) {

                            // When find the previous owner of the device
                            if (patients[i].current_device_id == deviceId) {
                                patients[i].current_device_id = null;
                                idCurrentSelected = patients[i].id;
                            }

                            // When find the new owner of the device
                            if (patients[i].id == patientId) {
                                patients[i].current_device_id = deviceId;
                            }

                        }

                        $scope.patients = patients;
                        $scope.$apply();

                        setPagination();

                        // Delete the select option of the previous owner
                        var opt = document.getElementById("sel" + idCurrentSelected);
                        if (opt) {
                            opt.options[0].selected = true;
                        }

                        // Update the selected option on bootstrap-select
                        $("select").selectpicker("refresh");

                    } else {
                        bootbox.alert({
                            message: "Failed to assign device",
                            backdrop: true
                        });
                    }
                });
            }

        }

        // When click in a patient, redirects to the dashboard and shows the information of the clicked patient
        $scope.clickedPatient = function (patientId) {
            $location.path("dashboard");

            // Set a timer in order to send the request and make sure that the page is loaded
            let call = setInterval(function () {
                $rootScope.$broadcast('changeData', patientId);
                clearInterval(call);
            }, 500);


        }

        // Add pagination to the page
        function setPagination() {

            // Object that contain an array of chuncks and the total number of pages
            let result = paginationSplitInChuncks(patients, elementsPerChunck);

            let chuncks = result.arrayChuncks;
            let numPages = result.numberOfPages;

            myPatientsChunks = chuncks;

            $scope.numberPages = numPages;

            currentPage = 0;
            $scope.patients = myPatientsChunks[currentPage]; // set the page equals to the chunck 0
            $scope.$apply();

            document.getElementById("page0").classList.add("active");

        }

        // Change the page
        $scope.setPage = function (page) {

            if (page < 0) {
                page = 0;
            }

            if (page >= myPatientsChunks.length) {
                page = myPatientsChunks.length - 1;
            }

            currentPage = page;
            $scope.patients = myPatientsChunks[page];

            // Remove the active style of the pagination list
            for (let i = 0; i < myPatientsChunks.length; i++) {
                document.getElementById("page" + i).classList.remove("active");
            }

            // Add the active style to the current page
            document.getElementById("page" + page).classList.add("active");

        }

        // Go to the previous page
        $scope.previousPage = function () {
            $scope.setPage(currentPage - 1);
        }

        // Go to the next page
        $scope.nextPage = function () {
            $scope.setPage(currentPage + 1);
        }

    }

});