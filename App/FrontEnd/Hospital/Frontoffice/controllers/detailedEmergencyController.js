import { checkLogin } from '../js/myutil.js';
import { getActiveEmergencyById, getPatient, getPatientContacts, closeEmergency } from '../js/pedidos.js'

var app = angular.module('myApp');

// Controller do index
app.controller('detailedEmergencyCtrl', function ($scope, $window, $routeParams, $location, $sce) {

    let thereIsLogin = checkLogin();

    // Function that checks if the user has access to page of this controller
    function hasPermission() {
        if (thereIsLogin.userType === "Doctor") {

            let access;

            if (thereIsLogin.userPermissions.includes("emergency")) {
                access = true;
            } else {
                access = false;
            }

            return access;
        } else {
            return false;
        }
    }

    // Check if the user is logged in
    if (!thereIsLogin) {

        // Redirect do the index page
        $window.location.href = '/doctor/login';

    } if (!hasPermission()) {

        $window.location.href = '#!403';

    } else {

        let emergencyId = $routeParams.id;
        $scope.emergencyId = emergencyId;

        // Get an active emergency by id
        getActiveEmergencyById(thereIsLogin.token, emergencyId, (emergency) => {

            let patientId = emergency.emergency.patient_id;

            // Get the patient involved in the emergency
            getPatient(thereIsLogin.token, patientId, (patient) => {

                let patientObj = patient.patient;
                $scope.patient = patientObj;

                // Link to the google maps, with the address of the patient
                let link = 'http://maps.google.pt/maps?hl=pt&q=' + patientObj._address + '&ie=UTF8&t=&z=17&iwloc=B&output=embed';

                // Display the google maps the the address of the patient
                document.getElementById("iframe").setAttribute("src", link);

                /*var $iframe = $('#' + 'iframe');
                if ( $iframe.length ) {
                    $iframe.attr('src', 'http://maps.google.pt/maps?hl=pt&q='+patientObj._address+'&ie=UTF8&t=&z=17&iwloc=B&output=embed');
                    //$iframe.attr('src', link);   
                }*/

                //window.open("https://maps.google.com/?q="+patientObj._address, '_blank', 'location=yes,height=800,width=800,scrollbars=yes,status=yes,top=0,left=0');

                $scope.$apply();

            });

            // Get the contacts of the patient
            getPatientContacts(thereIsLogin.token, patientId, (contacts) => {
                let listContacts = contacts.contacts;

                let standardContacts = [];
                let ownContacts = [];

                // Separate the contacts by 'own' and standard
                for (let i = 0; i < listContacts.length; i++) {
                    if (listContacts[i].hasOwnProperty('contact')) {
                        standardContacts.push(listContacts[i]);
                    } else {
                        ownContacts.push(listContacts[i]);
                    }
                }

                // Update the scope
                $scope.standardContacts = standardContacts;
                $scope.ownContacts = ownContacts;
                $scope.$apply();

            });

        });

        // When click to close an emergency
        $scope.closeEmergency = function (emergencyId) {

            // Asks for confirmation
            bootbox.confirm("Are you sure?", function (result) {

                // If confirmed
                if (result) {

                    // Close the emergency
                    closeEmergency(thereIsLogin.token, { "emergencyId": emergencyId }, (result) => {

                        if (result) {

                            bootbox.alert({
                                message: "Closed",
                                backdrop: true
                            });

                            // Redirect to the list of emergencies
                            $location.path("listemergencies");

                        } else {
                            bootbox.alert({
                                message: "Failed",
                                backdrop: true
                            });
                        }

                    });

                }

            });

        }


        /*function displayMap() {
            var mymap = L.map('mapid').setView([41.478181, -8.3405282], 15);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoicGF1bG9jb2VsaG8iLCJhIjoiY2p1aWc4ZmZ4MDZ1ejQzbGxzMzVrOGswdSJ9.OlfPbh0mszY0YmtMy7KqVg'
            }).addTo(mymap);

            var marker = L.marker([41.478181, -8.3405282]).addTo(mymap);
            marker.bindPopup("<b>Emergency!</b><br>Here.").openPopup();


        }*/


    }
});