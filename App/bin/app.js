// Necessary modules
const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const cors = require('cors');
const fs = require('fs');
const expressSanitizer = require('express-sanitizer');
const expressValidator = require('express-validator');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const path = require('path');

const https = require('https');

// Middleware for debug purposes that prints the route
/*app.use(function (req, res, next) {
    console.log(req.path);
    next();
});*/

// Middleware to get the files that come with the request
app.use(fileUpload());

// Middleware to allow cross origin requests
app.use(cors());

// Root route
app.get('/', function (req, res) {
    res.sendFile('index.html', { 'root': './FrontEnd/Portal' });
});

// Get the page Hospital>Backoffice>Login
app.get('/admin/login', function (req, res) {
    res.sendFile('login.html', { 'root': './FrontEnd/Hospital/Backoffice' });
});

// Get the page Hospital>Backoffice>Index
app.get('/admin/index', function (req, res) {
    res.sendFile('index.html', { 'root': './FrontEnd/Hospital/Backoffice' });
});

// Get the page Hospital>Frontoffice>Login
app.get('/doctor/login', function (req, res) {
    res.sendFile('login.html', { 'root': './FrontEnd/Hospital/Frontoffice' });
});

// Get the page Hospital>Frontoffice>Index
app.get('/doctor/index', function (req, res) {
    res.sendFile('index.html', { 'root': './FrontEnd/Hospital/Frontoffice' });
});

// Get the page Patient>Login
app.get('/patient/login', function (req, res) {
    res.sendFile('login.html', { 'root': './FrontEnd/Patient' });
});

// Get the page Patient>Index
app.get('/patient/index', function (req, res) {
    res.sendFile('index.html', { 'root': './FrontEnd/Patient' });
});

// Middleware to get static files
app.use(express.static(path.join(__dirname, '../FrontEnd/Hospital')));
app.use(express.static(path.join(__dirname, '../FrontEnd/Patient')));
app.use(express.static(path.join(__dirname, '../FrontEnd')));

// Middleware to get json data from the request
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(expressValidator()); //Middleware to validate data
app.use(expressSanitizer()); //Middleware to sanitize data

// API route files
const routerAdmin = require('../API/routes/adminRoutes');
const routerDoctor = require('../API/routes/doctorRoutes');
const routerPatient = require('../API/routes/patientRoutes');

// Verify Token
function verifyToken(req, res, callback) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        callback();
    } else {
        // Forbidden
        res.sendStatus(403);
    }

}

// Middleware to sanitize the body of the requests  BUG add plan because of the object inside the array with array inside
/*app.use(function (req, res, next) {

    // Check if the request has a body
    if (req.body) {

        // Iterate over the body properties
        for (var key in req.body) {

            console.log(key)

            // Check if the body has the property
            if (req.body.hasOwnProperty(key)) {

                // Check if the property is an array
                if (Array.isArray(req.body[key])) {

                    // Iterate over the array and sanitize the values
                    for (let i = 0; i < req.body[key].length; i++) {
                        req.body[key][i] = req.sanitize(req.body[key][i])
                    }

                } else {

                    // Sanitize the value
                    req.body[key] = req.sanitize(req.body[key]);

                }
            }
        }

    }

    //next();

});*/

// Middleware to check if the request comes with a valid access token
app.use(function (req, res, next) {

    // Check if it is a request to login
    if ((req.path === '/api/admin/login' || req.path === '/api/doctor/login' || req.path === '/api/patient/login') && req.method === 'POST') {

        // If it is a login request go to the next middleware
        next();

        // If it isn't a login request, check for an access token
    } else {

        // Read the secret key from a file
        fs.readFile('./API/datafiles/jwtsecretkey.json', 'utf8', function (err, secretKey) {

            // If couldn't read the secreat key from the file return an error message
            if (err) {
                //throw err;
                console.log(err)
                res.status(500).json({ "Message": "Something went wrong" });

            } else {

                // Parse the file content to json
                secretKey = JSON.parse(secretKey);

                // Check if the request has an access token
                verifyToken(req, res, () => {

                    // Check if the access token is valid
                    jwt.verify(req.token, secretKey.secret_key, (err, authData) => {

                        // If the token isn't valid return a 403 error
                        if (err) {
                            res.status(403).json({ "Message": "Invalid token" });
                        } else {

                            // Place the token content in a special variable to be accessed later to get the user informations
                            res.locals.authData = authData;
                            next();

                        }

                    });

                });
            }

        });
    }

});

// API routes routes
app.use('/', routerAdmin);
app.use('/', routerDoctor);
app.use('/', routerPatient);

// If the specified route doesn't exist, shows 404 message
app.use(function (req, res) {
    res.status(404).json({ "Info": "Route not found" });
});

// Start server on port 8080
app.listen(8080, () => {
    console.log('Server is Running on Port 8080!');
});

// openssl req -nodes -new -x509 -keyout server.key -out server.cert          -> Generate a self-signed certificate
// we will pass our 'app' to 'https' server
/*https.createServer({
    key: fs.readFileSync('../encryption/server.key'),
    cert: fs.readFileSync('../encryption/server.cert'),
}, app)
.listen(8080, () => {
    console.log('Server is Running on Port 8080!');
});*/
